﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ExcellOn.DAL;

namespace ExcellOn.BLL
{
    public class Repository<T> : IRepository<T> where T : class, new()
    {
        private DataContext db;
        private DbSet<T> tbl;
        public Repository()
        {
            db = new DataContext();
            tbl = db.Set<T>();
        }

        public T Add(T entity)
        {
            try
            {
                tbl.Add(entity);
                Save();
                return entity;
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }

        public T AddDTO(T entity)
        {
            try
            {
                tbl.Add(entity);
                SaveEdit();
                return entity;
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }

        public bool Any(Expression<Func<T, bool>> expression)
        {
            return tbl.AsNoTracking().Any(expression);
        }

        public bool Delete(object id)
        {
            try
            {
                T t = FindOneById(id);
                tbl.Remove(t);
                Save();
                return true;
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }

        public T Edit(T entity)
        {
            try
            {
                db.Entry(entity).State = EntityState.Modified;
                Save();
                return entity;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<T> FindAll()
        {
            return tbl.AsEnumerable();
        }

        public IEnumerable<T> FindAllByExpress(Expression<Func<T, bool>> expression)
        {
            return tbl.Where(expression).AsEnumerable();
        }

        public T FindOneByExpress(Expression<Func<T, bool>> expression)
        {
            return tbl.SingleOrDefault(expression);
        }

        public T FindOneByExpressToEdit(Expression<Func<T, bool>> expression)
        {
            return tbl.AsNoTracking().SingleOrDefault(expression);
        }

        public T FindOneById(object id)
        {
            return tbl.Find(id);
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public void SaveEdit()
        {
            db.Configuration.ValidateOnSaveEnabled = false;
            db.SaveChanges();
        }
    }
}
