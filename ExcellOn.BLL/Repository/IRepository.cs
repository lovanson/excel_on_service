﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExcellOn.BLL
{
    public interface IRepository<T> where T:class,new()
    {
        IEnumerable<T> FindAll();
        IEnumerable<T> FindAllByExpress(Expression<Func<T,bool>> expression);
        T FindOneByExpress(Expression<Func<T,bool>> expression);
        T FindOneById(object id);
        bool Any(Expression<Func<T, bool>> expression);
        T Add(T entity);
        T AddDTO(T entity);
        bool Delete(object id);
        T Edit(T entity);
        void Save();
        void SaveEdit();
        T FindOneByExpressToEdit(Expression<Func<T, bool>> expression);
    }
}
