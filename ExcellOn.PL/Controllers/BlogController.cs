﻿using ExcellOn.BLL;
using ExcellOn.PL.Areas.Admin.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Controllers
{
    public class BlogController : Controller
    {
        private Repository<DAL.New> newRepo;
        private Repository<DAL.PostCategory> postcatRepo;

        public BlogController()
        {
            newRepo = new Repository<DAL.New>();
            postcatRepo = new Repository<DAL.PostCategory>();

        }
        // GET: Blog
        public ActionResult Index(string categoryName = "", int page = 1)
        {
            var data = newRepo.FindAllByExpress(x => x.Status);
            if (!String.IsNullOrEmpty(categoryName))
            {
                data = data.Where(x => x.PostCategory.Name.Equals(categoryName)).ToList();
            }
            foreach (var item in data)
            {
                item.IdEncrypt = Encrypt.EncryptString(item.Id.ToString());
            }
            var listCategory = postcatRepo.FindAll().ToList();
            foreach (var item in listCategory)
            {
                item.NewsCount = newRepo.FindAllByExpress(x => x.PostCategoryId == item.Id).Count();
            }
            ViewBag.PostCategory = listCategory;

            var pageSize = 4;

            int totalPage = (int)Math.Ceiling((double)data.Count() / pageSize);
            ViewBag.TotalPage = totalPage;
            ViewBag.Page = page;
            ViewBag.categoryName = categoryName;
            var ListBlog = newRepo.FindAll().OrderByDescending(x => x.CreatedDate).Take(5);
            foreach (var item in ListBlog)
            {
                item.IdEncrypt = Encrypt.EncryptString(item.Id.ToString());
            }
            ViewBag.listBlog = ListBlog;
            if (data.Count() > 0)
            {
                return View(data.OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize));
            }
            else
            {
                return RedirectToAction("ClientNotFound", "Errors");
            }

        }


        public ActionResult BlogDetails(string id)
        {
            try
            {
                var Id = Convert.ToInt32(Encrypt.DecryptString(id));
              



                var listCategory = postcatRepo.FindAll().ToList();
                foreach (var item in listCategory)
                {
                    item.NewsCount = newRepo.FindAllByExpress(x => x.PostCategoryId == item.Id).Count();
                }
                ViewBag.PostCategory = listCategory;
                var ListBlog = newRepo.FindAll().OrderByDescending(x => x.CreatedDate).Take(5);
                foreach (var item in ListBlog)
                {
                    item.IdEncrypt = Encrypt.EncryptString(item.Id.ToString());
                }
                ViewBag.listBlog = ListBlog;
                return View(newRepo.FindOneById(Id));
            }
            catch (Exception)
            {

                return RedirectToAction("Index");
            }
        }
    }
}