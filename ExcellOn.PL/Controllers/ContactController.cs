﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Controllers
{
    public class ContactController : Controller
    {
        private Repository<ReceiveEmail> repository;
        private IRepository<ContactInformation> infor;
        public ContactController()
        {
            repository = new Repository<ReceiveEmail>();
            infor = new Repository<ContactInformation>();
        }
        public ActionResult Index()
        {
            return View(infor.FindAll().First());
        }
        // POST: Contact
        [HttpPost]
        [ModelValidation]
        public ActionResult Add(ReceiveEmail entity)
        {
            var type = "";
            var message = "";
            entity.CreatedDate = DateTime.Now;
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (ModelState.IsValid)
            {
                try
                {
                    repository.Add(entity);
                    type = "success";
                    message = "Feedback sent successfully!";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }


            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,
                data = entity,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet); ;
        }
    }
}