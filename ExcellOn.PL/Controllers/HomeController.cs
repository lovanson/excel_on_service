﻿
using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Helper;
using ExcellOn.PL.Areas.Admin.Helper.CartHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Controllers
{
    public class HomeController : Controller
    {
        private IRepository<ContactInformation> infor;
        private Repository<DAL.New> newRepo;
        private Repository<DAL.Service> serviceR;
        private Repository<ServiceReview> reviewR;
        private Repository<FavoriteService> favorite;
        private Repository<User> userRepos;

        public HomeController()
        {
            infor = new Repository<ContactInformation>();
            serviceR = new Repository<DAL.Service>();
            newRepo = new Repository<DAL.New>();
            reviewR = new Repository<ServiceReview>();
            favorite = new Repository<FavoriteService>();
            userRepos = new Repository<User>();

        }

        public ActionResult Index()
        {
            var blogs = newRepo.FindAll().OrderByDescending(x => x.CreatedDate).Take(3);
            foreach (var item in blogs)
            {
                item.IdEncrypt = Encrypt.EncryptString(item.Id.ToString());
            }
            ViewBag.Blogs = blogs;
            var Review = reviewR.FindAllByExpress(x => x.Status == 1);
            ViewBag.Review = Review;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Footer()
        {
            var data = infor.FindAll().Count() == 0 ? new ContactInformation() {Address="Excell On",Email="Excellon@gmail.com",Phone="0938934783",Status=true } : infor.FindAllByExpress(x => x.Status).First();
            return PartialView(data);
        }
        public ActionResult _Service()
        {
            return PartialView(serviceR.FindAllByExpress(x => x.Status));
        }
        public ActionResult Header()
        {
            var email = HttpContext.Request.Cookies["email"] == null ? "" : HttpContext.Request.Cookies["email"].Value;
            var user = userRepos.FindOneByExpress(x => x.Email.Equals(email));
            var cart = new Cart();
            ViewBag.totalFavorite = user == null ? 0 : favorite.FindAllByExpress(x => x.UserId == user.Id).Count();
            return PartialView(cart);
        }
        public ActionResult _Header2()
        {
            var email = HttpContext.Request.Cookies["email"] == null ? "" : HttpContext.Request.Cookies["email"].Value;
            var user = userRepos.FindOneByExpress(x => x.Email.Equals(email));
            var cart = new Cart();
            ViewBag.totalFavorite = user==null?0:favorite.FindAllByExpress(x=>x.UserId==user.Id).Count();
            return PartialView(cart);
        }
    }
}