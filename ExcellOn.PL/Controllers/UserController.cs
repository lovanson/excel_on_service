﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        private IRepository<User> userRepos;
        public UserController()
        {
            userRepos = new Repository<User>();
        }
        // Post: Register

        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Register(User entity)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (userRepos.Any(x => x.Email.Equals(entity.Email)))
            {
                ModelState.AddModelError("Email", "Email already exists !");
            }
            if (userRepos.Any(x => x.Phone.Equals(entity.Phone)))
            {
                ModelState.AddModelError("Phone", "Phone already exists !");
            }

            if (ModelState.IsValid)
            {
                try
                {

                    entity.Password = GetMD5(entity.Password);
                    entity.ConfirmPassword = GetMD5(entity.ConfirmPassword);
                    entity.GroupUserId = 2;
                    entity.IsClient = true;
                    entity.Phone = entity.Phone;
                    entity.BirthDay = DateTime.Now;

                    userRepos.Add(entity);
                    type = "success";
                    message = "Please confirm your email to login !";
                    MailHelper.ComfimEmail(entity.Email, entity.Name, "User/ComfirmEmail");


                }
                catch (Exception ex)
                {

                    type = "error";
                    message = ex.Message;
                }
            }
            else
            {

                foreach (var k in ModelState.Keys)
                {
                    foreach (var err in ModelState[k].Errors)
                    {
                        string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                        if (!errors.ContainsKey(key))
                        {
                            errors.Add(key, err.ErrorMessage);
                        }
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,
                data = entity,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null

            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Login()
        {


            return View();
        }

        [HttpPost]
        public ActionResult Login(UserDTOLogin entity)
        {

            if (ModelState.IsValid)
            {
                var Password = GetMD5(entity.Password);
                var user = userRepos.FindOneByExpress(x => x.Email.Equals(entity.Email) && x.Password.Equals(Password) && x.Status.Equals(true) && x.ComfirmEmail == true);
                if (user != null)
                {

                    HttpContext.Response.Cookies["email"].Value = user.Email;
                    return Json(new
                    {
                        statusCode = 200,
                        login = true,
                        type = "success",
                        message = "login successfull",


                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        statusCode = 200,

                        type = "error",
                        message = "login fail !",


                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                Dictionary<string, string> errors = new Dictionary<string, string>();
                foreach (var k in ModelState.Keys)
                {
                    foreach (var err in ModelState[k].Errors)
                    {
                        string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                        if (!errors.ContainsKey(key))
                        {
                            errors.Add(key, err.ErrorMessage);
                        }
                    }
                }
                return Json(new
                {
                    statusCode = 200,


                    errors = errors.Count() > 0 ? errors : null
                }, JsonRequestBehavior.AllowGet); ;
            }


        }

        public ActionResult Logout()
        {
            HttpContext.Response.Cookies["email"].Expires = DateTime.Now.AddDays(-1);
            return RedirectToAction("Login", "User");
        }

        public ActionResult ComfirmEmail(string Email)
        {

            Email = Encrypt.DecryptString(Email);
            var user = userRepos.FindOneByExpress(x => x.Email.Equals(Email) && x.ComfirmEmail == false);
            if (user != null)
            {
                user.ConfirmPassword = user.Password;
                user.ComfirmEmail = true;
                user.Status = true;

                userRepos.Save();

                HttpContext.Response.Cookies["email"].Value = Email;
                HttpContext.Response.Cookies["email"].Expires = DateTime.Now.AddDays(3);
                return RedirectToAction("Index", "Home");
            }



            return RedirectToAction("ClientNotFound", "Errors");

        }
        public ActionResult Profiles(string Email)
        {
            if (String.IsNullOrEmpty(Email))
            {
                return RedirectToAction("Login", "User");
            }
            Email = Encrypt.DecryptString(Email);
            var user = userRepos.FindOneByExpress(x => x.Email.Equals(Email));
            if (user != null)
            {
                return View(user);
            }
            else
            {
                return RedirectToAction("ClientNotFound", "Errors");
            }

        }
        [HttpPut]
        public ActionResult Profiles(UserDTO entity)
        {


            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            var user = userRepos.FindOneById(entity.Id);

            if (entity.PostedFileBase != null && entity.PostedFileBase.ContentLength > 0 && entity.PostedFileBase.FileName != null)
            {
                try
                {
                    entity.Avatar = FileHelper.UploadedFile(entity.PostedFileBase);
                    user.Avatar = entity.Avatar;
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("PostedFileBase", ex.Message);
                }
            }

            if (ModelState.IsValid)
            {
                try
                {

                    user.BirthDay = entity.BirthDay;
                    user.Address = entity.Address;
                    user.Phone = entity.Phone;
                    user.Name = entity.Name;
                    user.Gender = entity.Gender;
                    userRepos.SaveEdit();

                    type = "success";
                    message = "Successfully";
                }
                catch (Exception e)
                {


                    type = "error";
                    message = e.Message;
                }
            }

            else
            {

                foreach (var k in ModelState.Keys)
                {
                    foreach (var err in ModelState[k].Errors)
                    {
                        string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                        if (!errors.ContainsKey(key))
                        {
                            errors.Add(key, err.ErrorMessage);
                        }
                    }
                }
            }
            return Json(new
            {
                statusCode = 200,

                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ChangePassword(UserDTOChangePass entity)
        {
            var type = "success";
            var message = "Successfully";
            Dictionary<string, string> errors = new Dictionary<string, string>();

            if (ModelState.IsValid)
            {
                entity.Password = GetMD5(entity.Password);
                entity.NewPassword = GetMD5(entity.NewPassword);
                var email = HttpContext.Request.Cookies["email"].Value;
                var user = userRepos.FindOneByExpress(x => x.Email.Equals(email) && x.Password.Equals(entity.Password));
                if (user != null)
                {
                    try
                    {
                        user.Password = entity.NewPassword;
                        userRepos.SaveEdit();

                    }
                    catch (Exception e)
                    {
                        type = "error";
                        message = e.Message;

                    }

                }
                else
                {
                    ModelState.AddModelError("Password", "Incorrect password !");
                }

            }

            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }
            return Json(new
            {
                statusCode = 200,

                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PasswordRecovery()
        {
            return View();
        }
        [HttpPut]
        public ActionResult PasswordRecovery(string email)
        {
            var type = "success";
            var message = "Check your email to recover your password";
            if (string.IsNullOrEmpty(email))
            {
                ModelState.AddModelError("email", "This field is required.");
            }
            else
            {
                var user = userRepos.FindOneByExpress(x => x.Email.Equals(email));
                if (user != null)
                {
                    try
                    {
                        MailHelper.Recovery(user.Email, user.Name, "User/RecoveryNewPassword");
                    }
                    catch (Exception e)
                    {
                        type = "error";
                        message = e.Message;

                    }
                }
                else
                {
                    ModelState.AddModelError("email", "Email is not registered account");
                }
            }

            Dictionary<string, string> errors = new Dictionary<string, string>();
            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }
            return Json(new
            {
                statusCode = 200,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RecoveryNewPassword(string email)
        {
            ViewBag.email = email;
            email = Encrypt.DecryptString(email);

            var user = userRepos.FindOneByExpress(x => x.Email.Equals(email));
            if (user != null)
            {

                return View();
            }
            else
            {
                return RedirectToAction("ClientNotFound", "Errors");
            }
        }
        [HttpPost]

        public ActionResult RecoveryNewPassword(string Password, string email)
        {
            try
            {
                email = Encrypt.DecryptString(email);
                var user = userRepos.FindOneByExpress(x => x.Email.Equals(email));
                if (user != null)
                {
                    Password = GetMD5(Password);
                    user.Password = Password;
                    userRepos.SaveEdit();
                    return RedirectToAction("Login");
                }
            }
            catch (Exception e)
            {

                ViewBag.Message = e.Message;
            }
            return View();
        }
        public static string GetMD5(string str)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(str);
            byte[] targetData = md5.ComputeHash(fromData);
            string byte2String = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += targetData[i].ToString("x2");

            }
            return byte2String;
        }

    }
}
