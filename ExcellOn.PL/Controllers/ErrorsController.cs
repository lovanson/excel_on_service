﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Controllers
{
    public class ErrorsController : Controller
    {
        // GET: Errors
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AdminNotFound()
        {
            return View();
        }
        public ActionResult PagesMaintenenceAdmin()
        {
            return View();
        }
        public ActionResult ClientNotFound()
        {
            return View();
        }
        public ActionResult PagesMaintenenceClient()
        {
            return View();
        }
    }
}