﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Helper;
using ExcellOn.PL.Areas.Admin.Helper.CartHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Controllers
{
    public class OrderController : Controller
    {
        private Cart cart;
        private IRepository<DAL.Service> serviceR;
        private IRepository<User> userRepos;
        private IRepository<Payment> paymentRepos;
        private IRepository<Order> orderRepos;
        private IRepository<OrderDetail> orderDetailRepos;
        private IRepository<ExcellBank> excellBank;
        private IRepository<Transaction> transactionRepo;
        public OrderController()
        {
            cart = new Cart();
            serviceR = new Repository<DAL.Service>();
            userRepos = new Repository<User>();
            paymentRepos = new Repository<Payment>();
            orderRepos = new Repository<Order>();
            orderDetailRepos = new Repository<OrderDetail>();
            excellBank = new Repository<ExcellBank>();
            transactionRepo = new Repository<Transaction>();
        }
        // GET: Order
        public ActionResult Index()
        {
            return View(this.cart);
        }
        public ActionResult GetCart()
        {
            return Json(this.cart, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CheckOut(string Email)
        {
            if (String.IsNullOrEmpty(Email))
            {
                return RedirectToAction("Login", "User");
            }
            Email = Encrypt.DecryptString(Email);
            var user = userRepos.FindOneByExpress(x => x.Email.Equals(Email));
            if (user != null)
            {
                return View(this.cart);
            }
            else
            {
                return RedirectToAction("ClientNotFound", "Errors");
            }
            
        }
        [HttpPost]
        public ActionResult CheckOut (Payment entity)
        {
            entity.PaymentAmount = this.cart.TotalPrice;
            var type = "";
            var message = "";
            var Email = HttpContext.Request.Cookies["email"].Value;
            var user = userRepos.FindOneByExpress(x => x.Email.Equals(Email));
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (ModelState.IsValid)
            {
                try
                {
                    paymentRepos.Add(entity);
                    Order order = new Order
                    {
                        PaymentId = entity.Id,
                        TotalPrice = this.cart.TotalPrice,
                        TotalQuantity = this.cart.TotalService,
                        Status=0,
                        UserId = user.Id,
                        CreatedDate=DateTime.Now

                    };
                    orderRepos.Add(order);
                    foreach (var item in this.cart.CartItems)
                    {
                        OrderDetail orderDetail = new OrderDetail
                        {
                            OrderId = order.Id,
                            NumberDates = item.DateQuantity,
                            NumberEmployees = item.EmployeeQuantity,
                            ServiceId = item.Service.Id,
                            Charges = item.Changes,
                            TotalPrice = (item.Changes * item.DateQuantity * item.EmployeeQuantity)
                        };
                        orderDetailRepos.Add(orderDetail);
                    }

                   
                    var bank = excellBank.FindAll().First();
                    bank.AccountBalance += entity.PaymentAmount;
                    excellBank.SaveEdit();
                    Transaction t = new Transaction()
                    {
                         ClientName=user.Name,
                         Money=entity.PaymentAmount,
                         Blance= bank.AccountBalance,
                         Status=true,
                         CreatedDate=DateTime.Now
                    };
                    transactionRepo.Add(t);

                    this.cart.RemoveAll();
                    type = "success";
                    message = "Checkout successfull";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }
            }
            else
            {
                foreach (var k in ModelState.Keys)
                {
                    foreach (var err in ModelState[k].Errors)
                    {
                        string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                        if (!errors.ContainsKey(key))
                        {
                            errors.Add(key, err.ErrorMessage);
                        }
                    }
                }
            }
            return Json(new
            {
                statusCode = 200,
                data = entity,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null

            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ShoppingList(string Email)
        {
            if (String.IsNullOrEmpty(Email))
            {
                return RedirectToAction("Login", "User");
            }
            else
            {
                Email = Encrypt.DecryptString(Email);
                var user = userRepos.FindOneByExpress(x => x.Email.Equals(Email));
                if (user != null)
                {
                    return View(user);
                }
                else
                {
                    return RedirectToAction("ClientNotFound", "Errors");
                }
            }
        }
        [HttpPut]
        public ActionResult AddCartItem(int ServiceId,int NumberDate=1 ,int NumberEmployee=1)
        {
            if (!(NumberDate>=1))
            {
                ModelState.AddModelError("NumberDate", "Error NumberDate");
            }
             if (!(NumberDate>=1))
            {
                ModelState.AddModelError("NumberDate", "Error NumberDate");
            }
            try
            {

                if (ModelState.IsValid)
                {
                    cart.AddCartItem(ServiceId, NumberDate, NumberEmployee);
                    return Json(new
                    {
                        Status = 200,
                        type = "success",
                        message = "Add service successfully",
                        data = cart

                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Dictionary<string, string> errors = new Dictionary<string, string>();
                    foreach (var k in ModelState.Keys)
                    {
                        foreach (var err in ModelState[k].Errors)
                        {
                            string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                            if (!errors.ContainsKey(key))
                            {
                                errors.Add(key, err.ErrorMessage);
                            }
                        }
                    }
                    return Json(new
                    {
                        Status = 200,
                        type = "error",
                        errors=errors,
                        message = "Error quantity !",
                        data = cart

                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {

                return Json(new
                {
                    Status = 200,
                    type = "error",
                    message =e.Message,
                    data = cart

                }, JsonRequestBehavior.AllowGet);
            }
            
        }
        [HttpPut]
        public ActionResult UpdateCartItem(int ServiceId, int DateQuantity = 1, int EmployeeQuantity = 1)
        {
            try
            {
                if (DateQuantity >= 1 && EmployeeQuantity >= 1 && EmployeeQuantity <= 20)
                {
                    cart.UpdateCartItem(ServiceId, DateQuantity, EmployeeQuantity);
                    return Json(new
                    {
                        Status = 200,
                        type = "success",
                        message = "Update cart successfully",
                        data = cart

                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return Json(new
                    {
                        Status = 200,
                        type = "warning",
                        message = "Invalid number of days or employees !",
                        data = cart

                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {

                return Json(new
                {
                    Status = 200,
                    type = "error",
                    message = "Invalid number of days or employees !",
                    data = cart

                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult DeletCartItem(int ServiceId)
        {
            this.cart.RemoveCartItem(ServiceId);
            return RedirectToAction("Index", "Order");
        }
        public ActionResult GetOrders(string Email)
        {
            var user = userRepos.FindOneByExpress(x => x.Email.Equals(Email));
            var order = orderRepos.FindAllByExpress(x => x.UserId == user.Id).OrderByDescending(x=>x.CreatedDate);
            List<ShoppingListDTO> data = new List<ShoppingListDTO>();
            foreach (var item in order)
            {
                data.Add(new ShoppingListDTO()
                {
                    order = item,
                    orderdetails = orderDetailRepos.FindAllByExpress(x => x.OrderId == item.Id).ToList()
                });
            }
            return Json(new
            {
                Status = 200,
                type = "Success",
                message = "Invalid number of days or employees !",
                data = data
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPut]
        public ActionResult updateStatus(int id, byte status)
        {
            string type = "";
            string message = "";
            var order = orderRepos.FindOneByExpress(x=>x.Id==id);
            if (order.Status==0)
            {
                order.Status = status;
                orderRepos.SaveEdit();


                var bank = excellBank.FindAll().First();
                bank.AccountBalance -= order.TotalPrice;
                excellBank.SaveEdit();
                Transaction t = new Transaction()
                {
                    ClientName = order.User.Name,
                    Money = order.TotalPrice,
                    Blance = bank.AccountBalance,
                    Status = false,
                    CreatedDate = DateTime.Now
                };
                transactionRepo.Add(t);
                type = "success";
                message = "Cancel service successfuly  !";


            }
            else
            {
                type = "error";
                message = "This service cannot be canceled  !";
            }
            return Json(new
            {
                Status = 200,
                type = type,
                email = order.User.Email,
                message = message
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPut]
        public ActionResult Repurchase(int Id)
        {
            var order = orderRepos.FindOneById(Id);
            if (order != null)
            {
                foreach (var item in orderDetailRepos.FindAllByExpress(x=>x.OrderId==order.Id))
                {
                    this.cart.AddCartItem(item.Service.Id, item.NumberDates, item.NumberEmployees);
                }
              
                return Json(new
                {
                    type = "success",
                    message = "Successfull",
                    email = Encrypt.EncryptString(order.User.Email),

                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    type = "error",
                    message = "The system is busy, please try again later",


                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}