﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Controllers
{
    public class ServiceReviewController : Controller
    {
        private IRepository<ServiceReview> reviewRepo;
        private IRepository<User> userRepo;
        public ServiceReviewController()
        {
            reviewRepo = new Repository<ServiceReview>();
            userRepo = new Repository<User>();
        }
        // GET: ServiceReview
        [HttpPost]
        public ActionResult Add(ServiceReview entity)
        {

            var email = HttpContext.Request.Cookies["email"].Value;
            var user= userRepo.FindOneByExpress(x=>x.Email.Equals(email));
          
            var mes = "";
            var type = "";
            try
            {
                if (reviewRepo.Any(x => x.ServiceId == entity.ServiceId && x.UserId == user.Id))
                {

                    var serviceReview = reviewRepo.FindOneByExpress(x => x.ServiceId == entity.ServiceId && x.UserId == user.Id);
                    if (serviceReview.isUpdate == true)
                    {
                        mes = "You can only edit a review once!";
                        type = "error";
                    }
                    else
                    {
                        serviceReview.Rate = entity.Rate;
                        serviceReview.Comment = entity.Comment;
                        serviceReview.isUpdate = true;
                        serviceReview.Status = 0;
                        reviewRepo.SaveEdit();
                        mes = "Update Review Service Successfully!";
                        type = "success";
                    }
                }
                else
                {
                    entity.UserId = user.Id;
                    entity.CreatedDate = DateTime.Now;
                    reviewRepo.Add(entity);
                    mes = "Add Review Service Successfully!";
                    type = "success";

                }
            }
            catch (Exception e)
            {

                mes =e.Message;
                type = "error";
            }
            return Json(new
            {
                Status = 200,
                type = type,
                message = mes,
                data = entity

            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FindOne(int id)
        {
            var email = HttpContext.Request.Cookies["email"].Value;
            var user = userRepo.FindOneByExpress(x => x.Email.Equals(email));
            var serviceReview = reviewRepo.FindOneByExpress(x => x.ServiceId == id && x.UserId == user.Id);
            return Json(new
            {
                Status = 200,
                data = serviceReview

            }, JsonRequestBehavior.AllowGet);
        }
    }
}