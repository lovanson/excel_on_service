﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Helper;
using ExcellOn.PL.Areas.Admin.Helper.CartHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Controllers
{
    public class ClientServiceController : Controller
    {
        private IRepository<User> userRepos;
        private IRepository<ClientService> clientserviceRepos;
        private Cart cart;
        public ClientServiceController()
        {
            userRepos = new Repository<User>();
            clientserviceRepos = new Repository<ClientService>();
            cart = new Cart();
        }
        // GET: ClientService
        public ActionResult Index(string email)
        {
            if (String.IsNullOrEmpty(email))
            {
                return RedirectToAction("Login", "User");
            }
            else
            {
                email = Encrypt.DecryptString(email);
                var user = userRepos.FindOneByExpress(x => x.Email.Equals(email));
                var clientService = clientserviceRepos.FindAllByExpress(x => x.UserId == user.Id);
                if (user != null)
                {
                    ViewBag.listService = clientService;
                    return View(user);
                }
                else
                {
                    return RedirectToAction("ClientNotFound", "Errors");
                }
            }
        }

        public ActionResult Repurchase(int ClientServiceId)
        {
            var clientService = clientserviceRepos.FindOneById(ClientServiceId);
            if (clientService!=null)
            {
                this.cart.AddCartItem(clientService.Service.Id, clientService.NumberDates, clientService.NumberEmployees);
                return Json(new
                {
                    type = "success",
                    message = "Successfull",
                    email = Encrypt.EncryptString(clientService.User.Email),

                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    type = "error",
                    message = "The system is busy, please try again later",
                   

                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}