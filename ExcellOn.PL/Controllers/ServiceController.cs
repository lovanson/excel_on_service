﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Controllers
{
    public class ServiceController : Controller
    {
        // GET: Service
        private Repository<DAL.Service> serviceR;
        private Repository<ServiceReview> reviewR;
        private Repository<User> userRepo;
        private IRepository<FavoriteService> favoriteService;
        public ServiceController()
        {
            serviceR = new Repository<DAL.Service>();
            reviewR = new Repository<ServiceReview>();
            userRepo = new Repository<User>();
            favoriteService = new Repository<FavoriteService>();
        }

        public ActionResult Index(string search="")
        {
            var data = serviceR.FindAllByExpress(x=>x.Status);
            if (!String.IsNullOrEmpty(search))
            {
                data = data.Where(x => x.Name.ToLower().Contains(search.ToLower()) || x.ShortDescription.ToLower().Contains(search.ToLower()));
            }
            foreach (var item in data)
            {
                item.IdEncrypt = Encrypt.EncryptString(item.Id.ToString());
            }
            return View(data);
        }
        public ActionResult ServiceDetail(string id)
        {
            try
            {
                var serviceId = Convert.ToInt32(Encrypt.DecryptString(id));

                var email = HttpContext.Request.Cookies["email"] == null ? "" : HttpContext.Request.Cookies["email"].Value;
                var user = userRepo.FindOneByExpress(x => x.Email.Equals(email));

                var listReview = reviewR.FindAllByExpress(x => x.Status != 0 && x.ServiceId == serviceId);
                var countReview = listReview.Count();
                double countRate = 0;
                double count = 0;

                var review = reviewR.FindAllByExpress(x => x.ServiceId == serviceId);
                foreach (var item in review)
                {
                    count++;
                    countRate += item.Rate;
                }
                double mediumrate = countRate / count;
                ViewBag.mediumrate = mediumrate;

                var listService = serviceR.FindAllByExpress(x=>x.Id!=serviceId);
                var data = serviceR.FindOneById(serviceId);
                if (user != null)
                {
                    foreach (var item in listService)
                    {
                        item.IdEncrypt = Encrypt.EncryptString(item.Id.ToString());
                        if (favoriteService.Any(x => x.UserId == user.Id && x.ServiceId == item.Id))
                        {
                            item.IsFavorite = true;
                        }
                    }

                    if (favoriteService.Any(x => x.UserId == user.Id && x.ServiceId == data.Id))
                    {
                        data.IsFavorite = true;
                    }
                }
                ViewBag.listService = listService;
                ViewBag.listReview = listReview;
                ViewBag.coureview = countReview;
                return View(data);

            }
            catch (Exception)
            {

                return RedirectToAction("Index");
            }
            
        }
    }
}