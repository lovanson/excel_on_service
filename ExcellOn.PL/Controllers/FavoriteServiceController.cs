﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Controllers
{
    public class FavoriteServiceController : Controller
    {
        // GET: FavoriteService

        private IRepository<FavoriteService> favoriteService;
        private IRepository<Service> serviceRepos;
        private IRepository<User> userRepos;
        public FavoriteServiceController()
        {
            favoriteService = new Repository<FavoriteService>();
            serviceRepos = new Repository<Service>();
            userRepos = new Repository<User>();
        }
        public ActionResult Index(string Email)
        {
           
            if (String.IsNullOrEmpty(Email))
            {
                return RedirectToAction("Login", "User");
            }
            else
            {

                Email = Encrypt.DecryptString(Email);

                var user = userRepos.FindOneByExpress(x => x.Email.Equals(Email));
                if (user != null)
                {
                   
                    ViewBag.data = favoriteService.FindAllByExpress(x=>x.UserId==user.Id);
                    return View(user);
                }
                else
                {
                    return RedirectToAction("ClientNotFound", "Errors");
                }
            }
        }
        [HttpPost]
        public ActionResult Add(int serviceId)
        {

            var email = HttpContext.Request.Cookies["email"] == null ?"" : HttpContext.Request.Cookies["email"].Value;
            var user = userRepos.FindOneByExpress(x => x.Email.Equals(email));
            var message = "";
            var type = "";
            if (user != null)
            {
                if (favoriteService.Any(x => x.UserId == user.Id && x.ServiceId == serviceId))
                {
                    favoriteService.Delete(favoriteService.FindOneByExpressToEdit(x => x.UserId == user.Id && x.ServiceId == serviceId).Id);
                    type = "success";
                    message = "Removed from favorites";
                }
                else
                {
                    FavoriteService favorite = new FavoriteService();
                    favorite.ServiceId = serviceId;
                    favorite.UserId = user.Id;

                    favoriteService.Add(favorite);
                    type = "success";
                    message = "Added to favorites";
                }
            }
            else
            {
                type = "warning";
                message = "You are not logged in";
            }
            
            
            
            return Json(new
            {
                statusCode = 200,
                type= type,
                message = message,
                count = user == null ? 0 : favoriteService.FindAllByExpress(x => x.UserId == user.Id).Count()
            }, JsonRequestBehavior.AllowGet);
        } 
        [HttpDelete]
        public ActionResult Delete(int Id)
        {
            try
            {
                favoriteService.Delete(Id);
                return Json(new
                {
                    statusCode = 200,
                    type = "success",
                    message = "Removed from favorites"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new
                {
                    statusCode = 200,
                    type = "error",
                    message = e.Message
                }, JsonRequestBehavior.AllowGet);
            }
            
        }

    }
}