﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Filter;
using ExcellOn.PL.Areas.Admin.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Controllers
{
    public class ProductClientController : Controller
    {
        private IRepository<User> userRepos;
        private IRepository<ProductClient> proclRepos;
        public ProductClientController()
        {
            userRepos = new Repository<User>();
            proclRepos = new Repository<ProductClient>();
        }
        // GET: ProductClient
        public ActionResult Index(string Email)
        {
            Email = Encrypt.DecryptString(Email);
            var user = userRepos.FindOneByExpress(x => x.Email.Equals(Email));
            if (user != null)
            {
                return View(user);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult GetAll(int page = 1, string search = "")
        {
            var data = proclRepos.FindAll();

            if (!string.IsNullOrEmpty(search))
            {
                data = data.Where(x => (x.Name.ToLower().Contains(search.ToLower())) || (x.Description.ToLower().Contains(search.ToLower()))).ToList();
            }

            foreach (var item in data)
            {
                item.IdEncrypt = Encrypt.EncryptString(item.Id.ToString());
            }
            var pageSize = 5;

            int totalPage = (int)Math.Ceiling((double)data.Count() / pageSize);
            return Json(new
            {
                statusCode = 200,
                page = page,
                search = search,
                totalPage = totalPage,
                pageSize = pageSize,
                data = data.Skip((page - 1) * pageSize).Take(pageSize).OrderBy(x => x.Name)
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ModelValidation]
        public ActionResult Save(DAL.ProductClient entity)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            try
            {
                entity.Image = FileHelper.UploadedFile(entity.PostedFileBase);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("PostedFileBase", ex.Message);
            }

            if (ModelState.IsValid)
            {
                var email = HttpContext.Request.Cookies["email"].Value;

                try
                {
                    entity.UserId = userRepos.FindOneByExpress(x => x.Email.Equals(email)).Id;
                    proclRepos.Add(entity);
                    type = "success";
                    message = "Successfully";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;

                }

            }
            else
            {

                foreach (var k in ModelState.Keys)
                {
                    foreach (var err in ModelState[k].Errors)
                    {
                        string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                        if (!errors.ContainsKey(key))
                        {
                            errors.Add(key, err.ErrorMessage);
                        }
                    }
                }
            }

            return Json(new
            {
                statusCode = HttpStatusCode.OK,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet); ;
        }
        [HttpPut]
        public ActionResult Edit(DAL.ProductClient entity)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (entity.PostedFileBase != null && entity.PostedFileBase.ContentLength > 0)
            {
                try
                {
                    entity.Image = FileHelper.UploadedFile(entity.PostedFileBase);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("PostedFileBase", ex.Message);
                }
            }
            else
            {
                entity.Image = proclRepos.FindOneByExpressToEdit(x => x.Id == entity.Id).Image;
            }
            if (ModelState.IsValid)
            {
                var email = HttpContext.Request.Cookies["email"].Value;
                try
                {
                    entity.UserId = userRepos.FindOneByExpress(x => x.Email.Equals(email)).Id;
                    proclRepos.Edit(entity);
                    type = "success";
                    message = "Successfully";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }

            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,

                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int IdEncrypt)
        {
            try
            {

                proclRepos.Delete(IdEncrypt);

                return Json(new
                {
                    status = 200,
                    type = "success",
                    message = "Delete successfully ",

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new
                {
                    status = 200,
                    type = "error",
                    message = e.Message

                }, JsonRequestBehavior.AllowGet); ;

            }
        }
        public ActionResult GetOne(int IdEncrypt)
        {
            try
            {
                var service = proclRepos.FindOneById(IdEncrypt);

                return Json(new
                {
                    statusCode = 200,
                    data = service,

                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception)
            {

                return Json(new
                {
                    statusCode = 200,
                    type = "error",
                    message = "The system is busy, please reload the page !",

                }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}