﻿$(function () {

    var readURL = function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".file-upload").on('change', function () {
        readURL(this);
    });

    $(".upload-button").on('click', function () {
        $(".file-upload").click();
    });
});

jQuery.validator.addMethod("phone", function (phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, "");
    return this.optional(element) || phone_number.length > 9 &&
        phone_number.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/gm);
}, "Phone number is not valid");

var validator = $("#general-info").validate({
    rules: {
        Name: "required",

        Phone: {
            required: true,
            phone: true
        },
        Address: "required",
        BirthDay: "required",

    },
    messages: {
        Name: "The Name field is required.",
        Address: "The Name field is required.",
        BirthDay: "The BirthDay field is required.",

        Phone: {
            required: "The Phone field is required.",


        }
    }
});
$('#general-info').submit(function (e) {
    e.preventDefault();



    let formdata = new FormData();
    formdata.append("Id", $('#Id').val())
    formdata.append("Name", $('#Name').val())

    formdata.append("BirthDay", $('#BirthDay').val())
    formdata.append("Phone", $('#Phone').val())
    formdata.append("Address", $('#Address').val())
    formdata.append("Gender", (parseInt($('input[name="Gender"]:checked').val()) == 1) ? true : false)




    formdata.append("PostedFileBase", $('#PostedFileBase')[0].files[0] || new Blob())
    $.ajax({
        type: "PUT",
        url: "/User/Profiles",
        data: formdata,
        contentType: false,
        processData: false,
        success: function (res) {
            console.log(res);
            if (!res.errors) {
                swal({
                    title: res.message,
                    text: "You clicked the!",
                    type: res.type,
                    padding: '2em'
                }).then(function (res) {

                    location.reload();
                })
                validator.resetForm();

            } else {
                validator.showErrors(res.errors);
            }
        }
    })
})

var validateFormPass = $('#form-validate-password').validate({
    rules: {
        Password: {
            required: true,
            minlength: 8,

        },
        NewPassword: {
            required: true,
            minlength: 8,

        },
        ComfirmPassword: {
            required: true,
            minlength: 8,
            equalTo: "#NewPassword"
        }
    }
});

$('#form-validate-password').submit(function (e) {
    e.preventDefault()
    $.ajax({
        type: "POST",
        url: "/User/ChangePassword",
        data: $('#form-validate-password').serialize(),
        success: function (res) {
          
            if (res.errors) {
                validateFormPass.showErrors(res.errors);

            } else {

                swal({
                    title: res.message,
                    text: "You clicked the!",
                    type: res.type,
                    padding: '2em'
                }).then(function (res) {

                    location.reload();
                })

            }
        }
    });
})