﻿$(function () {
    
    $('#tbl-using').DataTable();
    $('#tbl-expired').DataTable();
    $('.review').click(function () {
        var id = $(this).data('id');
        console.log(id);
        $('#exampleModalCenter').modal('show');
        $('#ServiceId').val(id);
        $.ajax({
            type: 'GET',
            url: '/ServiceReview/FindOne',
            data: { id: id },
            success: function (res) {
                if (res.data.Rate == 1) {
                    $('.rate1').click()
                } else if (res.data.Rate == 2) {
                    $('.rate2').click()
                } else if (res.data.Rate == 3) {
                    $('.rate3').click()
                } else if (res.data.Rate == 4) {
                    $('.rate4').click()
                } else if (res.data.Rate == 5) {
                    $('.rate5').click()
                }

                $('#Comment').val(res.data.Comment)
            }
        })
    });
    $("#postReview").click(function () {
        //formdata = formdata.replace("id=&", '');
        var obj = {
            ServiceId: $('#ServiceId').val(),
            Rate: $('.rate').val(),
            Comment: $('#Comment').val(),
            Status: $('#Status').val()
        }

        $.ajax({
            type: "Post",
            url: '/ServiceReview/Add',
            data: { entity: obj },
            success: function (res) {
                swal({
                    title: res.message,
                    text: "You clicked the!",
                    type: res.type,
                    padding: '2em'
                })
                $('#ServiceId').val('')
                $('.rate').val('')
                $('#Comment').val('')
                $('#Status').val('')
                $('#exampleModalCenter').modal('toggle');

            }
        })


    })

    $('.close').click(function () {
        $('.rate').val('')
        $('#Comment').val('')
        $('#exampleModalCenter').modal('toggle');
    });

    $('.btn-repurchase').click(function () {
        var clientServiceId = $(this).data('id');


        swal({
            title: 'Are you sure?',
            text: "Do you want to rent this service again !",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Confirm',
            padding: '2em'
        }).then(function (result) {
            if (result.value) {

                $.ajax({
                    type: "POST",
                    url: "/ClientService/Repurchase",
                    data: { ClientServiceId: clientServiceId },
                    success: function (res) {
                        if (res.email) {
                            swal({
                                title: res.message,
                                text: "You clicked the!",
                                type: res.type,
                                padding: '2em'
                            }).then(function () {
                                window.location.href = "/Order/CheckOut?Email=" + res.email
                            })
                        } else {
                            swal({
                                title: res.message,
                                text: "You clicked the!",
                                type: res.type,
                                padding: '2em'
                            })
                        }
                    }
                })
            }
        })
    })

})