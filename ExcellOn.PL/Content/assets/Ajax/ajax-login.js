﻿$(function () {


    var validatorLogin = $("#login-form").validate({
        rules: {
            Password: {
                required: true,
                minlength: 8,
            },

            Email: {
                required: true,
                email: true
            }
        }

    });
    $("#login-form").submit(function (e) {

        e.preventDefault();


        $.ajax({
            type: "POST",
            url: "/User/Login",
            data: $(this).serialize(),
            success: function (res) {

                if (!res.errors) {
                    if (res.login) {
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        }).then(function (res) {


                            window.location = "/Home"

                        })
                    } else {
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                    }

                } else {
                    validatorLogin.showErrors(res.errors);

                }
            }
        })


    });
    jQuery.validator.addMethod("phone", function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.length > 9 &&
            phone_number.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/gm);
    }, "Phone number is not valid");

    var validatorRegister = $("#register-form").validate({
        rules: {
            RePassword: {
                required: true,
                minlength: 8,
            },
            Name: "required",
            Email: {
                required: true,
                email: true
            },

            Phone: {
                required: true,
                phone: true
            },
            ConfirmPassword: {
                required: true,
                minlength: 8,
                equalTo: "#RePassword"
            }
        }

    });
    $('#register-form').submit(function (e) {

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/User/Register",
            data: $(this).serialize(),
            success: function (res) {
               
                if (res.errors) {
                    validatorRegister.showErrors(res.errors);
                } else {
                    if (res.type == "success") {
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        }).then(function (res) {

                            location.reload();
                        })
                    } else {
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                    }
                }
            }
        })
    })
})