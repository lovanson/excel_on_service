﻿$(function () {
    function setInputFilter(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {

            textbox.addEventListener(event, function () {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = 1;
                }
            });

        });
    }

    setInputFilter(document.getElementById("AccountNumber"), function (value) {
        return /^\d*$/.test(value) && (value === "" || parseInt(value) >= 0);
    });
    setInputFilter(document.getElementById("Identity"), function (value) {
        return /^\d*$/.test(value) && (value === "" || parseInt(value) >= 0);
    });


    var validator = $("#form-order").validate({
        rules: {
            BankName: "required",

            AccountNumber: {
                required: true,
                number: true,
                rangelength: [10, 13]
            },
            ReleaseDate: "required",
            ExpirationDate: "required",
            Identity: {
                required: true,
                number: true,
                rangelength: [12, 13]
            }
        }
    });

    $('#form-order').submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/Order/CheckOut",
            data: $(this).serialize(),
            success: function (res) {
              
                if (!res.errors) {

                    swal({
                        title: res.message,
                        text: "You clicked the!",
                        type: res.type,
                        padding: '2em'
                    }).then(function (res) {


                        window.location = "/Home"

                    })


                } else {
                    validator.showErrors(res.errors);

                }
            }
        })
    })
})