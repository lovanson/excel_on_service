﻿$(function () {
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',

        // These options are needed to round to whole numbers if that's what you want.
        //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
        //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
    });
    function setInputFilter(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {

            textbox.addEventListener(event, function () {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = 1;
                }
            });

        });
    }

    setInputFilter(document.getElementById("DateNumber"), function (value) {
        return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 500 && parseInt(value) >= 1);
    });
    setInputFilter(document.getElementById("EmployeeNumber"), function (value) {
        return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 20 && parseInt(value) >= 1);
    });
    var validateform = $('#form-add-service').validate({
        rules: {
            DateNumber: {
                required: true,
                digits: true,
                min: 1

            },
            EmployeeNumber: {
                required: true,
                digits: true,
                min: 1,
                max: 20

            }
        }
    })

    $('#DateNumber').change(function () {
        if ($(this).val() === "") {
            $(this).val(1)
        }
        let dateNumber = $('#DateNumber').val()
        let employeeNumber = $('#EmployeeNumber').val()
        let changes = $('#Changes').val()
        var regex = /^[0-9]+$/;
        if (dateNumber.match(regex) && employeeNumber.match(regex) && changes.match(regex)) {
            $('#TotalPrice').val(dateNumber * employeeNumber * changes)
        }
    })
    $('#EmployeeNumber').change(function () {
        if ($(this).val() === "") {
            $(this).val(1)
        }
        let dateNumber = $('#DateNumber').val()
        let employeeNumber = $('#EmployeeNumber').val()
        let changes = $('#Changes').val()
        var regex = /^[0-9]+$/;
        if (dateNumber.match(regex) && employeeNumber.match(regex) && changes.match(regex)) {
            $('#TotalPrice').val(dateNumber * employeeNumber * changes)
        }
    })

    var Cart = {
        add: function (ServiceId, NumberDate, NumberEmployee) {
            $.ajax({
                type: "PUT",
                url: "/Order/AddCartItem",
                data: { ServiceId: ServiceId, NumberDate: NumberDate, NumberEmployee: NumberEmployee },
                success: function (res) {
                    console.log(res)
                    if (res.errors) {
                        validateform.showErrors(res.errors);
                    } else {
                        $('#modal-add-service').modal('hide');
                        $('#EmployeeNumber').val(1)
                        $('#DateNumber').val(1)
                        validateform.resetForm()
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                        Cart.load()

                    }
                }
            })
        },
        load: function () {
            $.ajax({
                type: "GET",
                url: "/Order/GetCart",

                success: function (res) {
                    console.log(res)
                    let td = ``
                    for (let item of res.CartItems) {
                        let change = formatter.format(item.Changes)
                        td += `<div class="cart-product-wrapper mb-6">

                                            <!-- Single Cart Product Start -->
                                            <div class="single-cart-product">
                                                <div class="cart-product-thumb">
                                                    <a href="/Service/ServiceDetail/${item.ServiceId}">
                                                        <img src="/Areas/Admin/Content/Images/${item.Service.Image}" />

                                                    </a>
                                                </div>
                                                <div class="cart-product-content">
                                                    <h3 class="title"><a href="/Service/ServiceDetail/${item.ServiceId}">${item.Service.Name}</a></h3>
                                                    <span class="price">
                                                        <span>${change}</span>
                                                    </span>
                                                </div>
                                            </div>
                                            <!-- Single Cart Product End -->
                                            <!-- Product Remove Start -->
                                            <div class="cart-product-remove">
                                                <a href="href="/Order/DeletCartItem?ServiceId=${item.Service.Id}"><i class="ri-delete-bin-line"></i></a>
                                            </div>
                                            <!-- Product Remove End -->

                                        </div>`
                    }
                    $('.list-mini-cart').html(td);

                    $('.cart-product-total').find('.price').text(formatter.format(res.TotalPrice))
                    $('.total-cart').text(res.TotalService)
                }
            })
        },
    }

    $('#DateNumber').change(function () {
        let dateNumber = $('#DateNumber').val()
        let employeeNumber = $('#EmployeeNumber').val()
        let changes = $('#Changes').val()
        var regex = /^[0-9]+$/;
        if (dateNumber.match(regex) && employeeNumber.match(regex) && changes.match(regex)) {
            $('#TotalPrice').val(dateNumber * employeeNumber * changes)
        }
    })
    $('#EmployeeNumber').change(function () {
        let dateNumber = $('#DateNumber').val()
        let employeeNumber = $('#EmployeeNumber').val()
        let changes = $('#Changes').val()
        var regex = /^[0-9]+$/;
        if (dateNumber.match(regex) && employeeNumber.match(regex) && changes.match(regex)) {
            $('#TotalPrice').val(dateNumber * employeeNumber * changes)
        }
    })

    $('.btn-add').click(function () {
        $('#modal-add-service').modal('show');
    });
    $('#close-modal').click(function () {
        $('#modal-add-service').modal('hide');
    });


    $('#form-add-service').submit(function (e) {
        e.preventDefault();
        let ServiceId = $('#ServiceId').val();
        let NumberDate = $('#DateNumber').val();
        let NumberEmployee = $('#EmployeeNumber').val();

        Cart.add(ServiceId, NumberDate, NumberEmployee)

    })
    $('.btn-add-cartItem-fast').click(function () {
        let ServiceId = $(this).attr("data-serviceId")
        Cart.add(ServiceId, 1, 1)
    })

    $('.add-wishlist').on('click', function () {
        var color = $(this).css('color');
        var id = $(this).data("id")
        if (color == 'rgb(255, 0, 0)') {
            $(this).css('color', '#5A5858');
        } else {
            $(this).css('color', 'red');
        }

        $.ajax({
            type: "POST",
            url: "/FavoriteService/Add",
            data: { serviceId: id },
            success: function (res) {
                swal({
                    title: res.message,
                    text: "You clicked the!",
                    type: res.type,
                    padding: '2em'
                })
                $('.total-withlist').text(res.count)
            }

        });
    })
})