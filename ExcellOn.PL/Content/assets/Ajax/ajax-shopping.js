﻿$(function () {

    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
    });
    var order = {
        load: function (email) {
            $.ajax({
                type: "GET",
                url: "/Order/GetOrders",
                data: { Email: email },
                success: function (res) {
                    let html = '';
                    let htmlUnconfimred = '';
                    let htmlConfirmed = '';
                    let htmlCancelled = '';
                    let status = '';
                    for (let item of res.data) {
                        if (item.order.Status == 0) {
                            status = "Pending";
                            htmlUnconfimred += `<div class="tabcontent-body">
                                                    <div class="tabcontent-body-top-header">
                                                        <div class="text-status">${status}</div>
                                                    </div>
                                                <div class="border-t"></div>`
                            for (let item2 of item.orderdetails) {

                                htmlUnconfimred += `<div class="tabcontent-body-top">
                                                    <div class="tabcontent-body-top-fooder">
                                                         <div class="image-service">
                                                            <img src="/Areas/Admin/Content/Images/${item2.Service.Image}" />
                                                        </div>
                                                        <div class="name-service">
                                                            <div class="name">${item2.Service.Name}</div>
                                                            <div class="quantity"><p><span class="unit">x</span>${item2.NumberEmployees}  <span class="unit">Employee</span><span class="unit">&ensp;x</span> ${item2.NumberDates}  <span class="unit">Date</span> </p></div>
                                                        </div>
                                                        <div class="price-service">
                                                            ${formatter.format(item2.Charges)}
                                                        </div>
                                                    </div>
                                                 </div>
                                        `;
                            }
                            htmlUnconfimred += `
                                                                    <div class="border">
                                                                        </div><div class="tabcontent-body-bot">
                                                                            <div class="totalprice">
                                                                                <div class="totalprice-text">Total Price:</div>
                                                                                <div class="totalprice-number"> ${formatter.format(item.order.TotalPrice)}</div>
                                                                            </div>
                                                                        </div>
                                                                         <div class="tabcontent-body-end">
                                                                            <div class="groupbutton">
                                                                               <button type="submit" class="btn btn-outline-danger cancel" data-id="${item.order.Id}">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                        `;

                        } else if (item.order.Status == 1) {
                            status = "Success";
                            htmlConfirmed += `<div class="tabcontent-body">
                                                <div class="tabcontent-body-top-header">
                                                    <div class="text-status">${status}</div>
                                                </div>
                                                <div class="border-t"></div>`
                            for (let item2 of item.orderdetails) {

                                htmlConfirmed += `<div class="tabcontent-body-top">
                                                    <div class="tabcontent-body-top-fooder">
                                                         <div class="image-service">
                                                            <img src="/Areas/Admin/Content/Images/${item2.Service.Image}" />
                                                        </div>
                                                        <div class="name-service">
                                                            <div class="name">${item2.Service.Name}</div>
                                                            <div class="quantity"><p><span class="unit">x</span>${item2.NumberEmployees}  <span class="unit">Employee</span><span class="unit">&ensp;x</span> ${item2.NumberDates}  <span class="unit">Date</span> </p></div>
                                                        </div>
                                                        <div class="price-service">
                                                            ${formatter.format(item2.Charges)}
                                                        </div>
                                                    </div>
                                                 </div>
                                        `;
                            }
                            htmlConfirmed += `
                                                                    <div class="border">
                                                                        </div><div class="tabcontent-body-bot">
                                                                            <div class="totalprice">
                                                                                <div class="totalprice-text">Total Price:</div>
                                                                                <div class="totalprice-number"> ${formatter.format(item.order.TotalPrice)}</div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                        `;

                        } else if (item.order.Status == 2) {
                            status = "Cancelled";
                            htmlCancelled += `<div class="tabcontent-body">
                                                <div class="tabcontent-body-top-header">
                                                    <div class="text-status">${status}</div>
                                                </div>
                                                <div class="border-t"></div>`
                            for (let item2 of item.orderdetails) {

                                htmlCancelled += `<div class="tabcontent-body-top">
                                                    <div class="tabcontent-body-top-fooder">
                                                         <div class="image-service">
                                                            <img src="/Areas/Admin/Content/Images/${item2.Service.Image}" />
                                                        </div>
                                                        <div class="name-service">
                                                            <div class="name">${item2.Service.Name}</div>
                                                            <div class="quantity"><p><span class="unit">x</span>${item2.NumberEmployees}  <span class="unit">Employee</span><span class="unit">&ensp;x</span> ${item2.NumberDates}  <span class="unit">Date</span> </p></div>
                                                        </div>
                                                        <div class="price-service">
                                                            ${formatter.format(item2.Charges)}
                                                        </div>
                                                    </div>
                                                 </div>
                                        `;
                            }
                            htmlCancelled += `
                                                                    <div class="border">
                                                                        </div><div class="tabcontent-body-bot">
                                                                            <div class="totalprice">
                                                                                <div class="totalprice-text">Total Price:</div>
                                                                                <div class="totalprice-number"> ${formatter.format(item.order.TotalPrice)}</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="tabcontent-body-end">
                                                                            <div class="groupbutton">
                                                                                <button type="button" class="btn btn-outline-success repurchase"  data-id="${item.order.Id}">Repurchase</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                        `;

                        }
                        html += `<div class="tabcontent-body">
                                                <div class="tabcontent-body-top-header">
                                                    <div class="text-status">${status}</div>
                                                </div>
                                                <div class="border-t"></div>`
                        for (let item2 of item.orderdetails) {

                            html += `<div class="tabcontent-body-top">
                                                    <div class="tabcontent-body-top-fooder">
                                                         <div class="image-service">
                                                            <img src="/Areas/Admin/Content/Images/${item2.Service.Image}" />
                                                        </div>
                                                        <div class="name-service">
                                                            <div class="name">${item2.Service.Name}</div>
                                                            <div class="quantity"><p><span class="unit">x</span>${item2.NumberEmployees}  <span class="unit">Employee</span><span class="unit">&ensp;x</span> ${item2.NumberDates}  <span class="unit">Date</span> </p></div>
                                                        </div>
                                                        <div class="price-service">
                                                            ${formatter.format(item2.Charges)}
                                                        </div>
                                                    </div>
                                                 </div>
                                        `;
                        }
                        html += `
                                                                    <div class="border">
                                                                        </div><div class="tabcontent-body-bot">
                                                                            <div class="totalprice">
                                                                                <div class="totalprice-text">Total Price:</div>
                                                                                <div class="totalprice-number"> ${formatter.format(item.order.TotalPrice)}</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="tabcontent-body-end">
                                                                            <div class="groupbutton">`
                        if (status == "Cancelled") {
                            html += ` <button type="submit" class="btn btn-outline-success repurchase"  data-id="${item.order.Id}">Repurchase</button>`;
                        } else if (status == "Pending") {
                            html += ` <button type="submit" class="btn btn-outline-danger cancel" data-id="${item.order.Id}">Cancel</button>`;

                        }



                        html += `
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                        `;

                    }
                    $('#add-all').html(html);
                    $('#add-unconfirmred').html(htmlUnconfimred);
                    $('#add-Confirmed').html(htmlConfirmed);
                    $('#add-cancelled').html(htmlCancelled);
                }
            });
        }
    }
    $(document).on('click', '.cancel', function () {
        var id = $(this).data('id');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            padding: '2em'
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: 'PUT',
                    url: '/Order/updateStatus',
                    data: { id: id, status: 2 },
                    success: function (res) {


                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                        order.load(res.email);
                    }
                });

            }
        })

    });
    $(document).on('click', '.repurchase', function () {
        var id = $(this).data('id');


        swal({
            title: 'Are you sure?',
            text: "Do you want to rent this order again !",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Confirm',
            padding: '2em'
        }).then(function (result) {
            if (result.value) {

                $.ajax({
                    type: "PUT",
                    url: "/Order/Repurchase",
                    data: { Id: id },
                    success: function (res) {
                        if (res.email) {
                            swal({
                                title: res.message,
                                text: "You clicked the!",
                                type: res.type,
                                padding: '2em'
                            }).then(function () {
                                window.location.href = "/Order/CheckOut?Email=" + res.email
                            })
                        } else {
                            swal({
                                title: res.message,
                                text: "You clicked the!",
                                type: res.type,
                                padding: '2em'
                            })
                        }
                    }
                })
            }
        })
    })
   
    
})