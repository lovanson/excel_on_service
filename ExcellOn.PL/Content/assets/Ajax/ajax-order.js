﻿$(function () {
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',

        // These options are needed to round to whole numbers if that's what you want.
        //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
        //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
    });
    var Cart = {
        load: function () {
            $.ajax({
                type: "GET",
                url: "/Order/GetCart",

                success: function (res) {
                  

                    for (let item of res.CartItems) {

                        let total = formatter.format(item.Changes * item.DateQuantity * item.EmployeeQuantity)
                        $('.subtotal-amount[data-serviceid="' + item.Service.Id + '"]').find('span').text(total);

                    }

                    $('.total-price-cart').html(formatter.format(res.TotalPrice))
                    $('.subtotal-price-cart').html(formatter.format(res.TotalPrice))
                    $('.cart-product-total').find('.price').text(formatter.format(res.TotalPrice))
                    $('.total-cart').text(res.TotalService)
                }
            })
        },
        updateCart: function (ServiceId, DateQuantity, EmployeeQuantity) {
            $.ajax({
                type: "PUT",
                url: "/Order/UpdateCartItem",
                data: { ServiceId: ServiceId, DateQuantity: DateQuantity, EmployeeQuantity: EmployeeQuantity },
                success: function (res) {
                    swal({
                        title: res.message,
                        text: "You clicked the!",
                        type: res.type,
                        padding: '2em'
                    })

                    Cart.load();
                }
            })
        }
    }
    function setInputFilter(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {
            for (let item of textbox) {
                item.addEventListener(event, function () {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    } else {
                        this.value = 1;
                    }
                });
            }
        });
    }

    setInputFilter(document.getElementsByClassName("DateQuantity"), function (value) {
        return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 500 && parseInt(value) >= 1);
    });
    setInputFilter(document.getElementsByClassName("EmployeeQuantity"), function (value) {
        return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 20 && parseInt(value) >= 1);
    });

    $('.DateQuantity').change(function () {

        let ServiceId = $(this).attr("data-serviceId");
        if ($(this).val() === "") {
            $(this).val(1)
        }
        let DateQuantity = $(this).val();


        let EmployeeQuantity = $('.input-counter').find('input[data-serviceId="' + ServiceId + '"].EmployeeQuantity').val();

        Cart.updateCart(ServiceId, DateQuantity, EmployeeQuantity);
    })
    $('.EmployeeQuantity').change(function () {
        let ServiceId = $(this).attr("data-serviceId");
        if ($(this).val() === "") {
            $(this).val(1)
        }
        let EmployeeQuantity = $(this).val();


        let DateQuantity = $('.input-counter').find('input[data-serviceId="' + ServiceId + '"].DateQuantity').val();

        Cart.updateCart(ServiceId, DateQuantity, EmployeeQuantity);
    })

})