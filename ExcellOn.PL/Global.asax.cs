﻿
using ExcellOn.PL.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ExcellOn.PL
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_Error(object sender, EventArgs e)
        {

            string area = Request.FilePath.Split('/')[1];
            Exception exception = Server.GetLastError();
            Response.Clear();
            HttpException httpException = exception as HttpException;
            RouteData route = new RouteData();
            route.Values.Add("controller", "Errors");
            if (area.Equals("Admin"))
            {

             
                if (httpException != null)
                {
                    switch (httpException.GetHttpCode())
                    {
                        case 404:
                            route.Values.Add("action", "AdminNotFound");
                            break;
                       
                        default:
                            route.Values.Add("action", "PagesMaintenenceAdmin");
                            break;
                    }
                  
                }

            }
            else
            {
                if (httpException != null)
                {
                    switch (httpException.GetHttpCode())
                    {
                        case 404:
                            route.Values.Add("action", "ClientNotFound");
                            break;
                     
                        default:
                            route.Values.Add("action", "PagesMaintenenceClient");
                            break;
                    }

                }
            }
            Server.ClearError();
            Response.TrySkipIisCustomErrors = true;
            IController errController = new ErrorsController();
            errController.Execute(new RequestContext(new HttpContextWrapper(Context), route));


        }
    }
}
