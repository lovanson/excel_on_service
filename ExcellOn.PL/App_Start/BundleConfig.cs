﻿using System.Web;
using System.Web.Optimization;

namespace ExcellOn.PL
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));
            bundles.Add(new StyleBundle("~/Admin/css").Include(
                  "~/Areas/Admin/Content/assets/css/loader.css",


                    "~/Areas/Admin/Content/bootstrap/css/bootstrap.min.css",
                    "~/Areas/Admin/Content/assets/css/plugins.css",

                     "~/Areas/Admin/Content/plugins/font-icons/fontawesome/css/regular.css",
                     "~/Areas/Admin/Content/plugins/font-icons/fontawesome/css/fontawesome.css",
                    "~/Areas/Admin/Content/bootstrap/css/style.css",
                    "~/Areas/Admin/Content/toastr/toastr.min.css",
                    "~/Areas/Admin/Content/plugins/sweetalerts/sweetalert2.min.css",
                    "~/Areas/Admin/Content/plugins/sweetalerts/sweetalert.css"
                   ));
            bundles.Add(new ScriptBundle("~/Admin/js").Include(
                      "~/Areas/Admin/Content/assets/js/loader.js",
                        "~/Areas/Admin/Content/assets/js/libs/jquery-3.1.1.min.js",
                        "~/Areas/Admin/Content/bootstrap/js/popper.min.js",
                        "~/Areas/Admin/Content/bootstrap/js/bootstrap.min.js",
                        "~/Areas/Admin/Content/plugins/perfect-scrollbar/perfect-scrollbar.min.js",
                        "~/Areas/Admin/Content/assets/js/app.js",
                        "~/Areas/Admin/Content/assets/js/custom.js",


                        "~/Areas/Admin/Content/toastr/toastr.js",
                        "~/Areas/Admin/Content/bootstrap/js/validate.min.js",
                        "~/Areas/Admin/Content/plugins/sweetalerts/sweetalert2.min.js",
                        "~/Areas/Admin/Content/plugins/sweetalerts/custom-sweetalert.js"


                      ));
            bundles.Add(new ScriptBundle("~/Content/assets/js").Include(
                    "~/Content/assets/js/jquery.min.js",
                    "~/Areas/Admin/Content/toastr/toastr.js",
                    "~/Areas/Admin/Content/bootstrap/js/validate.min.js",
                    "~/Content/assets/js/bootstrap.bundle.min.js",
                    "~/Content/assets/js/jquery.meanmenu.js",
                    "~/Content/assets/js/owl.carousel.min.js",
                    "~/Content/assets/js/jquery.appear.js",

                    "~/Content/assets/js/odometer.min.js",

                    "~/Content/assets/js/nice-select.min.js",

                    "~/Content/assets/js/jquery.magnific-popup.min.js",

                    "~/Content/assets/js/fancybox.min.js",

                    "~/Content/assets/js/TweenMax.min.js",

                    "~/Content/assets/js/scrollbar.min.js",

                    "~/Content/assets/js/horizontal-scrollbar.min.js",

                    "~/Content/assets/js/jquery.ajaxchimp.min.js",

                    "~/Content/assets/js/form-validator.min.js",

                    "~/Content/assets/js/contact-form-script.js",

                    "~/Content/assets/js/wow.min.js",

                    "~/Content/assets/js/main.js",
                    "~/Areas/Admin/Content/plugins/sweetalerts/sweetalert2.min.js",
                    "~/Areas/Admin/Content/plugins/sweetalerts/custom-sweetalert.js"


                     ));



            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"
                      ));
            bundles.Add(new StyleBundle("~/Content/assets/css").Include(

                     "~/Content/assets/css/bootstrap.min.css",
                     "~/Content/assets/css/animate.min.css",
                     "~/Content/assets/css/meanmenu.css",
                     "~/Content/assets/css/remixicon.css",
                     "~/Content/assets/css/odometer.min.css",
                     "~/Content/assets/css/nice-select.min.css",
                     "~/Content/assets/css/owl.carousel.min.css",
                     "~/Content/assets/css/owl.theme.default.min.css",
                     "~/Content/assets/css/magnific-popup.min.css",
                     "~/Content/assets/css/fancybox.min.css",
                     "~/Content/assets/css/style.css",
                     "~/Content/assets/css/responsive.css",

                     "~/Areas/Admin/Content/toastr/toastr.min.css",
                     "~/Areas/Admin/Content/plugins/sweetalerts/sweetalert2.min.css",
                     "~/Areas/Admin/Content/plugins/sweetalerts/sweetalert.css"
                     ));

        }
    }
}
