﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using System.Web.Mvc;


namespace ExcellOn.PL.Areas.Admin.Filter
{
    public class ModelValidationAttribute : ActionFilterAttribute
{
    public override void OnActionExecuting(System.Web.Mvc.ActionExecutingContext filterContext)
    {
        // get the controller for access to the ModelState dictionary
        var controller = filterContext.Controller as Controller;
        if(controller != null)
        {
            var modelState = controller.ModelState;

            // get entities that could have validation attributes
            foreach (var entity in filterContext.ActionParameters.Values.Where(o => o != null))
            {
                // get metadata attribute
                MetadataTypeAttribute metadataTypeAttribute =
                    entity.GetType().GetCustomAttributes(typeof(MetadataTypeAttribute), true)
                        .FirstOrDefault() as MetadataTypeAttribute;

                Type attributedType = (metadataTypeAttribute != null)
                    ? metadataTypeAttribute.MetadataClassType
                    : entity.GetType();

                // get all properties of entity class and possibly defined metadata class
                var attributedTypeProperties = TypeDescriptor.GetProperties(attributedType)
                    .Cast<PropertyDescriptor>();
                var entityProperties = TypeDescriptor.GetProperties(entity.GetType())
                    .Cast<PropertyDescriptor>();

                // get errors from all validation attributes of entity and metadata class
                var errors = from attributedTypeProperty in attributedTypeProperties
                             join entityProperty in entityProperties
                             on attributedTypeProperty.Name equals entityProperty.Name
                             from attribute in attributedTypeProperty.Attributes.OfType<ValidationAttribute>()
                             where !attribute.IsValid(entityProperty.GetValue(entity))
                             select new KeyValuePair<string, string>(attributedTypeProperty.Name,
                                 attribute.FormatErrorMessage(string.Empty));

                // add errors to ModelState dictionary
                foreach (var error in errors)
                    if (!modelState.ContainsKey(error.Key))
                        modelState.AddModelError(error.Key, error.Value);
            }
        }

        base.OnActionExecuting(filterContext);
    }
}
}