﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExcellOn.PL.Areas.Admin.Helper.CartHelper
{
    public class CartItem
    {
        private IRepository<Service> repository = new Repository<Service>();
        public int DateQuantity { get; set; }
        public int EmployeeQuantity { get; set; }
        public double Changes { get; set; }
        public Service Service { get; set; }

        public CartItem(int Id)
        {
            var service = repository.FindOneById(Id);
            this.Service = service;
            this.Changes = service.Change;
        }
    }
}