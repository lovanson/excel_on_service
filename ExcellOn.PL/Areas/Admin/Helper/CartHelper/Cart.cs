﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExcellOn.PL.Areas.Admin.Helper.CartHelper
{
    public class Cart
    {
      
        public double TotalPrice { get; set; }
        public int TotalService { get; set; }
        public List<CartItem> CartItems { get; set; }

        public Cart()
        {
            if (HttpContext.Current.Session["Cart"] == null)
            {
                this.CartItems = new List<CartItem>();
                HttpContext.Current.Session["Cart"] = this;
            }
            else
            {
                this.CartItems = ((Cart)HttpContext.Current.Session["Cart"]).CartItems;
               
            }
            GetTotalPrice();
        }

        public void GetTotalPrice()
        {
            double totalPrice = 0;
            foreach (var item in this.CartItems)
            {
                totalPrice += item.Changes * item.DateQuantity * item.EmployeeQuantity;

            }
            this.TotalPrice = totalPrice;
            this.TotalService = this.CartItems.Count();
        }

        public void AddCartItem(int serviceId, int dateQuantity,int employeeQuantity)
        {
            var cartItem = this.CartItems.SingleOrDefault(x => x.Service.Id==serviceId);
            if (cartItem == null)
            {
                cartItem = new CartItem(serviceId);
                cartItem.DateQuantity = dateQuantity;
                cartItem.EmployeeQuantity = employeeQuantity;
                this.CartItems.Add(cartItem);
            }
            else
            {
                cartItem.DateQuantity += dateQuantity;
                cartItem.EmployeeQuantity += employeeQuantity;
            }

            GetTotalPrice();
        }

        public void RemoveCartItem(int serviceId)
        {
            var cartItem = this.CartItems.SingleOrDefault(x => x.Service.Id== serviceId);
            this.CartItems.Remove(cartItem);
            GetTotalPrice();
        }
        public void RemoveAll()
        {
            this.CartItems.Clear();
        }

        public void UpdateCartItem(int serviceId, int dateQuantity, int employeeQuantity)
        {
            var cartItem = this.CartItems.SingleOrDefault(x => x.Service.Id== serviceId);
            cartItem.DateQuantity = dateQuantity;
            cartItem.EmployeeQuantity = employeeQuantity;
            GetTotalPrice();
        }
    }
}