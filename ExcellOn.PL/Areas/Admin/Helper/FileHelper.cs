﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ExcellOn.PL.Areas.Admin.Helper
{
    public  class FileHelper
    {
        public static string UploadedFile(HttpPostedFileBase Image)
        {
            

            if (Image != null && Image.ContentLength > 0)
            {

                var supportedTypes = new[] { "jpg", "jpeg", "png","gif" };
                string[] fileExt = Image.FileName.Split('.');

                if (supportedTypes.Contains(fileExt[fileExt.Length - 1]))
                {
                    try
                    {
                        //Guid.NewGuid().ToString() + "-" +
                        var fileName = Path.GetFileName(Image.FileName);
                        string path = Path.Combine(HttpContext.Current.Server.MapPath("~/Areas/Admin/Content/Images"),
                                                   fileName);
                        Image.SaveAs(path);
                        return fileName;

                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);

                    }
                }
                else
                {
                    throw new Exception("The file is in the wrong format ");
                }
            }
            else
            {
                throw new Exception("file not selected");
            }
           
        }
    }
}