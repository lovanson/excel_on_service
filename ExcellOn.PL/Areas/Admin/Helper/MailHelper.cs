﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace ExcellOn.PL.Areas.Admin.Helper
{
    public class MailHelper
    {
        public static bool SendMail(string Email,string subject,string content)
        {
            try
            {
                var senderEmail = new MailAddress("hunganhtu01@gmail.com", "Excell-On Services");
                var receiverEmail = new MailAddress(Email, "test");
                var password = "bqjupckrrrzbgxot";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(senderEmail.Address, password)
                };
                string body = new StreamReader(HttpContext.Current.Server.MapPath("~/Areas/Admin/Content/Email/SendEmail.html")).ReadToEnd();
                body = body.Replace("$Content", content);
                body = body.Replace("$Email", Email);
                body = body.Replace("$Date", DateTime.Now.ToString("yyyy/MM/dd"));

                using (var mess = new MailMessage(senderEmail, receiverEmail))
                {
                    mess.Subject = subject;
                    mess.IsBodyHtml = true;
                    mess.Body = body;
                    smtp.Send(mess);

                }
             
                return true;

            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }

        public static bool ComfimEmail(string Email, string Name,string Url)
        {
            try
            {
                var senderEmail = new MailAddress("hunganhtu01@gmail.com", "Excell-On Services");
                var receiverEmail = new MailAddress(Email, Name);
                var password = "bqjupckrrrzbgxot";


                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(senderEmail.Address, password)
                };
                string body = new StreamReader(HttpContext.Current.Server.MapPath("~/Areas/Admin/Content/Email/ComfirmEmail.html")).ReadToEnd();

               
                body = body.Replace("$ConfirmEmail", "https://localhost:44309/"+Url+"?Email=" + Encrypt.EncryptString(Email));
                using (var mess = new MailMessage(senderEmail, receiverEmail))
                {
                    mess.Subject = "Confirm your email on ExcellOn";
                    mess.IsBodyHtml = true;
                    mess.Body = body;
                    smtp.Send(mess);

                }
                

                return true;

            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }
        public static bool Recovery(string Email, string Name,string Url)
        {
            try
            {
                var senderEmail = new MailAddress("hunganhtu01@gmail.com", "Excell-On Services");
                var receiverEmail = new MailAddress(Email, Name);
                var password = "bqjupckrrrzbgxot";


                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(senderEmail.Address, password)
                };
                string body = new StreamReader(HttpContext.Current.Server.MapPath("~/Areas/Admin/Content/Email/PasswordRecovery.html")).ReadToEnd();


                body = body.Replace("$ConfirmEmail", "https://localhost:44309/"+Url+"?Email=" + Encrypt.EncryptString(Email));
                using (var mess = new MailMessage(senderEmail, receiverEmail))
                {
                    mess.Subject = "Password recovery on ExcellOn";
                    mess.IsBodyHtml = true;
                    mess.Body = body;
                    smtp.Send(mess);

                }


                return true;

            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }

    }
}