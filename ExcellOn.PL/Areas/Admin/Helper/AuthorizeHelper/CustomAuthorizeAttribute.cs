﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


namespace ExcellOn.PL.Areas.Admin.Helper.AuthorizeHelper
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            IRepository<GroupUser> groupUsers = new Repository<GroupUser>();
            IRepository<Permission> pers = new Repository<Permission>();
            IRepository<GroupUserPermission> grPer = new Repository<GroupUserPermission>();

            if (httpContext.Session["user"] == null)
            {
                return false;
            }

            User u = (User)httpContext.Session["user"];
            if (u.IsClient)
            {
                return false;

            }
            if (groupUsers.FindOneByExpress(x => x.Id.Equals(u.GroupUserId)).IsAdmin)
            {
                return true;
            }
            string actionName = httpContext.Request.RequestContext.RouteData.Values["action"].ToString();
            string controllerName = httpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            string permissionName = controllerName + "Controller_" + actionName;

            Permission p = pers.FindOneByExpress(x => x.Name.Equals(permissionName));
            var granted = grPer.FindAllByExpress(x => x.GroupUserId.Equals(u.GroupUserId));
            if (!granted.Any(x => x.PermisionId.Equals(p.Id)))
            {
                return false;
            }
            return true;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (HttpContext.Current.Session["user"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                               new RouteValueDictionary(new { controller = "User", action = "Login" }));
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(
                             new RouteValueDictionary(new { controller = "Error", action = "Page401" }));
            }


        }
    }
}