﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Areas.Admin.Helper.AuthorizeHelper
{
    public class ControllerAndActionByNamspace
    {

        // get all controller by namespace
        public static List<Type> GetControllersByNamespace(string namspace)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            IEnumerable<Type> types = assembly.GetTypes()
                .Where(t => typeof(Controller).IsAssignableFrom(t) && t.Namespace.Contains(namspace))
                .OrderBy(t => t.Name);
            return types.ToList();
        }

        public static List<string> GetActionsByController(Type controller)
        {
            List<string> actions = new List<string>();
            actions = controller
                .GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public)
                .Where(m =>
                    !m.GetCustomAttributes(typeof(CompilerGeneratedAttribute), true).Any() &&
                    !m.GetCustomAttributes(typeof(AllowAnonymousAttribute), false).Any() &&
                    !m.IsDefined(typeof(NonActionAttribute)) &&
                    m.ReflectedType.IsPublic)
                .OrderBy(x => x.Name).Select(x => x.Name).ToList();
            return actions;
        }
    }
}