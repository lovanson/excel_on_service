﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Helper.AuthorizeHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Areas.Admin.Controllers
{
    [CustomAuthorize]
    public class OrderServiceController : Controller
    {
        // GET: Admin/OrderService

        private IRepository<Order> order;
        private IRepository<OrderDetail> orderDetail;
        private IRepository<ClientService> clientService;
        private IRepository<ExcellBank> excellBank;
        private IRepository<Transaction> transactionRepo;
        public OrderServiceController()
        {
            order = new Repository<Order>();
            orderDetail = new Repository<OrderDetail>();
            clientService = new Repository<ClientService>();
            excellBank = new Repository<ExcellBank>();
            transactionRepo = new Repository<Transaction>();
        }

        public ActionResult Index()
        {
            ViewBag.showOrderService = "show";
            ViewBag.activeListOrderService = "active";
            return View();
        }
        [HttpPut]
        public ActionResult UpdateStatus(int Id, int Status)
        {
            var infor = order.FindOneById(Id);

            if (infor != null && infor.Status == 0)
            {
                if (Status == 1)
                {
                    var list = orderDetail.FindAllByExpress(x => x.OrderId == infor.Id);
                    foreach (var item in list)
                    {
                        if (clientService.Any(x => x.ServiceId == item.ServiceId && x.UserId == item.Order.UserId))
                        {
                            var service = clientService.FindOneByExpress(x => x.ServiceId == item.ServiceId && x.UserId == item.Order.UserId);
                            service.EndDate = service.EndDate.AddDays(item.NumberDates);
                            service.NumberEmployees += item.NumberEmployees;
                            service.NumberDates += item.NumberDates;
                            service.IsExpired = false;
                            clientService.SaveEdit();
                        }
                        else
                        {
                            ClientService service = new ClientService
                            {
                                UserId = item.Order.UserId,
                                ServiceId = item.ServiceId,
                                NumberEmployees = item.NumberEmployees,
                                NumberDates = item.NumberDates,
                                StartDate = DateTime.Now,
                                EndDate = DateTime.Now.AddDays(item.NumberDates)
                            };

                            clientService.Add(service);
                        }
                    }

                    infor.Status = (byte)Status;
                    order.SaveEdit();
                    return Json(new
                    {
                        type = "success",
                        message = "Update client service successfull "

                    }, JsonRequestBehavior.AllowGet);
                }
                else if (Status == 2)
                {
                    var bank = excellBank.FindAll().First();
                    bank.AccountBalance -= infor.TotalPrice;
                    excellBank.SaveEdit();
                    Transaction t = new Transaction()
                    {
                        ClientName = infor.User.Name,
                        Money = infor.TotalPrice,
                        Blance = bank.AccountBalance,
                        Status = false,
                        CreatedDate = DateTime.Now
                    };
                    transactionRepo.Add(t);
                    infor.Status = (byte)Status;
                    order.SaveEdit();
                    return Json(new
                    {
                        type = "success",
                        message = "Update status successfull "

                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    infor.Status = (byte)Status;
                    order.SaveEdit();
                    return Json(new
                    {
                        type = "success",
                        message = "Successfully "

                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new
                {
                    type = "error",
                    message = "Can't update status for this order !"

                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetOne(int id)
        {
            return Json(new
            {
                statusCode = 200,
                Order = order.FindOneById(id),
                OrderDetail = orderDetail.FindAllByExpress(x => x.OrderId == id),

            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAll()
        {
            var data = order.FindAll();


            return Json(new
            {
                statusCode = 200,


                data = data.OrderByDescending(x => x.CreatedDate)
            }, JsonRequestBehavior.AllowGet);
        }
    }
}