﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Filter;
using ExcellOn.PL.Areas.Admin.Helper;
using ExcellOn.PL.Areas.Admin.Helper.AuthorizeHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Areas.Admin.Controllers
{
    [CustomAuthorize]
    public class NewController : Controller
    {
        private Repository<DAL.New> newRepo;
        public NewController(){
            newRepo = new Repository<DAL.New>();
        }
        // GET: Admin/New
        public ActionResult Index()
        {
            ViewBag.activeNew = "active";
            ViewBag.show = "show";
            return View();
        }
        public ActionResult GetAll(int page = 1, string search = "")
        {

            var data = newRepo.FindAll(); 
            
            if (!string.IsNullOrEmpty(search))
            {
                data = data.Where(x => (x.Title.ToLower().Contains(search.ToLower())) || (x.ShortDescription.ToLower().Contains(search.ToLower()))).ToList();
            }

            var pageSize = 3;

            int totalPage = (int)Math.Ceiling((double)data.Count() / pageSize);

           

            return Json(new
            {
                statusCode = 200,
                page = page,
                search = search,
                totalPage = totalPage,
                pageSize = pageSize,
                data = data.OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize)
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOne(int Id)
        {
            try
            {
              
                var service = newRepo.FindOneById(Id);

                return Json(new
                {
                    statusCode = 200,
                    data = service,

                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception)
            {

                return Json(new
                {
                    statusCode = 200,
                    type = "error",
                    message = "The system is busy, please reload the page !",

                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPut]
        public ActionResult UpdateStatus(int id)
        {
            var infor = newRepo.FindOneById(id);

            if (infor != null)
            {
                infor.Status = !infor.Status;
                newRepo.SaveEdit();
                return Json(new
                {
                    type = "success",
                    message = "Successfully "

                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    type = "error",
                    message = "Error "

                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ModelValidation]
        public ActionResult Add(DAL.New entity)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            entity.Status = true;
            entity.CreatedDate = DateTime.Now;
            entity.UpdatedDate = DateTime.Now;
            entity.UserId = ((User)HttpContext.Session["user"]).Id;
            try
            {
                entity.Image = FileHelper.UploadedFile(entity.PostedFileBase);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("PostedFileBase", ex.Message);
            }

            if (ModelState.IsValid)
            {
                try
                {

                    newRepo.Add(entity);
                    type = "success";
                    message = "Successfully";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;

                }

            }
            else
            {

                foreach (var k in ModelState.Keys)
                {
                    foreach (var err in ModelState[k].Errors)
                    {
                        string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                        if (!errors.ContainsKey(key))
                        {
                            errors.Add(key, err.ErrorMessage);
                        }
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,

                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet); ;
        }

        [HttpPut]
        public ActionResult Edit(DAL.New entity)
        {
            var type = "";
            var message = "";
            entity.UpdatedDate = DateTime.Now;
            entity.CreatedDate = newRepo.FindOneByExpressToEdit(x => x.Id == entity.Id).CreatedDate;
            entity.UserId = newRepo.FindOneByExpressToEdit(x => x.Id == entity.Id).UserId;
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (entity.PostedFileBase != null && entity.PostedFileBase.ContentLength > 0)
            {
                try
                {
                    entity.Image = FileHelper.UploadedFile(entity.PostedFileBase);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("PostedFileBase", ex.Message);
                }
            }
            else
            {
                entity.Image = newRepo.FindOneByExpressToEdit(x => x.Id == entity.Id).Image;
                
            }
            if (ModelState.IsValid)
            {
                try
                {
                    entity.Status = newRepo.FindOneByExpressToEdit(x => x.Id == entity.Id).Status;
                    newRepo.Edit(entity);
                    type = "success";
                    message = "Successfully";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }

            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,

                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int id)
        {
            var type = "success";
            var message = "Delete successfully";
            var category = newRepo.FindOneById(id);
            if (!newRepo.Delete(id))
            {
                type = "error";
                message = "This New cannot be deleted !";
            }
            return Json(new
            {
                status = 200,
                type = type,
                message = message,
                category = category
            }, JsonRequestBehavior.AllowGet);
        }
    }
}