﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Filter;
using ExcellOn.PL.Areas.Admin.Helper.AuthorizeHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Areas.Admin.Controllers
{
    [CustomAuthorize]
    public class PostCategoriController : Controller
    {
        // GET: Admin/PostCategori
        private Repository<PostCategory> postCategoryRepo;
        private Repository<New> news;
        public PostCategoriController()
        {
            postCategoryRepo = new Repository<PostCategory>();
            news = new Repository<New>();
        }
        public ActionResult Index()
        {
            ViewBag.activePostCategory = "active";
            ViewBag.show = "show";
            return View();
        }
        [HttpGet]
        public ActionResult FindAll()
        {
            var data = postCategoryRepo.FindAll();
            return Json(new
            {
                statusCode = 200,
                data = data.ToList()
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAll(int page = 1, string search = "")
        {
            var data = postCategoryRepo.FindAll();
            if (!string.IsNullOrEmpty(search))
            {
                data = data.Where(x => (x.Name.ToLower().Contains(search.ToLower())) || (x.Description.ToLower().Contains(search.ToLower()))).ToList();
            }

            var pageSize = 5;

            int totalPage = (int)Math.Ceiling((double)data.Count() / pageSize);
            return Json(new
            {
                statusCode = 200,
                page = page,
                search = search,
                totalPage = totalPage,
                pageSize = pageSize,
                data = data.Skip((page - 1) * pageSize).Take(pageSize).OrderBy(x => x.Name)
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOne(int id)
        {
            return Json(new
            {
                statusCode = 200,
                data = postCategoryRepo.FindOneById(id),

            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ModelValidation]
        public ActionResult Add(PostCategory entity)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (ModelState.IsValid)
            {
                try
                {
                    postCategoryRepo.Add(entity);
                    type = "success";
                    message = "Successfully added new postCategory";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }

            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }
            return Json(new
            {
                statusCode = 200,
                data = entity,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet); ;
        }

        [HttpPut]
        public ActionResult Edit(PostCategory entity)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (ModelState.IsValid)
            {
                try
                {
                    postCategoryRepo.Edit(entity);
                    type = "success";
                    message = "Successfully edited postCategory";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }

            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,
                data = entity,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int id)
        {
            try
            {
                if (news.Any(x=>x.PostCategoryId==id))
                {
                    throw new Exception("Can't delete this post category ");
                }
                postCategoryRepo.Delete(id);
                return Json(new
                {
                    status = 200,
                    type = "success",
                    message = "Delete successfull"
                    
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new
                {
                    status = 200,
                    type = "error",
                    message = e.Message

                }, JsonRequestBehavior.AllowGet);

            }
          
            
            
        }
    }
}