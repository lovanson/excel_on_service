﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Helper.AuthorizeHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Areas.Admin.Controllers
{
    [CustomAuthorize]
    public class BussinessController : Controller
    {
        private IRepository<Bussiness> bussinessRepo;
        private IRepository<Permission> PermissionRepo;
        public BussinessController()
        {
            bussinessRepo = new Repository<Bussiness>();
            PermissionRepo = new Repository<Permission>();
        }
        // GET: Admin/Bussiness

        public ActionResult Index()
        {
            ViewBag.activeBussiness = "active";
            ViewBag.show = "show";
            return View();
        }
        public ActionResult FindAll()
        {
            var data = bussinessRepo.FindAll();
            return Json(new
            {
                statusCode = 200,
                data = data.OrderBy(x => x.Name).ToList()
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAll(int page = 1, string search = "")
        {
            var data = bussinessRepo.FindAll();
            if (!string.IsNullOrEmpty(search))
            {
                data = data.Where(x => (x.Name.ToLower().Contains(search.ToLower())) || (x.Description.ToLower().Contains(search.ToLower()))).ToList();
            }

            var pageSize = 5;

            int totalPage = (int)Math.Ceiling((double)data.Count() / pageSize);
            return Json(new
            {
                statusCode = 200,
                page = page,
                search = search,
                totalPage = totalPage,
                pageSize = pageSize,
                data = data.Skip((page - 1) * pageSize).Take(pageSize).OrderBy(x => x.Name)
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOne(string id)
        {
            return Json(new
            {
                statusCode = 200,
                data = bussinessRepo.FindOneById(id),

            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
      
        public ActionResult Add()
        {
            try
            {
                var controlers = ControllerAndActionByNamspace.GetControllersByNamespace("ExcellOn.PL.Areas.Admin.Controllers");
                foreach (var item in controlers)
                {
                    Bussiness b = new Bussiness
                    {
                        BusinessId = item.Name,
                        Name = item.Name,
                        Description = item.FullName
                    };
                    if (!bussinessRepo.Any(x => x.Name.Equals(b.Name)))
                    {
                        bussinessRepo.Add(b);
                    }
                    foreach (var action in ControllerAndActionByNamspace.GetActionsByController(item))
                    {
                        Permission p = new Permission
                        {

                            BusinessId = item.Name,
                            Name = item.Name + "_" + action,
                            Description = "Permission " + action
                        };
                        if (!PermissionRepo.Any(x => x.Name.Equals(p.Name)))
                        {
                            PermissionRepo.Add(p);
                        }


                    }
                }
                return Json(new
                {
                    statusCode = 200,
                
                    type = "success",
                    message = "Successfully",
                  
                }, JsonRequestBehavior.AllowGet); ;
            }
            catch (Exception e)
            {

                return Json(new
                {
                    statusCode = 200,
                  
                    type = "error",
                    message = e.Message,
                   
                }, JsonRequestBehavior.AllowGet); ;
            }
            
        }
        [HttpPut]
        public ActionResult Edit(Bussiness entity)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (ModelState.IsValid)
            {
                try
                {
                    bussinessRepo.Edit(entity);
                    type = "success";
                    message = "Successfully";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }

            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,
                data = entity,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpDelete]
        public ActionResult Delete(string id)
        {
            try
            {

                bussinessRepo.Delete(id);
                
                return Json(new
                {
                    status = 200,
                    type = "success",
                    message = "Delete successfully"

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new
                {
                    status = 200,
                    type = "error",
                    message = "This Bussiness cannot be delete!"

                }, JsonRequestBehavior.AllowGet);



            }
        }
    }
}