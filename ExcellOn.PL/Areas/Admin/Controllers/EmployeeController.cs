﻿using ClosedXML.Excel;
using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Filter;
using ExcellOn.PL.Areas.Admin.Helper.AuthorizeHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Areas.Admin.Controllers
{
    [CustomAuthorize]
    public class EmployeeController : Controller
    {
        private IRepository<Employee> employeeRepo;
        private IRepository<AssignServiceEmployee> assignServiceEmployee;
    
        public EmployeeController()
        {
            employeeRepo = new Repository<Employee>();
            assignServiceEmployee = new Repository<AssignServiceEmployee>();
          
        }
        // GET: Admin/Employee
        public ActionResult Index()
        {
            ViewBag.activeEmployee = "active";
            ViewBag.show = "show";
            return View();
        }
        public ActionResult GetAll(int page = 1, string search = "")
        {
            var data = employeeRepo.FindAll();
            if (!string.IsNullOrEmpty(search))
            {
                data = data.Where(x => 
                (x.Name.ToLower().Contains(search.ToLower())) ||
                (x.Phone.ToLower().Contains(search.ToLower())) ||
                (x.Email.ToLower().Contains(search.ToLower())) ||
                (x.Department.Name.ToLower().Contains(search.ToLower())) ||
                (x.Address.ToLower().Contains(search.ToLower())) 
                ).ToList();

            }

            var pageSize = 4;

            int totalPage = (int)Math.Ceiling((double)data.Count() / pageSize);
            return Json(new
            {
                statusCode = 200,
                page = page,
                search = search,
                totalPage = totalPage,
                pageSize = pageSize,
                data = data.Skip((page - 1) * pageSize).Take(pageSize).OrderBy(x => x.Name)
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOne(int id)
        {
            return Json(new
            {
                statusCode = 200,
                data = employeeRepo.FindOneById(id),

            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateStatus(int Id)
        {
            var infor = employeeRepo.FindOneById(Id);
            if (infor != null)
            {
                infor.Status = !infor.Status;
                employeeRepo.SaveEdit();
                return Json(new
                {
                    type = "success",
                    message = "Successfully "

                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    type = "error",
                    message = "Error "

                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult FindAllByDepart(int DepartmentId)
        {
            return Json(new
            {
                statusCode = 200,
                data = employeeRepo.FindAllByExpress(x=>x.DepartmentId==DepartmentId&&x.Status==true),

            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ModelValidation]
        public ActionResult Add(Employee entity)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (employeeRepo.Any(x=>x.Email.Equals(entity.Email)))
            {
                ModelState.AddModelError("Email", "Email already exists");
            }
            if (employeeRepo.Any(x => x.Phone.Equals(entity.Phone)) )
            {
                ModelState.AddModelError("Phone", "Phone already exists");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    employeeRepo.Add(entity);
                    type = "success";
                    message = "Successfully";
                  
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }


            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,
                data = entity,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null

            }, JsonRequestBehavior.AllowGet);

         

        }
        [HttpPut]
        public ActionResult Edit(Employee entity)
        {
            var type = "";
            var message = "";

            if (employeeRepo.Any(x =>x.Email.Equals(entity.Email) && x.Id !=entity.Id))
            {
                ModelState.AddModelError("Email", "Email already exists");
            }
            if (employeeRepo.Any(x => x.Phone.Equals(entity.Email) && x.Id != entity.Id))
            {
                ModelState.AddModelError("Phone", "Phone already exists");
            }

            //get errors in modelSate
            Dictionary<string, string> errors = new Dictionary<string, string>();
            
            if (ModelState.IsValid)
            {
                entity.Status = true;
                try
                {
                    employeeRepo.Edit(entity);
                    type = "success";
                    message = "Successfully";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }

            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,
                data = entity,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpDelete]
        public ActionResult Delete(int id)
        {
            try
            {
                if (assignServiceEmployee.Any(x=>x.EmployeeId==id))
                {
                    throw new Exception("This employee cannot be deleted !");
                }
                else
                {
                    employeeRepo.Delete(id);


                    return Json(new
                    {
                        status = 200,
                        type = "success",
                        message = "Delete successfully"

                    }, JsonRequestBehavior.AllowGet);
                }

               
            }
            catch (Exception)
            {
                return Json(new
                {
                    status = 200,
                    type = "error",
                    message = "This employee cannot be deleted !"

            }, JsonRequestBehavior.AllowGet);

               

            }
        }

        public ActionResult ExportExcel()
        {



            DataTable dt = new DataTable("Grid");

            dt.Columns.AddRange(new DataColumn[9]
                                            {
                                            new DataColumn(".No"),
                                            new DataColumn("Employee code "),
                                            new DataColumn("Employee Name"),
                                            new DataColumn("Phone"),
                                            new DataColumn("Email"),
                                            new DataColumn("Address"),
                                            new DataColumn("Department"),
                                            new DataColumn("BirthDay"),
                                            new DataColumn("Gender"),


                                            });


            int index = 1;
            foreach (var item in employeeRepo.FindAll())
            {
                dt.Rows.Add(index, item.Id, item.Name,item.Phone,item.Email,item.Address,item.Department.Name,String.Format("{0:yyyy-MM-dd}",item.BirthDay),item.Gender?"Male":"Female");
                index++;
            }

            var path = HttpContext.Server.MapPath("~/Areas/Admin/Content/Excel/" + "Excell On - List of employee.xlsx");
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);


                using (FileStream fs = new FileStream(path, FileMode.Create))
                {

                    wb.SaveAs(fs);
                    fs.Flush();
                    fs.Close();
                }

            }
            return Json(new
            {

                path = "/Areas/Admin/Content/Excel/Excell On - List of employee.xlsx"
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ImportExcel()
        {
            try
            {
                if (HttpContext.Request.Files.Count > 0)
                {


                    var postedFile = HttpContext.Request.Files[0];
                    var supportedTypes = new[] { "xlsx", "xlsm" };
                    string[] TypeFile = postedFile.FileName.Split('.');
                    if (supportedTypes.Contains(TypeFile[TypeFile.Length - 1]))
                    {
                        using (XLWorkbook workBook = new XLWorkbook(postedFile.InputStream))
                        {
                            //Read the first Sheet from Excel file.


                            var Worksheet = workBook.Worksheet(1);

                            var rowCount = Worksheet.Rows();
                            for (int row = 2; row <= rowCount.Count(); row++)
                            {
                                if (!String.IsNullOrEmpty(Worksheet.Cell(row, 2).Value.ToString()))
                                {
                                    var emp = new Employee
                                    {

                                        Name = Worksheet.Cell(row, 2).Value.ToString(),
                                        Phone = Worksheet.Cell(row, 3).Value.ToString().Trim(),
                                        Email = Worksheet.Cell(row, 4).Value.ToString().Trim(),
                                        Address = Worksheet.Cell(row, 5).Value.ToString(),

                                        DepartmentId = Convert.ToInt32(Worksheet.Cell(row, 6).Value.ToString().Trim()),
                                        BirthDay = DateTime.Parse(Worksheet.Cell(row, 7).Value.ToString().Trim()),
                                        Gender = Worksheet.Cell(row, 8).Value.ToString().Trim().ToLower().Equals("male") ? true : false,
                                        Status=true


                                    };
                                    if (!employeeRepo.Any(x => x.Email.Equals(emp.Email)) && !employeeRepo.Any(x => x.Phone.Equals(emp.Phone)) && !String.IsNullOrEmpty(emp.Name))
                                    {
                                        employeeRepo.Add(emp);
                                    }
                                }
                               
                               
                            }
                            return Json(new
                            {
                                StatusCode = 200,
                                type = "success",
                                message = "Successfull",

                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("ImportFile", "Wrong file format.");
                    }


                }
                else
                {
                    ModelState.AddModelError("ImportFile", "The File field is required.");
                }

                Dictionary<string, string> errors = new Dictionary<string, string>();
                foreach (var k in ModelState.Keys)
                {
                    foreach (var err in ModelState[k].Errors)
                    {
                        string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                        if (!errors.ContainsKey(key))
                        {
                            errors.Add(key, err.ErrorMessage);
                        }
                    }
                }

                return Json(new
                {
                    StatusCode = 200,
                    type = "success",
                    message = "Successfull",
                    errors = errors
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                return Json(new
                {
                    StatusCode = 200,
                    type = "error",
                    message = e.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }

    }
}