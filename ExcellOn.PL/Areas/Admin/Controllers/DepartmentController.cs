﻿
using ClosedXML.Excel;
using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Filter;
using ExcellOn.PL.Areas.Admin.Helper.AuthorizeHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Areas.Admin.Controllers
{
    [CustomAuthorize]
    public class DepartmentController : Controller
    {
        private IRepository<Department> derpartmentRepo;
        private IRepository<Employee> employeeRepo;
        public DepartmentController()
        {
            derpartmentRepo = new Repository<Department>();
            employeeRepo = new Repository<Employee>();
        }
        // GET: Admin/Department
        public ActionResult Index()
        {
            ViewBag.activeDerpartment = "active";
            ViewBag.show = "show";
            return View();
        }
        public ActionResult FindAll()
        {
            var data = derpartmentRepo.FindAll();
            return Json(new
            {
                statusCode = 200,
                data = data.OrderBy(x => x.Name).ToList()
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAll(int page = 1, string search = "")
        {
            var data = derpartmentRepo.FindAll();
            if (!string.IsNullOrEmpty(search))
            {
                data = data.Where(x => (x.Name.ToLower().Contains(search.ToLower())) || (x.Description.ToLower().Contains(search.ToLower()))).ToList();
            }

            var pageSize = 3;

            int totalPage = (int)Math.Ceiling((double)data.Count() / pageSize);
            return Json(new
            {
                statusCode = 200,
                page = page,
                search = search,
                totalPage = totalPage,
                pageSize = pageSize,
                data = data.Skip((page - 1) * pageSize).Take(pageSize).OrderBy(x => x.Name)
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOne(int id)
        {
            return Json(new
            {
                statusCode = 200,
                data = derpartmentRepo.FindOneById(id),

            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ModelValidation]
        public ActionResult Add(Department entity)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (ModelState.IsValid)
            {
                try
                {
                    derpartmentRepo.Add(entity);
                    type = "success";
                    message = "Successfully";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }


            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,
                data = entity,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet); ;
        }
        [HttpPut]
        public ActionResult Edit(Department entity)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (ModelState.IsValid)
            {
                try
                {
                    derpartmentRepo.Edit(entity);
                    type = "success";
                    message = "Successfully";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }

            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,
                data = entity,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpDelete]
        public ActionResult Delete(int id)
        {
            try
            {
                if (!employeeRepo.Any(x => x.DepartmentId == id))
                {
                    derpartmentRepo.Delete(id);
                    return Json(new
                    {
                        status = 200,
                        type = "success",
                        message = "Delete successfully"

                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = 200,
                        type = "error",
                        message = "This department cannot be delete!"

                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception)
            {
                return Json(new
                {
                    status = 200,
                    type = "error",
                    message = "This department cannot be delete!"

                }, JsonRequestBehavior.AllowGet);



            }
        }

        public ActionResult ExportExcel()
        {



            DataTable dt = new DataTable("Grid");

            dt.Columns.AddRange(new DataColumn[4]
                                            {
                                            new DataColumn(".No"),
                                            new DataColumn("Department code "),
                                            new DataColumn("Department Name"),
                                            new DataColumn("Description"),


                                            });


            int index = 1;
            foreach (var item in derpartmentRepo.FindAll())
            {
                dt.Rows.Add(index, item.Id, item.Name, item.Description);
                index++;
            }

            var path = HttpContext.Server.MapPath("~/Areas/Admin/Content/Excel/" + "Excell On - List of department.xlsx");
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);


                using (FileStream fs = new FileStream(path, FileMode.Create))
                {

                    wb.SaveAs(fs);
                    fs.Flush();
                    fs.Close();
                }

            }
            return Json(new
            {

                path = "/Areas/Admin/Content/Excel/Excell On - List of department.xlsx"
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ImportExcel()
        {
            try
            {
                if (HttpContext.Request.Files.Count > 0)
                {


                    var postedFile = HttpContext.Request.Files[0];
                    var supportedTypes = new[] { "xlsx", "xlsm" };
                    string[] TypeFile = postedFile.FileName.Split('.');
                    if (supportedTypes.Contains(TypeFile[TypeFile.Length - 1]))
                    {
                        using (XLWorkbook workBook = new XLWorkbook(postedFile.InputStream))
                        {
                            //Read the first Sheet from Excel file.


                            var Worksheet = workBook.Worksheet(1);

                            var rowCount = Worksheet.Rows();
                            for (int row = 2; row <= rowCount.Count(); row++)
                            {
                                var c = new Department
                                {
                                    Name = Worksheet.Cell(row, 2).Value.ToString(),
                                    Description = Worksheet.Cell(row, 3).Value.ToString()
                                };
                                if (!derpartmentRepo.Any(x=>x.Name.Equals(c.Name)) && !String.IsNullOrEmpty(c.Name))
                                {
                                    derpartmentRepo.Add(c);
                                }
                            }
                            return Json(new
                            {
                                StatusCode = 200,
                                type = "success",
                                message = "Successfull",
                              
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("ImportFile", "Wrong file format.");
                    }


                }
                else
                {
                    ModelState.AddModelError("ImportFile", "The File field is required.");
                }

                Dictionary<string, string> errors = new Dictionary<string, string>();
                foreach (var k in ModelState.Keys)
                {
                    foreach (var err in ModelState[k].Errors)
                    {
                        string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                        if (!errors.ContainsKey(key))
                        {
                            errors.Add(key, err.ErrorMessage);
                        }
                    }
                }

                return Json(new
                {
                    StatusCode = 200,
                    type="success",
                    message="Successfull",
                    errors=errors
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                return Json(new
                {
                    StatusCode = 200,
                    type="error",
                    message=e.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }

    }
}