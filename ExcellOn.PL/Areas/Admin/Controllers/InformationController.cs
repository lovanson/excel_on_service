﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Filter;
using ExcellOn.PL.Areas.Admin.Helper.AuthorizeHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Areas.Admin.Controllers
{
    [CustomAuthorize]
    public class InformationController : Controller
    {
        // GET: Admin/Information
        private IRepository<ContactInformation> InforRepo;
        public InformationController()
        {
            InforRepo = new Repository<ContactInformation>();
        }

        [HttpPut]
        public ActionResult UpdateStatus(int id)
        {
            var infor = InforRepo.FindOneById(id);
            if (infor!=null)
            {
                infor.Status = !(infor.Status);
                InforRepo.Save();
                return Json(new
                {
                    type = "success",
                    message = "Successfully "

                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    type = "error",
                    message = "Error "

                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Index()
        {
            ViewBag.activenformation = "active";
            ViewBag.show = "show";
            return View();
        }
        public ActionResult FindAll()
        {
            var data = InforRepo.FindAll();
            return Json(new
            {
                statusCode = 200,
                data = data.OrderBy(x => x.Address).ToList()
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAll(int page = 1, string search = "")
        {
            var data = InforRepo.FindAll();
            if (!string.IsNullOrEmpty(search))
            {
                data = data.Where(x => (x.Address.ToLower().Contains(search.ToLower())) ).ToList();
            }

            var pageSize = 5;

            int totalPage = (int)Math.Ceiling((double)data.Count() / pageSize);
            return Json(new
            {
                statusCode = 200,
                page = page,
                search = search,
                totalPage = totalPage,
                pageSize = pageSize,
                data = data.Skip((page - 1) * pageSize).Take(pageSize).OrderBy(x => x.Address)
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOne(int id)
        {
            return Json(new
            {
                statusCode = 200,
                data = InforRepo.FindOneById(id),

            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ModelValidation]
        public ActionResult Add(ContactInformation entity)
        {
          
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
          
            if (InforRepo.Any(x => x.Email.Equals(entity.Email) ))
            {
                ModelState.AddModelError("Email", "Email already exists");
            }
            if (InforRepo.Any(x => x.Phone.Equals(entity.Phone) ))
            {
                ModelState.AddModelError("Phone", "Phone already exists");
            }
            if (ModelState.IsValid)
            {
                try
                {
                    InforRepo.Add(entity);
                    type = "success";
                    message = "Successfully";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }


            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,
                data = entity,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet); ;
        }
        [HttpPut]
        public ActionResult Edit(ContactInformation entity)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (InforRepo.Any(x => x.Email.Equals(entity.Email) && x.Id!=entity.Id))
            {
                ModelState.AddModelError("Email", "Email already exists");
            }
            if (InforRepo.Any(x => x.Phone.Equals(entity.Phone) && x.Id != entity.Id))
            {
                ModelState.AddModelError("Phone", "Phone already exists");
            }
            if (ModelState.IsValid)
            {
                try
                {
                    InforRepo.Edit(entity);
                    type = "success";
                    message = "Successfully";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }

            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,
                data = entity,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            var type = "success";
            var message = "Delete successfully";
           
            if (!InforRepo.Delete(id))
            {
                type = "error";
                message = "This department cannot be deleted !";
            }
            return Json(new
            {
                status = 200,
                type = type,
                message = message,
             
            }, JsonRequestBehavior.AllowGet);
        }
    }
}