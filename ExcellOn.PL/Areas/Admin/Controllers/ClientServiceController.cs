﻿using ClosedXML.Excel;
using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Helper.AuthorizeHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Areas.Admin.Controllers
{
    [CustomAuthorize]
    public class ClientServiceController : Controller
    {
        // GET: Admin/ClientService
        private IRepository<ClientService> clientServices;
        private IRepository<AssignServiceEmployee> assignServiceEmployee;
        public ClientServiceController()
        {
            clientServices = new Repository<ClientService>();
            assignServiceEmployee = new Repository<AssignServiceEmployee>();
        }
        public ActionResult Index()
        {
            ViewBag.showOrderService = "show";
            ViewBag.activeClientService = "active";
            return View();
        }

        public ActionResult GetAll()
        {
            var data = clientServices.FindAll();
            return Json(new
            {
                statusCode = 200,
              
                data = data.OrderByDescending(x => x.StartDate)
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOne(int Id)
        {
            return Json(new { 
                data = clientServices.FindOneById(Id),
                employee = assignServiceEmployee.FindAllByExpress(x=>x.ClientServiceId==Id)
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateStatus(int Id)
        {
            var infor = clientServices.FindOneById(Id);
            if (infor != null)
            {
                infor.Status = !infor.Status;
                clientServices.SaveEdit();
                return Json(new
                {
                    type = "success",
                    message = "Successfully "

                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    type = "error",
                    message = "Error "

                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult UpdateExpired(int Id)
        {
            var infor = clientServices.FindOneById(Id);
            if (infor != null&& !infor.IsExpired)
            {
                infor.IsExpired = true;
                clientServices.SaveEdit();
                
            }
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GrantEmployee(int Id ,int[] EmployeeIds)
        {
            try
            {
                foreach (var EmployeeId in EmployeeIds)
                {
                    if (!assignServiceEmployee.Any(x => x.ClientServiceId == Id && x.EmployeeId == EmployeeId))
                    {
                        AssignServiceEmployee serviceEmployee = new AssignServiceEmployee
                        {
                            EmployeeId = EmployeeId,
                            ClientServiceId = Id,

                        };
                        assignServiceEmployee.Add(serviceEmployee);
                    }
                }
            }
            catch (Exception e)
            {

                return Json(new
                {
                    type = "error",
                    message =e.Message

                }, JsonRequestBehavior.AllowGet);
            }
            return Json(new
            {
                type = "success",
                message = "Grant employee successfull "

            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AssignServiceEmployee()
        {
            ViewBag.showOrderService = "show";
            ViewBag.activeAssignServiceEmployee = "active";
            return View();
        }
        public ActionResult GetAllAssignServiceEmployeeByClientService(int ClientServiceId)
        {
            return Json(new
            {
                data=assignServiceEmployee.FindAllByExpress(x => x.ClientServiceId == ClientServiceId)
            }, JsonRequestBehavior.AllowGet); ;
        }
        public ActionResult GetAllAssignServiceEmployee(int DepartmentId, int page = 1, string search = "")
        {
            var data = assignServiceEmployee.FindAllByExpress(x => x.Employee.DepartmentId == DepartmentId);
            if (!string.IsNullOrEmpty(search))
            {
                data = data.Where(x => (x.Employee.Name.ToLower().Contains(search.ToLower())) ||
                (x.Employee.Email.ToLower().Contains(search.ToLower())) ||

                (x.ClientService.Service.Name.ToLower().Contains(search.ToLower())) ||
                (x.Employee.Phone.ToLower().Contains(search.ToLower()))


                ).ToList();
            }

            var pageSize = 5;

            int totalPage = (int)Math.Ceiling((double)data.Count() / pageSize);
            return Json(new
            {
                statusCode = 200,
                page = page,
                search = search,
                totalPage = totalPage,
                pageSize = pageSize,
                status = DepartmentId,
                data = data.Skip((page - 1) * pageSize).Take(pageSize).OrderBy(x => x.ClientService.StartDate)
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteAssignServiceEmployee(int Id)
        {
            try
            {
                assignServiceEmployee.Delete(Id);
                return Json(new
                {
                   type="success",
                   message="Successfull"
                },JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                return Json(new
                {
                    type = "error",
                    message = e.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetOneAssignServiceEmployee(int Id)
        {
            return Json(new
            {
                
                data = assignServiceEmployee.FindOneById(Id)
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportExcel()
        {
         

            //khởi tạo đối tượng data table
            DataTable dt = new DataTable("Grid");
            //Thêm các cột tiêu đề cho bảng
            dt.Columns.AddRange(new DataColumn[7]
                                            {
                                            new DataColumn(".No"),
                                            new DataColumn("Client Name"),
                                            new DataColumn("Service Name"),
                                            new DataColumn("Employee Number"),
                                            new DataColumn("Start Date"),
                                            new DataColumn("End Date"),
                                            new DataColumn("Status"),
                                       
                                            });

            //Thêm dữ liệu cho từng cột theo danh sách category
            int index = 1;
            foreach (var item in clientServices.FindAll())
            {
                dt.Rows.Add(index,item.User.Name,item.Service.Name,item.NumberEmployees,item.StartDate,item.EndDate,item.IsExpired);
                index++;
            }

            var path = HttpContext.Server.MapPath("~/Areas/Admin/Content/Excel/" + "Excell On - List of client services.xlsx");
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);


                using (FileStream fs = new FileStream(path, FileMode.Create))
                {

                    wb.SaveAs(fs);
                    fs.Flush();
                    fs.Close();
                }

            }
            return Json(new
            {
               
                path= "/Areas/Admin/Content/Excel/Excell On - List of client services.xlsx"
            }, JsonRequestBehavior.AllowGet);
        }
    }
}