﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Helper.AuthorizeHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Areas.Admin.Controllers
{
    [CustomAuthorize]
    public class ServiceReviewController : Controller
    {
        private Repository<ServiceReview> serviceReviewR;
        public ServiceReviewController()
        {
            serviceReviewR = new Repository<ServiceReview>();
        }
        // GET: Admin/ServiceReview
        public ActionResult Index()
        {
            ViewBag.ServiceReview = "active";
            ViewBag.show = "show";
            return View();
        }
        public ActionResult GetAll(int page = 1, string search = "")
        {
            var data = serviceReviewR.FindAll();

            if (!string.IsNullOrEmpty(search))
            {
                data = data.Where(x => (x.User.Name.ToLower().Contains(search.ToLower())) || (x.Service.Name.ToLower().Contains(search.ToLower()))).ToList();
            }

            var pageSize = 3;

            int totalPage = (int)Math.Ceiling((double)data.Count() / pageSize);
            return Json(new
            {
                statusCode = 200,
                page = page,
                search = search,
                totalPage = totalPage,
                pageSize = pageSize,
                data = data.OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize)
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPut]
        public ActionResult UpdateStatus(int id,byte status)
        {
            var infor = serviceReviewR.FindOneById(id);

            if (infor != null)
            {
                infor.Status = status;
                serviceReviewR.SaveEdit();
                return Json(new
                {
                    type = "success",
                    message = "Successfully "

                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    type = "error",
                    message = "Error "

                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}