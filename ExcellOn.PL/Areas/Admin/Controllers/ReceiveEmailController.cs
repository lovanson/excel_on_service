﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Filter;
using ExcellOn.PL.Areas.Admin.Helper;
using ExcellOn.PL.Areas.Admin.Helper.AuthorizeHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Areas.Admin.Controllers
{
    [CustomAuthorize]
    public class ReceiveEmailController : Controller
    {
        // GET: Admin/ReceiveEmail

        private IRepository<ReceiveEmail> receiveEmailRepo;
        public ReceiveEmailController()
        {
            receiveEmailRepo = new Repository<ReceiveEmail>();
        }
        public ActionResult Index()
        {
            ViewBag.activeReceiveEmail = "active";
          
            return View(receiveEmailRepo.FindAll());
        }
        [HttpPut]
        public ActionResult ChangeStatus(int[] ids, byte status)
        {
            var type = "";
            var message = "";
            foreach (var item in ids)
            {
                try
                {
                    var updatesttus = receiveEmailRepo.FindOneById(item);
                    updatesttus.Status = status;
                    receiveEmailRepo.Save();
                    type = "success";
                    message = "Successfully";
                }
                catch (Exception ex)
                {
                    type = "error";
                    message = ex.Message;
                }

            }
            return Json(new
            {
                statusCode = 200,
                Type = type,
                message = message

            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FindAll()
        {
            var data = receiveEmailRepo.FindAllByExpress(x=>x.Status!=3).OrderByDescending(x => x.CreatedDate);
            return Json(new
            {
                statusCode = 200,
                data = data.ToList()
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FindAllByStatus(int status)
        {
            var data = receiveEmailRepo.FindAllByExpress(x=>x.Status==status).OrderByDescending(x=>x.CreatedDate);
            return Json(new
            {
                statusCode = 200,
                data = data.ToList()
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAll(int page = 1, string search = "")
        {
            var data = receiveEmailRepo.FindAll();
            if (!string.IsNullOrEmpty(search))
            {
                data = data.Where(x => (x.Email.ToLower().Contains(search.ToLower())) || (x.Phone.Contains(search))).ToList();
            }

            var pageSize = 5;

            int totalPage = (int)Math.Ceiling((double)data.Count() / pageSize);
            return Json(new
            {
                statusCode = 200,
                page = page,
                search = search,
                totalPage = totalPage,
                pageSize = pageSize,
                data = data.Skip((page - 1) * pageSize).Take(pageSize)
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOne(int id)
        {
            return Json(new
            {
                statusCode = 200,
                data = receiveEmailRepo.FindOneById(id),

            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ModelValidation]
        public ActionResult Add(ReceiveEmail entity)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (ModelState.IsValid)
            {
                try
                {
                    receiveEmailRepo.Add(entity);
                    type = "success";
                    message = "Successfully";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }


            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,
                data = entity,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet); ;
        }
        [HttpPut]
        public ActionResult Edit(ReceiveEmail entity)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (ModelState.IsValid)
            {
                try
                {
                    receiveEmailRepo.Edit(entity);
                    type = "success";
                    message = "Successfully";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }

            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,
                data = entity,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {
            var type = "success";
            var message = "Delete successfully";
          
            if (!receiveEmailRepo.Delete(id))
            {
                type = "error";
                message = "This department cannot be deleted !";
            }
            return Json(new
            {
                status = 200,
                type = type,
                message = message,
            
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendMail(string email,string subject, string content,int? idReceive)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(email))
            {
                ModelState.AddModelError("email", "The field is required");
            }
            if (string.IsNullOrEmpty(subject))
            {
                ModelState.AddModelError("subject", "The field is required");
            }
           
            if (ModelState.IsValid)
            {
                try
                {
                    if (MailHelper.SendMail(email,subject,content))
                    {
                        if (idReceive!=null)
                        {
                          var receive=  receiveEmailRepo.FindOneById(idReceive);
                            receive.Status = 5;
                            receiveEmailRepo.Save();
                        }
                        type = "success";
                        message = "Successfully";
                    }
                 
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }

            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,
               
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet);
        }

    }
}