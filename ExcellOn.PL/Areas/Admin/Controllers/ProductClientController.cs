﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Helper.AuthorizeHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Areas.Admin.Controllers
{
    [CustomAuthorize]
    public class ProductClientController : Controller
    {
        
        private IRepository<ProductClient> procRepos;
        public ProductClientController()
        {
            procRepos = new Repository<ProductClient>();
        }
        // GET: Admin/ProductClient
        public ActionResult Index()
        {
            ViewBag.activeProductClient = "active";
            ViewBag.show = "show";
            return View(procRepos.FindAll());
        }
        public ActionResult GetOne(int IdEncrypt)
        {
            try
            {
                var service = procRepos.FindOneById(IdEncrypt);

                return Json(new
                {
                    statusCode = 200,
                    data = service,

                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception)
            {

                return Json(new
                {
                    statusCode = 200,
                    type = "error",
                    message = "The system is busy, please reload the page !",

                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}