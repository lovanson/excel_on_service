﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Filter;
using ExcellOn.PL.Areas.Admin.Helper.AuthorizeHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Areas.Admin.Controllers
{
    [CustomAuthorize]
    public class GroupUserController : Controller
    {
        // GET: Admin/GroupUser
        private IRepository<GroupUser> groupUserRepo;
        private IRepository<Bussiness> business;
        private IRepository<Permission> permissions;
        private IRepository<GroupUserPermission> groupUserPermission;
        private IRepository<User> users;
        public GroupUserController()
        {
            groupUserRepo = new Repository<GroupUser>();
           
            business = new Repository<Bussiness>();
            permissions = new Repository<Permission>();
            groupUserPermission = new Repository<GroupUserPermission>();
            users = new Repository<User>();
        }
        // GET: Admin/Department
        public ActionResult Index()
        {
            ViewBag.activeGroupUser = "active";
            ViewBag.show = "show";
            return View();
        }
        [HttpPut]
        public ActionResult UpdateStatus(int id)
        {
            var infor = groupUserRepo.FindOneById(id);

            if (infor != null)
            {
                infor.IsAdmin = !infor.IsAdmin;
                groupUserRepo.SaveEdit();
                return Json(new
                {
                    type = "success",
                    message = "Successfully "

                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    type = "error",
                    message = "Error "

                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult FindAll()
        {
            var data = groupUserRepo.FindAll();
            return Json(new
            {
                statusCode = 200,
                data = data.OrderBy(x => x.Name).ToList()
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAll(int page = 1, string search = "")
        {
            var data = groupUserRepo.FindAll();
            if (!string.IsNullOrEmpty(search))
            {
                data = data.Where(x => (x.Name.ToLower().Contains(search.ToLower()))).ToList();
            }

            var pageSize = 5;

            int totalPage = (int)Math.Ceiling((double)data.Count() / pageSize);
            return Json(new
            {
                statusCode = 200,
                page = page,
                search = search,
                totalPage = totalPage,
                pageSize = pageSize,
                data = data.Skip((page - 1) * pageSize).Take(pageSize).OrderBy(x => x.Name)
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOne(int id)
        {
            return Json(new
            {
                statusCode = 200,
                data = groupUserRepo.FindOneById(id),

            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ModelValidation]
        public ActionResult Add(GroupUser entity)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (ModelState.IsValid)
            {
                try
                {
                    groupUserRepo.Add(entity);
                    type = "success";
                    message = "Successfully";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }


            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,
                data = entity,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet); ;
        }
        [HttpPut]
        public ActionResult Edit(GroupUser entity)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (ModelState.IsValid)
            {
                try
                {
                    groupUserRepo.Edit(entity);
                    type = "success";
                    message = "Successfully";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }

            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,
                data = entity,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpDelete]
        public ActionResult Delete(int id)
        {
           
            try
            {
               

                if (groupUserPermission.Any(x => x.GroupUserId == id)||users.Any(x=>x.GroupUserId==id))
                {
                    
                    throw new Exception( "This groupUser cannot be deleted !");
                }
                else
                {
                    groupUserRepo.Delete(id);
                    return Json(new
                    {
                        status = 200,
                        type = "success",
                        message = "Delete user group successfully",

                    }, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception e)
            {

                return Json(new
                {
                    status = 200,
                    type = "error",
                    message = e.Message,

                }, JsonRequestBehavior.AllowGet);
            }
            
        }


        public ActionResult FindAllBussiness()
        {
            var bus = business.FindAll();
            return Json(new { data=bus }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPerMission(string BusinessId, int GroupUserId)
        {
            var grantPermissions = permissions.FindAllByExpress(x => x.BusinessId.Equals(BusinessId)).ToList();
            foreach (var item in grantPermissions)
            {
                if (groupUserPermission.Any(x => x.PermisionId.Equals(item.Id) && x.GroupUserId.Equals(GroupUserId)))
                {
                    item.IsGranted = true;
                }
            }
            return Json(new { 
                data=grantPermissions
            
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CheckPerMission(int PermissionId, int GroupUserId)
        {
            try
            {
                var grouperMission = groupUserPermission.FindOneByExpress(x => x.PermisionId.Equals(PermissionId) && x.GroupUserId.Equals(GroupUserId));
                if (grouperMission == null)
                {
                    GroupUserPermission gr = new GroupUserPermission
                    {
                        PermisionId = PermissionId,
                        GroupUserId = GroupUserId
                    };
                    groupUserPermission.Add(gr);
                    return Json(new
                    {
                        type = "success",
                        message = "Successfully"

                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    groupUserPermission.Delete(grouperMission.Id);
                    return Json(new
                    {
                        type = "warning",
                        message = "Successfully"

                    }, JsonRequestBehavior.AllowGet);
                }
              
            }
            catch (Exception e)
            {
                return Json(new
                {
                    type = "error",
                    message = e.Message

                }, JsonRequestBehavior.AllowGet);
            }
           
        }
    }
}
