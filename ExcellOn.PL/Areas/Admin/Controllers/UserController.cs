﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using ExcellOn.BLL;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Helper;
using ExcellOn.PL.Areas.Admin.Helper.AuthorizeHelper;

namespace ExcellOn.PL.Areas.Admin.Controllers
{
   
    public class UserController : Controller
    {
        private IRepository<User> userRepos;
       
        public UserController()
        {
            userRepos = new Repository<User>();
           
        }
        // GET: Admin/User
        [CustomAuthorize]
        public ActionResult Index()
        {
            ViewBag.activeUser = "active";
            ViewBag.show = "show";
            return View();
        }
        [HttpPut]
        public ActionResult UpdateStatus(int id)
        {
            var infor = userRepos.FindOneById(id);

            if (infor != null)
            {
                infor.Status = !infor.Status;
                userRepos.SaveEdit();
                return Json(new
                {
                    type = "success",
                    message = "Successfully "

                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    type = "error",
                    message = "Error "

                }, JsonRequestBehavior.AllowGet);
            }
        } 
        [HttpPut]
        public ActionResult Edit(EditUserDTO entity)
        {
            var type = "";
            var message = "";
            if (userRepos.Any(x => x.Email.Equals(entity.Email) && x.Id != entity.Id))
            {
                ModelState.AddModelError("Email", "Email already exists");
            }
            if (userRepos.Any(x => x.Phone.Equals(entity.Email) && x.Id != entity.Id))
            {
                ModelState.AddModelError("Phone", "Phone already exists");
            }
            

            if (ModelState.IsValid)
            {
               
                try
                {
                    var user = userRepos.FindOneById(entity.Id);
                    user.Name = entity.Name;
                    user.Email = entity.Email;
                    user.Phone = entity.Phone;
                    user.GroupUserId = entity.GroupUserId;
                    user.IsClient = entity.GroupUserId==2?true:false;
                  
                    userRepos.SaveEdit();
                    type = "success";
                    message = "Update successfully";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }
            //get errors in modelSate
            Dictionary<string, string> errors = new Dictionary<string, string>();
            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,
                data = entity,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet);
        }
    
        [HttpPost]
        public ActionResult Add(AddUserDTO entity)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (userRepos.Any(x => x.Email.Equals(entity.Email)))
            {
                ModelState.AddModelError("Email", "Email already exists !");
            }
            if (userRepos.Any(x => x.Phone.Equals(entity.Phone)))
            {
                ModelState.AddModelError("Phone", "Phone already exists !");
            }

            if (ModelState.IsValid)
            {
                try
                {

                    entity.Password = GetMD5(entity.Password);


                    User user = new User()
                    {
                        Name = entity.Name,
                        Email = entity.Email,
                        Phone = entity.Phone,
                        Password = entity.Password,
                        GroupUserId = entity.GroupUserId,
                        ConfirmPassword=entity.Password

                    };
                    user.IsClient = entity.GroupUserId == 2 ? true : false;
                    user.ComfirmEmail = true;
                    user.BirthDay = DateTime.Now;
                    userRepos.AddDTO(user);
                    type = "success";
                    message = "Successfully";


                }
                catch (Exception ex)
                {

                    type = "error";
                    message = ex.Message;
                }
            }
            else
            {

                foreach (var k in ModelState.Keys)
                {
                    foreach (var err in ModelState[k].Errors)
                    {
                        string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                        if (!errors.ContainsKey(key))
                        {
                            errors.Add(key, err.ErrorMessage);
                        }
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,
                data = entity,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null

            }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetOne(int id)
        {
            return Json(new
            {
                statusCode = 200,
                data = userRepos.FindOneById(id),

            }, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorize]
        public ActionResult GetAll(int groupUserId,int page = 1, string search = "")
        {
            var data = userRepos.FindAllByExpress(x=>x.GroupUserId==groupUserId&&x.GroupUser.IsAdmin==false);
            if (!string.IsNullOrEmpty(search))
            {
                data = data.Where(x => (x.Name.ToLower().Contains(search.ToLower()))||(x.Email.ToLower().Contains(search.ToLower()))||(x.Address.ToLower().Contains(search.ToLower()))||(x.Phone.ToLower().Contains(search.ToLower()))).ToList();
            }

            var pageSize = 5;

            int totalPage = (int)Math.Ceiling((double)data.Count() / pageSize);
            return Json(new
            {
                statusCode = 200,
                page = page,
                search = search,
                totalPage = totalPage,
                pageSize = pageSize,
                groupUserId= groupUserId,
                data = data.Skip((page - 1) * pageSize).Take(pageSize).OrderBy(x => x.Name)
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FindAll()
        {
            var data = userRepos.FindAll();
            return Json(new
            {
                statusCode = 200,
                data = data.OrderBy(x => x.Email).ToList()
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Login()
        {


            return View();
        }

        [HttpPost]
        public ActionResult Login(string Email, string Password)
        {
            if (ModelState.IsValid)
            {
                Password = GetMD5(Password);
                var user = userRepos.FindOneByExpress(x => x.Email.Equals(Email) && x.Password.Equals(Password) && x.Status.Equals(true) && x.ComfirmEmail == true && x.IsClient==false);
                if (user != null)
                {
                    HttpContext.Session["user"] = user;

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.Message = "Incorrect account or password !";
                }
            }
            return View();
        }
        public ActionResult Register()
        {


            return View();
        }
       
        public ActionResult Logout()
        {
            Session.Remove("user");
            return RedirectToAction("Login");
        }

        public ActionResult ComfirmEmail(string Email)
        {

            Email = Encrypt.DecryptString(Email);
            var user = userRepos.FindOneByExpress(x => x.Email.Equals(Email) && x.ComfirmEmail == false);
            if (user != null)
            {
                user.ConfirmPassword = user.Password;
                user.ComfirmEmail = true;
                user.Status = true;

                userRepos.Save();
               

                return RedirectToAction("Login");
            }



            return RedirectToAction("NotFound", "Error");

        }

        public ActionResult Profiles()
        {
            if (HttpContext.Session["user"] == null)
            {
                return RedirectToAction("Login", "User");

            }
            return View();
        }
        [HttpPut]
        public ActionResult Profiles(UserDTO entity)
        {
          

            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            var user = userRepos.FindOneById(entity.Id);

            if (entity.PostedFileBase != null && entity.PostedFileBase.ContentLength > 0 && entity.PostedFileBase.FileName!=null)
            {
                try
                {
                    entity.Avatar = FileHelper.UploadedFile(entity.PostedFileBase);
                    user.Avatar = entity.Avatar;
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("PostedFileBase", ex.Message);
                }
            }

            if (ModelState.IsValid)
            {
                try
                {
                             
                    user.BirthDay = entity.BirthDay;
                    user.Address = entity.Address;
                    user.Phone = entity.Phone;
                    user.Name = entity.Name;
                    userRepos.SaveEdit();
                    HttpContext.Session["user"] = user;
                    type = "success";
                    message = "Successfully";
                }
                catch (Exception e)
                {


                    type = "error";
                    message = e.Message;
                }
            }

            else
            {

                foreach (var k in ModelState.Keys)
                {
                    foreach (var err in ModelState[k].Errors)
                    {
                        string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                        if (!errors.ContainsKey(key))
                        {
                            errors.Add(key, err.ErrorMessage);
                        }
                    }
                }
            }
            return Json(new
            {
                statusCode = 200,

                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPut]
        public ActionResult ChangePassword(UserDTOChangePass entity)
        {
            var type = "success";
            var message = "Successfully";
            Dictionary<string, string> errors = new Dictionary<string, string>();

            if (ModelState.IsValid)
            {
                entity.Password = GetMD5(entity.Password);
                entity.NewPassword = GetMD5(entity.NewPassword);
              
                var user = userRepos.FindOneByExpress(x=>x.Id==entity.Id&&x.Password.Equals(entity.Password));
                if (user!=null)
                {
                    try
                    {
                        user.Password = entity.NewPassword;
                        userRepos.SaveEdit();
                      
                    }
                    catch (Exception e)
                    {
                        type = "error";
                        message = e.Message;

                    }

                }
                else
                {
                    ModelState.AddModelError("Password", "Incorrect password !");
                }
               
            }

            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }
            return Json(new
            {
                statusCode = 200,

                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PasswordRecovery()
        {
            return View();
        }
        [HttpPut]
        public ActionResult PasswordRecovery(string email)
        {
            var type = "success";
            var message = "Check your email to recover your password";
            if (string.IsNullOrEmpty(email))
            {
                ModelState.AddModelError("email", "This field is required.");
            }
            else
            {
                var user = userRepos.FindOneByExpress(x => x.Email.Equals(email));
                if (user!=null)
                {
                    try
                    {
                        MailHelper.Recovery(user.Email, user.Name, "Admin/User/RecoveryNewPassword");
                    } 
                    catch (Exception e)
                    {
                        type = "error";
                        message = e.Message;
                       
                    }
                }
                else
                {
                    ModelState.AddModelError("email", "Email is not registered account");
                }
            }
           
            Dictionary<string, string> errors = new Dictionary<string, string>();
            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }
            return Json(new
            {
                statusCode = 200,
                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RecoveryNewPassword(string email)
        {
            ViewBag.email = email;
            email = Encrypt.DecryptString(email);

            var user = userRepos.FindOneByExpress(x => x.Email.Equals(email));
            if (user!=null)
            {
             
                return View();
            }
            else
            {
                return RedirectToAction("NotFound", "Error");
            }
        }
        [HttpPost]

        public ActionResult RecoveryNewPassword(string Password,string email)
        {
            try
            {
                email = Encrypt.DecryptString(email);
                var user = userRepos.FindOneByExpress(x => x.Email.Equals(email));
                if (user != null)
                {
                    Password = GetMD5(Password);
                    user.Password = Password;
                    userRepos.SaveEdit();
                    return RedirectToAction("Login");
                }
            }
            catch (Exception e)
            {

                ViewBag.Message = e.Message;
            }
            return View();
        }

        public static string GetMD5(string str)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(str);
            byte[] targetData = md5.ComputeHash(fromData);
            string byte2String = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += targetData[i].ToString("x2");

            }
            return byte2String;
        }


    }
}