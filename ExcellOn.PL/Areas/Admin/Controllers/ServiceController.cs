﻿using ExcellOn.BLL;
using ExcellOn.PL.Areas.Admin.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using ExcellOn.DAL;
using ExcellOn.PL.Areas.Admin.Helper;
using System.Net;
using ExcellOn.PL.Areas.Admin.Helper.AuthorizeHelper;

namespace ExcellOn.PL.Areas.Admin.Controllers
{
    [CustomAuthorize]
    public class ServiceController : Controller
    {


        // GET: Admin/Service
        private Repository<DAL.Service> servivceR;
        private Repository<OrderDetail> orderdetails;
        public ServiceController()
        {
            servivceR = new Repository<DAL.Service>();
            orderdetails = new Repository<OrderDetail>();
        }
        public ActionResult Index()
        {
            ViewBag.activeService = "active";
            ViewBag.show = "show";
            return View();
        }
        [HttpPut]
        public ActionResult UpdateStatus(int id)
        {
            var infor = servivceR.FindOneById(id);

            if (infor != null)
            {
                infor.Status = !infor.Status;
                servivceR.SaveEdit();
                return Json(new
                {
                    type = "success",
                    message = "Successfully "

                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    type = "error",
                    message = "Error "

                }, JsonRequestBehavior.AllowGet);
            }
        }
        
        public ActionResult GetAll(int page = 1, string search = "")
        {
            var data = servivceR.FindAll();

            if (!string.IsNullOrEmpty(search))
            {
                data = data.Where(x => (x.Name.ToLower().Contains(search.ToLower())) || (x.Description.ToLower().Contains(search.ToLower()))).ToList();
            }

            foreach (var item in data)
            {
                item.IdEncrypt = Encrypt.EncryptString(item.Id.ToString());
            }
            var pageSize = 5;

            int totalPage = (int)Math.Ceiling((double)data.Count() / pageSize);
            return Json(new
            {
                statusCode = 200,
                page = page,
                search = search,
                totalPage = totalPage,
                pageSize = pageSize,
                data = data.Skip((page - 1) * pageSize).Take(pageSize).OrderBy(x => x.Name)
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOne(string IdEncrypt)
        {
            try
            {
                var Id = Convert.ToInt32(Encrypt.DecryptString(IdEncrypt));
                var service = servivceR.FindOneById(Id);

                return Json(new
                {
                    statusCode = 200,
                    data = service,

                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception)
            {

                return Json(new
                {
                    statusCode = 200,
                    type = "error",
                    message = "The system is busy, please reload the page !",

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ModelValidation]
        public ActionResult Save(DAL.Service entity)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            entity.Status = true;
            try
            {
                entity.Image = FileHelper.UploadedFile(entity.PostedFileBase);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("PostedFileBase", ex.Message);
            }

            if (ModelState.IsValid)
            {
                try
                {

                    servivceR.Add(entity);
                    type = "success";
                    message = "Successfully";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;

                }

            }
            else
            {

                foreach (var k in ModelState.Keys)
                {
                    foreach (var err in ModelState[k].Errors)
                    {
                        string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                        if (!errors.ContainsKey(key))
                        {
                            errors.Add(key, err.ErrorMessage);
                        }
                    }
                }
            }

            return Json(new
            {
                statusCode = HttpStatusCode.OK,

                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet); ;
        }

        [HttpPut]
        public ActionResult Edit(DAL.Service entity)
        {
            var type = "";
            var message = "";
            Dictionary<string, string> errors = new Dictionary<string, string>();
            if (entity.PostedFileBase != null && entity.PostedFileBase.ContentLength > 0)
            {
                try
                {
                    entity.Image = FileHelper.UploadedFile(entity.PostedFileBase);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("PostedFileBase", ex.Message);
                }
            }
            else
            {
                entity.Image = servivceR.FindOneByExpressToEdit(x => x.Id == entity.Id).Image;
            }
            if (ModelState.IsValid)
            {
                try
                {
                    servivceR.Edit(entity);
                    type = "success";
                    message = "Successfully";
                }
                catch (Exception e)
                {

                    type = "error";
                    message = e.Message;
                }

            }

            foreach (var k in ModelState.Keys)
            {
                foreach (var err in ModelState[k].Errors)
                {
                    string key = Regex.Replace(k, @"(\w+)\.(\w+)", @"$2");
                    if (!errors.ContainsKey(key))
                    {
                        errors.Add(key, err.ErrorMessage);
                    }
                }
            }

            return Json(new
            {
                statusCode = 200,

                type = type,
                message = message,
                errors = errors.Count() > 0 ? errors : null
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpDelete]
        public ActionResult Delete(string IdEncrypt)
        {
            try
            {
                var Id = Convert.ToInt32(Encrypt.DecryptString(IdEncrypt));
                if (orderdetails.Any(x=>x.ServiceId==Id))
                {
                    return Json(new
                    {
                        status = 200,
                        type = "error",
                        message = "Can't delete this service "

                    }, JsonRequestBehavior.AllowGet); ;
                }
                else
                {
                    servivceR.Delete(Id);

                    return Json(new
                    {
                        status = 200,
                        type = "success",
                        message = "Delete successfully ",

                    }, JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (Exception e)
            {
                return Json(new
                {
                    status = 200,
                    type = "error",
                    message = e.Message

                }, JsonRequestBehavior.AllowGet); ;

            }
        }

        public ActionResult Detail(string Id)
        {
            try
            {
                var serviceId = Convert.ToInt32(Encrypt.DecryptString(Id));
                var service = servivceR.FindOneById(serviceId);
                service.IdEncrypt = Id;
                ViewBag.activeService = "active";
                ViewBag.show = "show";
                return View(service);


            }
            catch (Exception)
            {

                return RedirectToAction("Index");
            }

        }
    }
}