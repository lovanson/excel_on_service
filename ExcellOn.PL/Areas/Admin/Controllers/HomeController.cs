﻿using ExcellOn.BLL;
using ExcellOn.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.PL.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        private IRepository<User> users;
        private IRepository<ReceiveEmail> receiveEmails;
        private IRepository<ExcellBank> excellBank;
        private IRepository<Employee> employees;
        private IRepository<Transaction> transactions;
        private IRepository<OrderDetail> orderDetails;
        private IRepository<Order> orders;
        private IRepository<ExcellBank> bannk;
        private IRepository<Service> services;
        private IRepository<ServiceReview> serviceReviews;
        public HomeController()
        {
            users = new Repository<User>();
            receiveEmails = new Repository<ReceiveEmail>();
            excellBank = new Repository<ExcellBank>();
            employees = new Repository<Employee>();
            transactions = new Repository<Transaction>();
            orderDetails = new Repository<OrderDetail>();
            orders = new Repository<Order>();
            bannk = new Repository<ExcellBank>();
            services = new Repository<Service>();
            serviceReviews = new Repository<ServiceReview>();
            if (excellBank.FindAll().Count()==0)
            {
                ExcellBank ex = new ExcellBank()
                {
                    Name="Excell Bank",
                    Account="8888888888",
                    AccountBalance=40000
                };
                excellBank.Add(ex);
            }

        }
        // GET: Admin/Home
        public ActionResult Index()
        {
           

            if (HttpContext.Session["user"] == null)
            {

                return RedirectToAction("Login","User");
            }
            ViewBag.ShowDash = "show";
            ViewBag.ShowAnalytics = "active";

          
            AnalyticsDTO data = new AnalyticsDTO()
            {
                Receives = receiveEmails.FindAllByExpress(x=>x.Status==0).Take(6).ToList(),
                ServiceReview = serviceReviews.FindAllByExpress(x => x.Status == 1).Count() == 0 ? null : serviceReviews.FindAllByExpress(x => x.Status == 1).OrderByDescending(x=>x.CreatedDate).First(),
                Balance = excellBank.FindAll().First().AccountBalance,
                ClientNumber = users.FindAllByExpress(x => x.GroupUserId == 2).Count(),
                EmployeeNumber = employees.FindAll().Count()

        };
            return View(data);
        }
        public ActionResult Sales()
        {
            if (HttpContext.Session["user"] == null)
            {

                return RedirectToAction("Login", "User");
            }
            ViewBag.ShowDash = "show";
            ViewBag.ShowSales = "active";
           
            SaleDTO data = new SaleDTO()
            {
                ExcellBank = bannk.FindAll().First(),
                Orders = orders.FindAll().OrderByDescending(x=>x.CreatedDate).Take(10),
                Services= services.FindAll().Take(10),
                Received = transactions.FindAllByExpress(x => x.Status.Equals(true)).Sum(x=>x.Money),
                Spent= transactions.FindAllByExpress(x => x.Status.Equals(false)).Sum(x=>x.Money),
                Transactions =transactions.FindAll().OrderByDescending(x=>x.CreatedDate).Take(6),
                TotalOrder = orders.FindAll().Count(),
              

            };

            return View(data);
        }
    }
}