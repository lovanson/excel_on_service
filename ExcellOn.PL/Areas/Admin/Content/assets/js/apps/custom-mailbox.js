$(document).ready(function () {





    // Applying Scroll Bar

    const ps = new PerfectScrollbar('.message-box-scroll');
    const mailScroll = new PerfectScrollbar('.mail-sidebar-scroll', {
        suppressScrollX: true
    });

    function mailInboxScroll() {
        $('.mailbox-inbox .collapse').each(function () {
            const mailContainerScroll = new PerfectScrollbar($(this)[0], {
                suppressScrollX: true
            });
        });
    }
    mailInboxScroll();


    /*
        fn. dynamicBadgeNotification ==> Get the badge count for mail sidebar
    */
    function dynamicBadgeNotification(setMailCategoryCount) {
        var mailCategoryCount = setMailCategoryCount;

        // Get Parents Div(s)
        var get_ParentsDiv = $('.mail-item');
        var get_MailInboxParentsDiv = $('.mail-item.mailInbox');
        var get_UnreadMailInboxParentsDiv = $('[id*="unread-"]');
        var get_DraftParentsDiv = $('.mail-item.draft');

        // Get Parents Div(s) Counts
        var get_MailInboxElementsCount = get_MailInboxParentsDiv.length;
        var get_UnreadMailInboxElementsCount = get_UnreadMailInboxParentsDiv.length;
        var get_DraftElementsCount = get_DraftParentsDiv.length;

        // Get Badge Div(s)
        var getBadgeMailInboxDiv = $('#mailInbox .mail-badge');
        var getBadgeDraftMailDiv = $('#draft .mail-badge');

        if (mailCategoryCount === 'mailInbox') {
            if (get_UnreadMailInboxElementsCount === 0) {
                getBadgeMailInboxDiv.text('');
                return;
            }
            getBadgeMailInboxDiv.text(get_UnreadMailInboxElementsCount);
        } else if (mailCategoryCount === 'draftmail') {
            if (get_DraftElementsCount === 0) {
                getBadgeDraftMailDiv.text('');
                return;
            }
            getBadgeDraftMailDiv.text(get_DraftElementsCount);
        }
    }

    dynamicBadgeNotification('mailInbox');
    dynamicBadgeNotification('draftmail');

    // Open Modal on Compose Button Click
    $('#btn-compose-mail').on('click', function (event) {

        $('#composeMailModal').modal('show');


    })

    /*
        Init. fn. checkAll ==> Checkbox check all
    */
    document.getElementById('inboxAll').addEventListener('click', function () {
        var getActiveList = document.querySelectorAll('.tab-title .list-actions.active');
        var getActiveListID = '.' + getActiveList[0].id;

        var getItemsCheckboxes = '';

        if (getActiveList[0].id === 'personal' || getActiveList[0].id === 'work' || getActiveList[0].id === 'social' || getActiveList[0].id === 'private') {

            getItemsGroupCheckboxes = document.querySelectorAll(getActiveListID);
            for (var i = 0; i < getItemsGroupCheckboxes.length; i++) {
                getItemsGroupCheckboxes[i].parentNode.parentNode.parentNode;

                getItemsCheckboxes = document.querySelectorAll('.' + getItemsGroupCheckboxes[i].parentNode.parentNode.parentNode.className.split(' ')[0] + ' ' + getActiveListID + ' .inbox-chkbox');

                if (getItemsCheckboxes[i].checked) {
                    getItemsCheckboxes[i].checked = false;
                } else {
                    if (this.checked) {
                        getItemsCheckboxes[i].checked = true;
                    }
                }
            }

        } else {
            getItemsCheckboxes = document.querySelectorAll('.mail-item' + getActiveListID + ' .inbox-chkbox');
            for (var i = 0; i < getItemsCheckboxes.length; i++) {
                if (getItemsCheckboxes[i].checked) {
                    getItemsCheckboxes[i].checked = false;
                } else {
                    if (this.checked) {
                        getItemsCheckboxes[i].checked = true;
                    }
                }
            }
        }
    })

    /*
        fn. randomString ==> Generate Random Numbers
    */
    

    /*
        fn. formatAMPM ==> Get Time in 24hr Format
    */


    /*
        fn. formatBytes ==> Calculate and convert bytes into ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
    */

    // Search on each key pressed

    $('.input-search').on('keyup', function () {
        var rex = new RegExp($(this).val(), 'i');
        $('.message-box .mail-item').hide();
        $('.message-box .mail-item').filter(function () {
            return rex.test($(this).text());
        }).show();
    });

    // Tooltip

    $('[data-toggle="tooltip"]').tooltip({
        'template': '<div class="tooltip actions-btn-tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
    })

    // Triggered when mail is Closed

    $('.close-message').on('click', function (event) {
        event.preventDefault();
        $('.content-box .collapse').collapse('hide')
        $(this).parents('.content-box').css({
            width: '0',
            left: 'auto',
            right: '-46px'
        });
    });

    // Open Mail Sidebar on resolution below or equal to 991px.

    $('.mail-menu').on('click', function (e) {
        $(this).parents('.mail-box-container').children('.tab-title').addClass('mail-menu-show')
        $(this).parents('.mail-box-container').children('.mail-overlay').addClass('mail-overlay-show')
    })

    // Close sidebar when clicked on ovelay ( and ovelay itself ).

    $('.mail-overlay').on('click', function (e) {
        $(this).parents('.mail-box-container').children('.tab-title').removeClass('mail-menu-show')
        $(this).removeClass('mail-overlay-show')
    })

    /*
        fn. contentBoxPosition ==> Triggered when clicked on any each mail to show the mail content.
    */


    /*
        ====================
            Quill Editor
        ====================
    */


    // Validating input fields


    getEmailToInput = document.getElementById('m-to');



    getSubjectInput = document.getElementById('m-subject');


    $('#composeMailModal').on('hidden.bs.modal', function (e) {

        $(this)
            .find("input,textarea")
            .val('')
            .end();

        quill.deleteText(0, 2000);


    })


    /*
        =========================
            Tab Functionality
        =========================
    */
    var $listbtns = $('.list-actions').click(function () {
        $(this).parents('.mail-box-container').find('.mailbox-inbox > .content-box').css({
            width: '0',
            left: 'auto',
            right: '-46px'
        });
        $('.content-box .collapse').collapse('hide');
        var getActionCenterDivElement = $(this).parents('.mail-box-container').find('.action-center');
        if (this.id == 'mailInbox') {
            var $el = $('.' + this.id).show();
            getActionCenterDivElement.removeClass('tab-trash-active');
            $('#ct > div').not($el).hide();
        } else if (this.id == 'personal') {
            $el = '.' + $(this).attr('id');
            $elShow = $($el).show();
            getActionCenterDivElement.removeClass('tab-trash-active');
            $('#ct > div .mail-item-heading' + $el).parents('.mail-item').show();
            $('#ct > div .mail-item-heading').not($el).parents('.mail-item').hide();
        } else if (this.id == 'work') {
            $el = '.' + $(this).attr('id');
            $elShow = $($el).show();
            getActionCenterDivElement.removeClass('tab-trash-active');
            $('#ct > div .mail-item-heading' + $el).parents('.mail-item').show();
            $('#ct > div .mail-item-heading').not($el).parents('.mail-item').hide();
        } else if (this.id == 'social') {
            $el = '.' + $(this).attr('id');
            $elShow = $($el).show();
            getActionCenterDivElement.removeClass('tab-trash-active');
            $('#ct > div .mail-item-heading' + $el).parents('.mail-item').show();
            $('#ct > div .mail-item-heading').not($el).parents('.mail-item').hide();
        } else if (this.id == 'private') {
            $el = '.' + $(this).attr('id');
            $elShow = $($el).show();
            getActionCenterDivElement.removeClass('tab-trash-active');
            $('#ct > div .mail-item-heading' + $el).parents('.mail-item').show();
            $('#ct > div .mail-item-heading').not($el).parents('.mail-item').hide();
            getActionCenterDivElement.removeClass('tab-trash-active');
        } else if (this.id == 'trashed') {
            var $el = $('.' + this.id).show();
            getActionCenterDivElement.addClass('tab-trash-active');
            $('#ct > div').not($el).hide();
        } else {
            var $el = $('.' + this.id).show();
            getActionCenterDivElement.removeClass('tab-trash-active');
            $('#ct > div').not($el).hide();
        }
        $listbtns.removeClass('active');
        $(this).addClass('active');
    })

    setTimeout(function () {
        $(".list-actions#mailInbox").trigger('click');
    }, 10);


    // Revive Mail from Tash
    $(".revive-mail").on("click", function () {
        var inboxCheckboxParents = $(".inbox-chkbox:checked").parents('.mail-item');
        var inboxMailItemClass = inboxCheckboxParents.attr('class');
        var getFirstClass = inboxMailItemClass.split(' ')[1];

        var notificationText = '';
        var getCheckedItemlength = $(".inbox-chkbox:checked").length;
        var notificationText = getCheckedItemlength < 2 ? getCheckedItemlength + ' Mail restored' : getCheckedItemlength + ' Mails restored';


        inboxCheckboxParents.removeClass(getFirstClass);
        inboxCheckboxParents.addClass('mailInbox');
        $(".inbox-chkbox:checked").prop('checked', false);
        $("#inboxAll:checked").prop('checked', false);
        $(".list-actions#mailInbox").trigger('click');

        Snackbar.show({
            text: notificationText,
            width: 'auto',
            pos: 'top-center',
            actionTextColor: '#bfc9d4',
            backgroundColor: '#515365'
        });
    })

    // Permanently Delete Mail
    $(".permanent-delete").on("click", function () {
        var inboxCheckboxParents = $(".inbox-chkbox:checked").parents('.mail-item');

        var notificationText = '';
        var getCheckedItemlength = $(".inbox-chkbox:checked").length;
        var notificationText = getCheckedItemlength < 2 ? getCheckedItemlength + ' Mail Permanently Deleted' : getCheckedItemlength + ' Mails Permanently Deleted';

        if (inboxCheckboxParents.hasClass('trashed')) {
            inboxCheckboxParents.remove();
        }
        $("#inboxAll:checked").prop('checked', false);

        Snackbar.show({
            text: notificationText,
            width: 'auto',
            pos: 'top-center',
            actionTextColor: '#bfc9d4',
            backgroundColor: '#515365'
        });
    })

    // Mark mail Priority/Groups as [ Personal, Work, Social, Private ]
    $(".label-group-item").on("click", function () {
        var getLabelColor = $(this).attr('class').split(' ')[1];
        var splitLabelColor = getLabelColor.split('-')[1];


        var notificationText = '';
        var getCheckedItemlength = $(".inbox-chkbox:checked").length;

        if ($(".inbox-chkbox:checked").parents('.mail-item-heading').hasClass(splitLabelColor)) {
            var notificationText = getCheckedItemlength < 2 ? getCheckedItemlength + ' Mail removed from ' + splitLabelColor.toUpperCase() + ' Group' : getCheckedItemlength + ' Mails removed from ' + splitLabelColor.toUpperCase() + ' Group';
        } else {
            var notificationText = getCheckedItemlength < 2 ? getCheckedItemlength + ' Mail Grouped as ' + splitLabelColor.toUpperCase() : getCheckedItemlength + ' Mails Grouped as ' + splitLabelColor.toUpperCase();
        }


        $(".inbox-chkbox:checked").parents('.mail-item-heading').toggleClass(splitLabelColor);
        $(".inbox-chkbox:checked").prop('checked', false);
        $("#inboxAll:checked").prop('checked', false);

        Snackbar.show({
            text: notificationText,
            width: 'auto',
            pos: 'top-center',
            actionTextColor: '#bfc9d4',
            backgroundColor: '#515365'
        });
    });

 
    function $_GET_mailItem_Reply() {
        $(".reply").on('click', function (event) {


            var $_mailFrom = $(this).parents('.mail-content-container').attr('data-mailFrom');

            var $_mailTo = $(this).parents('.mail-content-container').attr('data-mailTo');

            var $_mailSubject = $(this).parents('.mail-content-container').find('.mail-content').attr('data-mailtitle');


            $('#m-form').val($_mailFrom);
            $('#m-to').val($_mailTo);

            $('#m-subject').val('Re: ' + $_mailSubject);

            $('#composeMailModal').modal('show');
        })
    }

    /*
        fn. $_GET_mailItem_Forward ==> Trigger when clicked on Forward Button inside Mail Content.
    */



    $_GET_mailItem_Reply();


    $('.tab-title .nav-pills a.nav-link').on('click', function (event) {
        $(this).parents('.mail-box-container').find('.tab-title').removeClass('mail-menu-show')
        $(this).parents('.mail-box-container').find('.mail-overlay').removeClass('mail-overlay-show')
    })
    var validator = $("#form-email").validate({
        rules: {
            subject: "required",
            email: {
                required: true,
                email: true

            }
        }
    });

    function contentBoxPosition() {
        $('.content-box .collapse').on('show.bs.collapse', function (event) {
            var getCollpaseElementId = this.id;
            var getSelectedMailTitleElement = $('.content-box').find('.mail-title');
            var getSelectedMailContentTitle = $(this).find('.mail-content').attr('data-mailTitle');
            $(this).parent('.content-box').css({
                width: '100%',
                left: '0',
                right: '100%'
            });
            $(this).parents('#mailbox-inbox').find('.message-box [data-target="#' + getCollpaseElementId + '"]').parents('.mail-item').removeAttr('id');
            getSelectedMailTitleElement.text(getSelectedMailContentTitle);
            getSelectedMailTitleElement.attr('data-selectedMailTitle', getSelectedMailContentTitle);

        })
    }

    function stopPropagations() {
        $('.mail-item-heading .mail-item-inner .new-control').on('click', function (e) {
            e.stopPropagation();
        })
    }

    contentBoxPosition();
    stopPropagations();
    var quill = new Quill('#editor-container', {
        modules: {
            toolbar: [
                [{
                    header: [1, 2, false]
                }],
                ['bold', 'italic', 'underline'],
                ['image', 'code-block']
            ]
        },

        theme: 'snow' // or 'bubble'
    });
    quill.setContents("ok");

    $("#btn-reply").on('click', function (event) {
        event.preventDefault();
        /* Act on the event */

        console.log($('#Id').val());
        var $_mailTo = document.getElementById('m-to').value;


        var $_mailSubject = document.getElementById('m-subject').value;
        var $_mailDescriptionText = quill.getText();


        var obj = {
            email: $_mailTo,
            subject: $_mailSubject,
            content: $_mailDescriptionText,
            idReceive: $('#idReceive').val()
        }
        console.log(obj)

        $.ajax({
            type: "POST",
            url: "/Admin/ReceiveEmail/SendMail",
            data: obj,
            success: function (res) {
                console.log(res);
                if (!res.errors) {
                    $('#composeMailModal').modal('hide');
                    swal({
                        title: res.message,
                        text: "You clicked the!",
                        type: res.type,
                        padding: '2em'
                    })
                  
                    receiveEmail.loadAll();
                } else {
                    validator.showErrors(res.errors);
                }
            }
        })


    });
    var receiveEmail = {

        loadAll: function () {

            $.ajax({
                type: "GET",
                url: "/Admin/ReceiveEmail/FindAll",

                success: function (res) {
                    let rows = ``;
                    for (let item of res.data) {
                        let created = moment(new Date(parseInt(item.CreatedDate.split('Date(')[1].split(')/')[0]))).format('yyyy-MM-DD');
                        rows += `<div id="unread-promotion-page" class="mail-item mailInbox " data-id="${item.Id}">
                                                                <div class="animated animatedFadeInUp fadeInUp" id="mailHeadingThree">
                                                                    <div class="mb-0">
                                                                        <div class="mail-item-heading social collapsed" data-toggle="collapse" role="navigation" data-target="#mailCollapseThree" aria-expanded="false">
                                                                            <div class="mail-item-inner">

                                                                                <div class="d-flex">
                                                                                    <div class="n-chk text-center">
                                                                                        <label class="new-control new-checkbox checkbox-primary">
                                                                                            <input type="checkbox" class="new-control-input inbox-chkbox checkbox-${item.Id}" data-idcheck="${item.Id}" value="1">
                                                                                            <span class="new-control-indicator"></span>
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="f-body">
                                                                                        <div class="meta-mail-time">
                                                                                            <p class="user-email" data-mailTo="${item.Email}">${item.Email}</p>
                                                                                        </div>
                                                                                        <div class="meta-title-tag">
                                                                                            <p class="mail-content-excerpt" data-mailDescription='{"ops":[{"insert":"\n"}]}'>
                                                                                                <span class="mail-title" data-mailTitle="Promotion Page">${item.Subject} - </span> ${item.Content}
                                                                                            </p>
                                                                                            <div class="tags">
                                                                                                <span class="g-dot-primary"></span>
                                                                                                <span class="g-dot-warning"></span>
                                                                                                <span class="g-dot-success"></span>
                                                                                                <span class="g-dot-danger"></span>
                                                                                            </div>

                                                                                            <p class="meta-time align-self-center mail-content-meta-date current-recent-mail">${created}</p>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>`


                    }
                    $('#ct').html(rows);
                }

            });
        },
        loadAllByStatus: function (status) {

            $.ajax({
                type: "GET",
                url: "/Admin/ReceiveEmail/FindAllByStatus?status=" + status,

                success: function (res) {
                    let rows = ``;
                    for (let item of res.data) {
                        let created = moment(new Date(parseInt(item.CreatedDate.split('Date(')[1].split(')/')[0]))).format('yyyy-MM-DD');
                        rows += `<div id="unread-promotion-page" class="mail-item mailInbox " data-id="${item.Id}">
                                                                <div class="animated animatedFadeInUp fadeInUp" id="mailHeadingThree">
                                                                    <div class="mb-0">
                                                                        <div class="mail-item-heading social collapsed" data-toggle="collapse" role="navigation" data-target="#mailCollapseThree" aria-expanded="false">
                                                                            <div class="mail-item-inner">

                                                                                <div class="d-flex">
                                                                                    <div class="n-chk text-center">
                                                                                        <label class="new-control new-checkbox checkbox-primary">
                                                                                            <input type="checkbox" class="new-control-input inbox-chkbox checkbox-${item.Id}" data-idcheck="${item.Id}" value="1">
                                                                                            <span class="new-control-indicator"></span>
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="f-body">
                                                                                        <div class="meta-mail-time">
                                                                                            <p class="user-email" data-mailTo="${item.Email}">${item.Email}</p>
                                                                                        </div>
                                                                                        <div class="meta-title-tag">
                                                                                            <p class="mail-content-excerpt" data-mailDescription='{"ops":[{"insert":"\n"}]}'>
                                                                                                <span class="mail-title" data-mailTitle="Promotion Page">${item.Subject} - </span> ${item.Content}
                                                                                            </p>
                                                                                            <div class="tags">
                                                                                                <span class="g-dot-primary"></span>
                                                                                                <span class="g-dot-warning"></span>
                                                                                                <span class="g-dot-success"></span>
                                                                                                <span class="g-dot-danger"></span>
                                                                                            </div>

                                                                                            <p class="meta-time align-self-center mail-content-meta-date current-recent-mail">${created}</p>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>`


                    }
                    $('#ct').html(rows);
                }

            });
        },
        finOne: function (id) {
            $.ajax({
                type: "GET",
                url: "/Admin/ReceiveEmail/GetOne",
                data: {
                    id: id
                },
                success: function (res) {
                    $('#idReceive').val(id);

                    $('.setName').html(res.data.Name);
                    $('.setMailto').attr('data-mailto', res.data.Email);
                    $('.setMail').html(res.data.Email);
                    $('.setMail').attr('data-mailto', res.data.Email);
                    $('.setTime').html(res.data.DateTime);
                    $('.setContent').html(res.data.Content);
                    $('.setAvatar').attr('src', 'https://ui-avatars.com/api/?name=' + res.data.Name);
                }
            })
        }
    }
    var modal = {
        removevalue: function () {
            $('#Id').val('')
            $('#Name').val('')
        },
        setValue: function (obj) {
            $('#Id').val(obj.Id)
            $('#Name').val(obj.Name)
        }
    }
    $(document).on('click', '.mail-item.mailInbox', function () {
        receiveEmail.finOne($(this).data('id'));
    });
    $('#unRead').click(function () {
        var ids = [];
        let checked = $(".inbox-chkbox:checked");
        for (let item of checked) {
            ids.push($(item).data('idcheck'));
        }
        $.ajax({
            type: "PUT",
            url: "/Admin/ReceiveEmail/ChangeStatus",
            data: {
                ids: ids,
                status: 0
            },
            success: function (res) {
                receiveEmail.loadAll();
                swal({
                    title: ' Successfully',
                    type: 'success',
                    padding: '2em'
                })

            }
        });
    });
    $('#Important').click(function () {
        var ids = [];
        let checked = $(".inbox-chkbox:checked");
        for (let item of checked) {
            ids.push($(item).data('idcheck'));
        }
        $.ajax({
            type: "PUT",
            url: "/Admin/ReceiveEmail/ChangeStatus",
            data: {
                ids: ids,
                status: 1
            },
            success: function (res) {
                receiveEmail.loadAll();
                swal({
                    title: 'Successfully',
                    type: 'success',
                    padding: '2em'
                })

            }
        });
    });

    $('#Spam').click(function () {
        var ids = [];
        let checked = $(".inbox-chkbox:checked");
        for (let item of checked) {
            ids.push($(item).data('idcheck'));
        }
        $.ajax({
            type: "PUT",
            url: "/Admin/ReceiveEmail/ChangeStatus",
            data: {
                ids: ids,
                status: 2
            },
            success: function (res) {
                receiveEmail.loadAll();

            }
        });
    });

    $('#Delete').click(function () {
        var ids = [];
        let checked = $(".inbox-chkbox:checked");
        for (let item of checked) {
            ids.push($(item).data('idcheck'));
        }
        $.ajax({
            type: "PUT",
            url: "/Admin/ReceiveEmail/ChangeStatus",
            data: {
                ids: ids,
                status: 3
            },
            success: function (res) {
                receiveEmail.loadAll();
                swal({
                    title: 'Successfully',
                    type: 'success',
                    padding: '2em'
                })
            }
        });
    });
    $('#Read').click(function () {
        var ids = [];
        let checked = $(".inbox-chkbox:checked");
        for (let item of checked) {
            ids.push($(item).data('idcheck'));
        }
        $.ajax({
            type: "PUT",
            url: "/Admin/ReceiveEmail/ChangeStatus",
            data: {
                ids: ids,
                status: 4
            },
            success: function (res) {
                receiveEmail.loadAll();
                swal({
                    title: 'Successfully',
                    type: 'success',
                    padding: '2em'
                })

            }
        });
    });
    receiveEmail.loadAll();

    $('.feather.feather-trash.permanent-delete').click(function () {

        let checked = $(".inbox-chkbox:checked");
        for (let item of checked) {
            let id = $(item).data('idcheck');
            console.log(id);
            if (confirm("Do you want to delete this receive email ?")) {
                receiveEmail.delete(id);
                $('.pagination').removeData("twbs-pagination");
            }
        }

    });

    $('#mailInbox').click(function () {
        receiveEmail.loadAll();
    })
    $('#important').click(function () {
        receiveEmail.loadAllByStatus(1);
    });

    $('#unread').click(function () {
        receiveEmail.loadAllByStatus(0);
    })
    $('#trashed').click(function () {
        receiveEmail.loadAllByStatus(3);
    })
    $('#spam').click(function () {
        receiveEmail.loadAllByStatus(2);
    })
    $('#read').click(function () {
        receiveEmail.loadAllByStatus(4);
    })
    $('#sentmail').click(function () {
        receiveEmail.loadAllByStatus(5);
    })


});