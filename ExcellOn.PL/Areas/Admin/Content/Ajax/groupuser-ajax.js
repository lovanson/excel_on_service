﻿$(function () {
    var validator = $("#form-validate").validate({
        rules: {
            Name: "required",
        },
        messages: {
            Name: "The Name field is required.",
        }
    });
    var groupUser = {
        finAllBusiness: function () {
            $.ajax({
                type: "GET",
                url: "/Admin/GroupUser/FindAllBussiness",

                success: function (res) {
                    console.log(res)
                    let rows = ``;

                    for (let item of res.data) {

                        rows += ` <option value="${item.BusinessId}">${item.Name}</option>`
                    };




                    $("#BusinessId").html(rows);
                }

            })
        },
        load: function (page, search) {
            $.ajax({
                type: "GET",
                url: "/Admin/GroupUser/GetAll",
                data: { page: page, search: search },
                success: function (res) {

                    let rows = ``;
                    let index = 0;
                    for (let item of res.data) {



                        index++;
                        rows += `<tr>
                                                    <td>${index}</td>
                                                    <td>${item.Name}</td>

                                                    <td >

                                                     
                                                            <label class="switch s-icons s-outline  s-outline-success">
                                                                        <input type="checkbox" ${item.IsAdmin ? `checked` : ``}>
                                                                        <span class="slider round change-status" data-id = ${item.Id}></span>
                                                                    </label>

                                                    </td>
                                                    <td class="text-center">

                                                        <button   class="btn btn-outline-warning btn-edit "   data-id="${item.Id}" ><i class="far fa-edit"></i></button>
                                                        <button   class="btn btn-outline-secondary btn-grant "   data-id="${item.Id}" ><i class="far fa-address-book"></i></button>
                                                        <button class="btn btn-outline-danger btn-delete" data-id="${item.Id}"  ><i class="far fa-trash-alt"></i></button>
                                                    </td>
                                                </tr>`
                    };


                    $('#input-search').val(res.search);
                    if (res.totalPage > 1) {

                        pagination.load(res.totalPage, res.page, res.search);
                    } else {
                        $('.pagination').hide();
                    }

                    $(".list-employee").html(rows);
                }

            })
        },
        post: function (obj) {
            $.ajax({
                type: "POST",
                url: "/Admin/GroupUser/Add",
                data: obj,
                success: function (res) {

                    validator.showErrors(res.errors);
                    if (!res.errors) {

                        $('.modal-entity').modal('hide');
                        groupUser.load();
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                    }

                }
            })
        },
        edit: function (obj) {
            $.ajax({
                type: "PUT",
                url: "/Admin/GroupUser/Edit",
                data: obj,
                success: function (res) {

                    validator.showErrors(res.errors);
                    if (!res.errors) {

                        $('.modal-entity').modal('hide');
                        groupUser.load();
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                    }

                }
            })
        },
        delete: function (id) {
            $.ajax({
                type: "DELETE",
                url: "/Admin/GroupUser/Delete",
                data: { id: id },
                success: function (res) {
                    swal({
                        title: res.message,
                        text: "You clicked the!",
                        type: res.type,
                        padding: '2em'
                    })
                    groupUser.load();

                }
            })
        },
        finOne: function (id) {
            $.ajax({
                type: "GET",
                url: "/Admin/GroupUser/GetOne",
                data: { id: id },
                success: function (res) {
                    modal.setValue(res.data)

                    if (res.data.IsAdmin) {
                        $('#IsAdmin').prop('checked', true);
                    } else {
                        $('#IsAdmin').prop('checked', false);
                    }
                }
            })
        },
        CheckPerMission: function (PermissionId, GroupUserId) {
            $.ajax({
                type: "GET",
                url: "/Admin/GroupUser/CheckPerMission",
                data: {
                    GroupUserId: GroupUserId,
                    PermissionId: PermissionId
                },
                success: function (res) {
                    showToastr(res.type, res.message)
                }
            })
        },
        GetPerMission: function (BusinessId, GroupUserId) {
            $.ajax({
                type: "GET",
                url: "/Admin/GroupUser/GetPerMission",
                data: {
                    BusinessId: BusinessId,
                    GroupUserId: GroupUserId
                },
                success: function (res) {

                    let div = ``;
                    for (let item of res.data) {
                        div += `<div class="col-md-4">
                                        <label>
                                            <input type="checkbox" name="checkPermisson" value=" ${item.Id}" ${item.IsGranted ? "checked" : ""} class="checkPermisson"  /> ${item.Description}
                                        </label>
                                    </div>`;
                    }
                    $('.list-permission').html(div);
                }
            })
        },
        updateStatus: function (id) {
            $.ajax({
                type: "PUT",
                url: "/Admin/GroupUser/UpdateStatus",
                data: { id: id },
                success: function (res) {

                    swal({
                        title: res.message,
                        text: "You clicked the!",
                        type: res.type,
                        padding: '2em'
                    })

                }
            })
        }
    }
    var modal = {
        removevalue: function () {
            $('#Id').val('')
            $('#Name').val('')
            $('#Status').val('')
        },
        setValue: function (obj) {
            $('#Id').val(obj.Id)
            $('#Name').val(obj.Name)
            $('#Status').val(obj.IsAdmin)
        }
    }

    groupUser.load()
    groupUser.finAllBusiness()

    //search
    $('.btn-search').click(function () {

        $('.pagination').removeData("twbs-pagination");
        let search = $('#input-search').val()
        groupUser.load(1, search);

    });
    $(".btn-save").click(function () {
        $('.pagination').removeData("twbs-pagination");
        let data = $('#form-validate').serialize();
        if ($('#Id').val()) {
            groupUser.edit(data);
        } else {
            let postData = data.replace("Id=&", "");
            groupUser.post(postData);
        }


    });

    $('.btn-add').click(function () {
        $('.modal-entity').modal("show");
    })

    //delete employee
    $(document).on('click', '.btn-delete', function () {

        let id = $(this).data("id");
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            padding: '2em'
        }).then(function (result) {
            if (result.value) {
                groupUser.delete(id);
                $('.pagination').removeData("twbs-pagination");

            }
        })
    });
    //delete employee
    $(document).on('click', '.btn-edit', function () {

        let id = $(this).data("id");
        $('.modal-entity').modal("show");

        groupUser.finOne(id);

    });


    $(document).on('click', '.btn-grant', function () {


        $('#GroupUserId').val($(this).data("id"))
        let BusinessId = $('#BusinessId').val();

        groupUser.GetPerMission(BusinessId, $('#GroupUserId').val())
        $('.modal-grant-permission').modal("show");



    });

    $('#BusinessId').change(function () {
        let BusinessId = $('#BusinessId').val();
        let GroupUserId = $('#GroupUserId').val();
        groupUser.GetPerMission(BusinessId, GroupUserId)
    })
    $('.modal-entity').on('hidden.bs.modal', function () {
        validator.resetForm();
        $(".form-control").removeClass("error");
        modal.removevalue();
    })



    $(document).on('click', '.checkPermisson ', function () {
        let GroupUserId = $('#GroupUserId').val();
        let PermissionId = $(this).val()

        groupUser.CheckPerMission(PermissionId, GroupUserId)
    });
    $(document).on('click', '.change-status ', function () {
        let id = $(this).attr("data-Id");
        if (id) {
            groupUser.updateStatus(id)
        }
    });
})