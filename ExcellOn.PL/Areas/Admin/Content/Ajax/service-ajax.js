﻿
$(function () {
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',

        // These options are needed to round to whole numbers if that's what you want.
        //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
        //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
    });
    var description = CKEDITOR.replace('Description');


    var validator = $("#form-validate").validate({
        rules: {
            Name: "required",
            Charges: {
                required: true,
                number: true

            }
        },
        messages: {
            Name: "The Name field is required.",


            Charges: {
                required: "The Charges field is required.",
                number: "You must enter a number"
            }
        }
    });
    var pagination = {

        load: function (totalPage, currentPage, search) {
            $('.pagination').show();
            $('.pagination').twbsPagination({
                totalPages: totalPage,
                visiblePages: 10,
                startPage: currentPage,

                onPageClick: function (event, page) {

                    let search = $('#input-search').val();
                    service.load(page, search);

                }
            })
        }
    }
    var service = {
        load: function (page, search) {
            $.ajax({
                type: "GET",
                url: "/Admin/Service/GetAll",
                data: { page: page, search: search },
                success: function (res) {
                    console.log('ôl')
                    let rows = ``;
                    let index = 0;
                    for (let item of res.data) {
                        let change = formatter.format(item.Change)
                        index++;
                        rows += `<tr>
                                        <td>${index}</td>
                                        <td>${item.Name}</td>
                                        <td><img src="/Areas/Admin/Content/Images/${item.Image}" style="width:100px;height:100px" /></td>
                                        <td>${change}</td>
                                        <td>
                                                 <label class="switch s-icons s-outline  s-outline-success">
                                                    <input type="checkbox" ${item.Status ? `checked` : ``}>
                                                    <span class="slider round change-status" data-id = ${item.Id}></span>
                                                </label>
                                        </td>

                                        <td class="text-center">

                                            <a   class="btn btn-outline-primary" href="/Admin/Service/Detail/${item.IdEncrypt}"  ><i class="far fa-eye"></i></a>
                                            <button   class="btn btn-outline-warning btn-edit "   data-id="${item.IdEncrypt}" ><i class="far fa-edit"></i></button>
                                            <button class="btn btn-outline-danger btn-delete" data-id="${item.IdEncrypt}"  ><i class="far fa-trash-alt"></i></button>
                                        </td>
                                    </tr>`
                    };


                    $('#input-search').val(res.search);
                    if (res.totalPage > 1) {

                        pagination.load(res.totalPage, res.page, res.search);
                    } else {
                        $('.pagination').hide();
                    }

                    $(".list-service").html(rows);
                }

            })
        },
        post: function (obj) {
            $.ajax({
                type: "POST",
                url: "/Admin/Service/Save",
                data: obj,
                processData: false,
                contentType: false,
                success: function (res) {

                    validator.showErrors(res.errors);
                    if (!res.errors) {

                        $('.modal-entity').modal('hide');
                        service.load();
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                    }
                }
            })
        },
        edit: function (obj) {
            $.ajax({
                type: "PUT",
                url: "/Admin/Service/Edit",
                data: obj,
                processData: false,
                contentType: false,
                success: function (res) {

                    validator.showErrors(res.errors);
                    if (!res.errors) {

                        $('.modal-entity').modal('hide');
                        service.load();
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                    }
                }
            })
        },
        delete: function (id) {
            $.ajax({
                type: "DELETE",
                url: "/Admin/Service/Delete",
                data: { IdEncrypt: id },
                success: function (res) {
                    swal({
                        title: res.message,
                        text: "You clicked the!",
                        type: res.type,
                        padding: '2em'
                    })
                    service.load();

                }
            })
        },
        finOne: function (id) {
            $.ajax({
                type: "GET",
                url: "/Admin/Service/GetOne",
                data: { IdEncrypt: id },
                success: function (res) {
                    if (res.data) {
                        modal.setValue(res.data)
                    } else {
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                    }



                }
            })
        },
        updateStatus: function (id) {
            $.ajax({
                type: "PUT",
                url: "/Admin/Service/UpdateStatus",
                data: { id: id },
                success: function (res) {

                    swal({
                        title: res.message,
                        text: "You clicked the!",
                        type: res.type,
                        padding: '2em'
                    })

                }
            })
        }
    }
    var modal = {
        removevalue: function () {
            $('#Id').val('')
            $('#Name').val('')
            $('#Change').val('')
            $('#Description').val('')
            $('#Status').val('true')
            $('#ShortDescription').val('')
            $('#PostedFileBase').val(null)
            description.setData('');
        },
        setValue: function (obj) {
            $('#Id').val(obj.Id)
            $('#Name').val(obj.Name)
            $('#Change').val(obj.Change)
            $('#Status').val(obj.Status)
            $('#ShortDescription').val(obj.ShortDescription)
            $('#Status').val(obj.Status)
            description.setData(obj.Description);
        }
    }

    service.load();
    //search
    $('.btn-search').click(function () {

        $('.pagination').removeData("twbs-pagination");
        let search = $('#input-search').val()
        service.load(1, search);

    });
    $(".btn-save").click(function () {
        $('.pagination').removeData("twbs-pagination");

        //formdata = formdata.replace("id=&", '');
        let formdata = new FormData();
        formdata.append("Id", $('#Id').val())
        formdata.append("Name", $('#Name').val())

        formdata.append("Change", $('#Change').val())
        formdata.append("Description", description.getData())
        formdata.append("ShortDescription", $('#ShortDescription').val())
        formdata.append("Status", $('#Status').val())


        //let formdata = $('#form').serialize();
        if ($('#Id').val()) {
            formdata.append("PostedFileBase", $('#PostedFileBase')[0].files[0] || new Blob())
            service.edit(formdata);
        } else {
            formdata.append("PostedFileBase", $('#PostedFileBase')[0].files[0])
            formdata.delete("Id")
            service.post(formdata);
        }


    });

    $('.btn-add').click(function () {
        $('.modal-entity').modal("show");
    })

    //delete service
    $(document).on('click', '.btn-delete', function () {

        let id = $(this).data("id");
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            padding: '2em'
        }).then(function (result) {
            if (result.value) {
                service.delete(id);
                $('.pagination').removeData("twbs-pagination");

            }
        })

    });
    //delete service
    $(document).on('click', '.btn-edit', function () {

        let id = $(this).data("id");
        
        $('.modal-entity').modal("show");
        service.finOne(id);

    });
    $('.modal-entity').on('hidden.bs.modal', function () {
        validator.resetForm();
        $(".form-control").removeClass("error");
        modal.removevalue();
    })

    $(document).on('click', '.change-status', function () {
        let id = $(this).attr("data-Id");

       
        if (id) {
            service.updateStatus(id)
        }
    });
})