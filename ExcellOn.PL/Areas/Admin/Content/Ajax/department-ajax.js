﻿$(function () {

    var validator = $("#form-validate").validate({
        rules: {
            Name: "required",
            Description: {
                required: true,

            }
        },
        messages: {
            Name: "The Name field is required.",
            Description: {
                required: "The Description field is required.",

            }
        }
    });
    var validatorFile = $("#form-import").validate({
        rules: {
            ImportFile: "required"
          
        }
    });
    var pagination = {

        load: function (totalPage, currentPage, search) {
            $('.pagination').show();
            $('.pagination').twbsPagination({
                totalPages: totalPage,
                visiblePages: 10,
                startPage: currentPage,

                onPageClick: function (event, page) {

                    let search = $('#input-search').val();
                    department.load(page, search);

                }
            })
        }
    }
    var department = {
        load: function (page, search) {
            $.ajax({
                type: "GET",
                url: "/Admin/Department/GetAll",
                data: { page: page, search: search },
                success: function (res) {
                    
                    let rows = ``;
                    let index = 0;
                    for (let item of res.data) {
                        index++;
                        rows += `<tr>
                                                <td>${index}</td>
                                                <td>${item.Id}</td>
                                                <td>${item.Name}</td>
                                                <td>${item.Description}</td>

                                                <td class="text-center">

                                                    <button   class="btn btn-outline-warning btn-edit "   data-id="${item.Id}" ><i class="far fa-edit"></i></button>
                                                    <button class="btn btn-outline-danger btn-delete" data-id="${item.Id}"  ><i class="far fa-trash-alt"></i></button>
                                                </td>
                                            </tr>`
                    };


                    $('#input-search').val(res.search);
                    if (res.totalPage > 1) {

                        pagination.load(res.totalPage, res.page, res.search);
                    } else {
                        $('.pagination').hide();
                    }

                    $(".list-department").html(rows);
                }

            })
        },
        post: function (obj) {
            $.ajax({
                type: "POST",
                url: "/Admin/Department/Add",
                data: obj,
                success: function (res) {

                    validator.showErrors(res.errors);
                    if (!res.errors) {

                        $('.modal-entity').modal('hide');
                        department.load();
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                    }

                }
            })
        },
        edit: function (obj) {
            $.ajax({
                type: "PUT",
                url: "/Admin/Department/Edit",
                data: obj,
                success: function (res) {

                    validator.showErrors(res.errors);
                    if (!res.errors) {

                        $('.modal-entity').modal('hide');
                        department.load();
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                    }

                }
            })
        },
        delete: function (id) {
            $.ajax({
                type: "DELETE",
                url: "/Admin/Department/Delete",
                data: { id: id },
                success: function (res) {
                    swal({
                        title: res.message,
                        text: "You clicked the!",
                        type: res.type,
                        padding: '2em'
                    })
                    department.load();

                }
            })
        },
        finOne: function (id) {
            $.ajax({
                type: "GET",
                url: "/Admin/Department/GetOne",
                data: { id: id },
                success: function (res) {
                    modal.setValue(res.data)


                }
            })
        },
        export: function () {
            $.ajax({
                type: "GET",
                url: "/Admin/Department/ExportExcel",

                success: function (res) {
                    window.location.href = res.path
                }
            })
        }, import: function () {
            let f = new FormData();
            f.append('FileUpload', $('#ImportFile')[0].files[0]);
            $.ajax({
                type: "POST",
                url: "/Admin/Department/ImportExcel",
                data: f,
                processData: false,
                contentType: false,
                success: function (res) {
                    if (!res.errors) {

                        $('.modal-import').modal('hide');
                        department.load();
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                        $('#ImportFile').val('')
                    } else {
                        validatorFile.showErrors(res.errors);
                    }
                }
            })
        }
    }
    var modal = {
        removevalue: function () {
            $('#Id').val('')
            $('#Name').val('')
            $('#Description').val('')
        },
        setValue: function (obj) {
            $('#Id').val(obj.Id)
            $('#Name').val(obj.Name)
            $('#Description').val(obj.Description)
        }
    }
    department.load();
    //search
    $('.btn-search').click(function () {

        $('.pagination').removeData("twbs-pagination");
        let search = $('#input-search').val()
        department.load(1, search);

    });
    $(".btn-save").click(function () {
        $('.pagination').removeData("twbs-pagination");

        //formdata = formdata.replace("id=&", '');
        let data = $('#form-validate').serialize();




        //let formdata = $('#form').serialize();
        if ($('#Id').val()) {

            department.edit(data);
        } else {
            let postData = data.replace("Id=&", "");

            department.post(postData);
        }


    });

    $('.btn-add').click(function () {
        $('.modal-entity').modal("show");
    })

    //delete department
    $(document).on('click', '.btn-delete', function () {

        let id = $(this).data("id");
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            padding: '2em'
        }).then(function (result) {
            if (result.value) {
                department.delete(id);
                $('.pagination').removeData("twbs-pagination");

            }
        })
    });
    //delete department
    $(document).on('click', '.btn-edit', function () {

        let id = $(this).data("id");
        $('.modal-entity').modal("show");
        department.finOne(id);

    });
    $('.modal-entity').on('hidden.bs.modal', function () {
        validator.resetForm();
        $(".form-control").removeClass("error");
        modal.removevalue();
    })

    $(document).on('click', '.btn-export', function () {

        department.export()

    });
    $(document).on('click', '.btn-import', function () {

        $('.modal-import').modal("show");

    });

    $('.btn-save-import').click(function () {
        department.import();
        $('.pagination').removeData("twbs-pagination");
    })
})