﻿$(function () {
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',

        // These options are needed to round to whole numbers if that's what you want.
        //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
        //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
    });


    ///

    //

    var ClientService = {

        load: function () {

            $.ajax({
                type: "GET",
                url: "/Admin/ClientService/GetAll",

                success: function (res) {
                   
                    let rows = ``;
                    let index = 0;
                    for (let item of res.data) {

                        index++;


                        let StartDate = moment(new Date(parseInt(item.StartDate.split('Date(')[1].split(')/')[0]))).format('yyyy-MM-DD');
                        let curentDate = new Date();


                        let EndDate = moment(new Date(parseInt(item.EndDate.split('Date(')[1].split(')/')[0]))).format('yyyy-MM-DD ');


                        if (curentDate >= new Date(parseInt(item.EndDate.split('Date(')[1].split(')/')[0]))) {
                            ClientService.updateExpired(item.Id)
                        }
                        rows += ` <tr>
                                            <td>${index}</td>
                                            <td>${item.User.Name}</td>
                                            <td>${item.Service.Name}</td>
                                         
                                            <td>${item.NumberEmployees}</td>


                                            <td>${StartDate} -> ${EndDate} </td>
                                            <td>
                                               <label class="new-control new-checkbox new-checkbox-rounded checkbox-danger" >
                                                  <input type="checkbox" class="new-control-input"  ${item.IsExpired ? `checked` : ``} disabled>
                                                  <span class="new-control-indicator"></span>Expired
                                                </label>
                                            </td>
                                            <td>

                                                <label class="switch s-icons s-outline  s-outline-success">
                                                    <input type="checkbox" ${item.Status ? `checked` : ``}>
                                                    <span class="slider round change-status" data-id = ${item.Id}></span>
                                                </label>

                                            </td>

                                            <td class="text-center">

                                                        <button   class="btn btn-outline-warning btn-grant-employee"   data-id="${item.Id}" ><i class="far fa-user"></i></button>
                                                        <button   class="btn btn-outline-primary btn-view"   data-id="${item.Id}" ><i class="far fa-eye"></i></button>

                                                    </td>
                                        </tr>`




                    };




                    $(".list-order-service").html(rows);

                }

            }).done(function () {
                $('#alter_pagination').DataTable({
                    "pagingType": "full_numbers",
                    "oLanguage": {
                        "oPaginate": {
                            "sFirst": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left"><polyline points="15 18 9 12 15 6"></polyline></svg>',
                            "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                            "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
                            "sLast": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>'
                        },
                        "sInfo": "Showing page _PAGE_ of _PAGES_",
                        "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                        "sSearchPlaceholder": "Search...",
                        "sLengthMenu": "Results :  _MENU_",
                    },
                    "stripeClasses": [],
                    "lengthMenu": [3, 9, 15, 20],
                    "pageLength": 3
                });
            })
        },
        grant: function (Id, EmployeeIds) {
            $.ajax({
                type: "PUT",
                url: "/Admin/ClientService/GrantEmployee",
                data: { Id: Id, EmployeeIds: EmployeeIds },
                success: function (res) {




                    $('.modal-entity').modal('hide');

                    swal({
                        title: res.message,
                        text: "You clicked the!",
                        type: res.type,
                        padding: '2em'
                    })


                }
            })
        },
        finOne: function (id) {
            $.ajax({
                type: "GET",
                url: "/Admin/ClientService/GetOne",
                data: { Id: id },
                success: function (res) {
                    console.log(res)
                    let StartDate = moment(new Date(parseInt(res.data.StartDate.split('Date(')[1].split(')/')[0]))).format('yyyy-MM-DD hh:mm:ss');
                    let EndDate = moment(new Date(parseInt(res.data.EndDate.split('Date(')[1].split(')/')[0]))).format('yyyy-MM-DD hh:mm:ss');
                    let birth = moment(new Date(parseInt(res.data.User.BirthDay.split('Date(')[1].split(')/')[0]))).format('yyyy-MM-DD');
                    let change = formatter.format(res.data.Service.Change)

                    let tableEmployee = ``;
                    let index = 0;
                    for (let td of res.employee) {
                        index++;
                        tableEmployee += ` <tr style=" background: #0e1726;">
                                                                        <th scope="row">${index}</th>
                                                                        <td>${td.Employee.Name}</td>
                                                                        <td>${td.Employee.Email}</td>
                                                                        <td>${td.Employee.Phone}</td>

                                                                        <td>${td.Employee.Department.Name}</td>
                                                                    </tr>`
                    }

                    let avatar = "/Areas/Admin/Content/Images/" + res.data.User.Avatar;
                    if (res.data.User.Avatar == null) {
                        avatar = "https://ui-avatars.com/api/?name=" + res.data.User.Name;
                    }
                    let detail = ` <div class="row">
                                                    <div class="col-md-6" style="text-align:left;">
                                                        <p class="badge outline-badge-success">Start Date: <b>${StartDate}</b></p>
                                                    </div>
                                                    <div class="col-md-6" style="text-align:right;">
                                                        <p class="badge outline-badge-danger">End Date:<b>${EndDate}</b> </p>
                                                    </div>
                                                </div>
                                                <div class="rounded-pills-icon">

                                                    <ul class="nav nav-pills mb-4 mt-3  justify-content-center" id="rounded-pills-icon-tab" role="tablist">
                                                        <li class="nav-item ml-2 mr-2">
                                                            <a class="nav-link mb-2 active text-center" id="rounded-pills-icon-home-tab" data-toggle="pill" href="#rounded-pills-icon-home" role="tab" aria-controls="rounded-pills-icon-home" aria-selected="true"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg> Client</a>
                                                        </li>
                                                        <li class="nav-item ml-2 mr-2">
                                                            <a class="nav-link mb-2 text-center" id="rounded-pills-icon-profile-tab" data-toggle="pill" href="#rounded-pills-icon-profile" role="tab" aria-controls="rounded-pills-icon-profile" aria-selected="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg> Service</a>
                                                        </li>
                                                        <li class="nav-item ml-2 mr-2">
                                                            <a class="nav-link mb-2 text-center" id="rounded-pills-icon-contact-tab" data-toggle="pill" href="#rounded-pills-icon-contact" role="tab" aria-controls="rounded-pills-icon-contact" aria-selected="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone"><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg> Employee</a>
                                                        </li>


                                                    </ul>
                                                    <div class="tab-content" id="rounded-pills-icon-tabContent">
                                                        <div class="tab-pane fade show active" id="rounded-pills-icon-home" role="tabpanel" aria-labelledby="rounded-pills-icon-home-tab">
                                                            <div class="row">
                                                                <div class="col-sm-2 col-md-2">

                                                                    <img src="${avatar}" width="100px" height="100px;" style="object-fit:cover" />
                                                                </div>
                                                                <div class="col-sm-6 col-md-6">
                                                                    <p>Name: <b>${res.data.User.Name}</b></p>
                                                                    <p>Phone: <b>${res.data.User.Phone}</b></p>
                                                                    <p>Email: <b>${res.data.User.Email}</b></p>
                                                                </div>
                                                                <div class="col-sm-4 col-md-4">
                                                                    <p>Date of birth: <b>${birth}</b></p>
                                                                    <p>Gender: <b>${res.data.User.Gender ? `Male` : `Female`}</b></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="rounded-pills-icon-profile" role="tabpanel" aria-labelledby="rounded-pills-icon-profile-tab">
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <h6>Name:</h6>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <b>${res.data.Service.Name}</b>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <h6>Charges:</h6>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <b>
                                                                        ${change}&nbsp; <i class=""
                                                                                          style="font-size: 10px;">/person/day</i>
                                                                    </b>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h6>Short decription:</h6>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <p>${res.data.Service.ShortDescription}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="rounded-pills-icon-contact" role="tabpanel" aria-labelledby="rounded-pills-icon-contact-tab">
                                                            <table class="table" style=" background: #0e1726;">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col">#</th>
                                                                        <th scope="col">Name</th>
                                                                        <th scope="col">Email</th>
                                                                        <th scope="col">Phone</th>

                                                                        <th scope="col">Department</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>

                                                                        ${tableEmployee}
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                    </div>
                                                </div>`

                    $('.detail-client-service').html(detail)


                }
            })
        },
        updateStatus: function (Id) {
            $.ajax({
                type: "PUT",
                url: "/Admin/ClientService/UpdateStatus",
                data: { Id: Id },
                success: function (res) {

                    swal({
                        title: res.message,
                        text: "You clicked the!",
                        type: res.type,
                        padding: '2em'
                    })

                }
            })
        },
        updateExpired: function (Id) {
            $.ajax({
                type: "PUT",
                url: "/Admin/ClientService/UpdateExpired",
                data: { Id: Id },
                success: function (res) {
                   

                }
            })
        },
        GetAllAssignServiceEmployeeByClentService: function (ClientServiceId) {
            $.ajax({
                type: "GET",
                url: "/Admin/ClientService/GetAllAssignServiceEmployeeByClientService",
                data: { ClientServiceId: ClientServiceId },
                success: function (res) {

                    console.log(res)
                    let row = ``
                    let index = 0;
                    for (let item of res.data) {

                        index++;
                        row += ` <tr>
                                                                <td>${index}</td>
                                                                <td>${item.Employee.Name}</td>
                                                                <td>${item.Employee.Email}</td>
                                                                <td>${item.Employee.Phone}</td>
                                                                <td>${item.Employee.Department.Name}</td>
                                                                <td class="text-center">

                                                                    <a href="javascript:void(0);" class="bs-tooltip btn-delete-employee" data-id ="${item.Id}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-octagon table-cancel"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg></a>
                                                                </td>
                                                            </tr>`
                    }

                    $('.list-employee').html(row)
                }
            })
        },
        deleteEmployee: function (Id) {
            $.ajax({
                type: "GET",
                url: "/Admin/ClientService/DeleteAssignServiceEmployee",
                data: { Id: Id },
                success: function (res) {
                    swal({
                        title: res.message,
                        text: "You clicked the!",
                        type: res.type,
                        padding: '2em'
                    })
                    ClientService.GetAllAssignServiceEmployeeByClentService($('#Id').val())
                }
            })
        },
        export: function () {
            $.ajax({
                type: "GET",
                url: "/Admin/ClientService/ExportExcel",
               
                success: function (res) {
                    window.location.href = res.path
                }
            })
        }
    }
    var Department = {
        getAll: function () {
            $.ajax({
                type: "GET",
                url: "/Admin/Department/FindAll",
                success: function (res) {
                    let option = ``;
                    for (let item of res.data) {
                        option += `<option value="${item.Id}">${item.Name}</option>`
                    }

                    $('#DepartmentId').html(option)

                }
            }).done(function (data) {

                Employee.load($('#DepartmentId').val())

            });
        }
    }
    var Employee = {
        load: function (DepartmentId) {
            $.ajax({
                type: "GET",
                url: "/Admin/Employee/FindAllByDepart",
                data: { DepartmentId: DepartmentId },
                success: function (res) {
                    let option = ``;
                    for (let item of res.data) {
                        option += `<option value="${item.Id}">${item.Name}</option>`
                    }

                    $('#EmployeeIds').html(option)


                }
            }).done(function () {

            })
        }
    }

    $(".btn-save").click(function () {
        let EmployeeIds = $("#EmployeeIds").val();
        if (EmployeeIds.length === 0) {
            swal({
                title: "You not select the employee",
                text: "You clicked the!",
                type: "warning",
                padding: '2em'
            })
        } else {
            ClientService.grant($('#Id').val(), EmployeeIds)
            ClientService.GetAllAssignServiceEmployeeByClentService($('#Id').val())
        }


    });



    //delete User
    $(document).on('click', '.btn-grant-employee', function () {

        let id = $(this).data("id");
        ClientService.GetAllAssignServiceEmployeeByClentService(id)
        $('#Id').val(id)
        $('.modal-entity').modal("show");



    });
    $(document).on('click', '.btn-view', function () {

        let id = $(this).data("id");

        $('.modal-view').modal("show");
        ClientService.finOne(id)

    });
    $(document).on('click', '.btn-export', function () {

        ClientService.export()

    });



    $(document).on('change', '#DepartmentId', function () {
        Employee.load($('#DepartmentId').val())
    })
    $(document).on('click', '.change-status', function () {
        let Id = $(this).attr("data-Id");

        swal({
            title: 'Are you sure?',
            text: "Update status for this data ",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Comfirm',
            padding: '2em'
        }).then(function (result) {
            if (result.value) {
                ClientService.updateStatus(Id)

            }
        })
    })



    $(document).on('click', '.btn-delete-employee', function () {

        let Id = $(this).attr("data-Id");

        swal({
            title: 'Are you sure?',
            text: "Delete this Employee ",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Comfirm',
            padding: '2em'
        }).then(function (result) {
            if (result.value) {

                ClientService.deleteEmployee(Id)
            }
        })
    })
    ClientService.load();
    Department.getAll();
})