﻿$(function () {
    var validator = $("#form-validate").validate({
        rules: {
            Name: "required",
            Description: {
                required: true,

            }
        },
        messages: {
            Name: "The Name field is required.",
            Description: {
                required: "The Description field is required.",

            }
        }
    });
    var pagination = {

        load: function (totalPage, currentPage, search) {
            $('.pagination').show();
            $('.pagination').twbsPagination({
                totalPages: totalPage,
                visiblePages: 10,
                startPage: currentPage,

                onPageClick: function (event, page) {

                    let search = $('#input-search').val();
                    postcategory.load(page, search);

                }
            })
        }
    }
    var postcategory = {
        load: function (page, search) {
            $.ajax({
                type: "GET",
                url: "/Admin/PostCategori/GetAll",
                data: { page: page, search: search },
                success: function (res) {
                
                    let rows = ``;
                    let index = 0;
                    for (let item of res.data) {
                        index++;
                        rows += `<tr>
                                                <td>${index}</td>
                                                <td>${item.Name}</td>
                                                <td>${item.Description}</td>

                                                <td class="text-center">

                                                    <button   class="btn btn-outline-warning btn-edit "   data-id="${item.Id}" ><i class="far fa-edit"></i></button>
                                                    <button class="btn btn-outline-danger btn-delete" data-id="${item.Id}"  ><i class="far fa-trash-alt"></i></button>
                                                </td>
                                            </tr>`
                    };


                    $('#input-search').val(res.search);
                    if (res.totalPage > 1) {

                        pagination.load(res.totalPage, res.page, res.search);
                    } else {
                        $('.pagination').hide();
                    }

                    $(".list-postcategory").html(rows);
                }

            })
        },
        post: function (obj) {
            $.ajax({
                type: "POST",
                url: "/Admin/PostCategori/Add",
                data: obj,
                success: function (res) {

                    validator.showErrors(res.errors);
                    if (!res.errors) {

                        $('.modal-entity').modal('hide');

                    }
                    postcategory.load();
                    showToastr(res.type, res.message)
                }
            })
        },
        edit: function (obj) {
            $.ajax({
                type: "PUT",
                url: "/Admin/PostCategori/Edit",
                data: obj,
                success: function (res) {

                    validator.showErrors(res.errors);
                    if (!res.errors) {

                        $('.modal-entity').modal('hide');
                        postcategory.load();
                    }
                    showToastr(res.type, res.message)
                }
            })
        },
        delete: function (id) {
            $.ajax({
                type: "DELETE",
                url: "/Admin/PostCategori/Delete",
                data: { id: id },
                success: function (res) {
                    showToastr(res.type, res.message)
                    postcategory.load();

                }
            })
        },
        finOne: function (id) {
            $.ajax({
                type: "DELETE",
                url: "/Admin/PostCategori/GetOne",
                data: { id: id },
                success: function (res) {
                    modal.setValue(res.data)


                }
            })
        }
    }
    var modal = {
        removevalue: function () {
            $('#Id').val('')
            $('#Name').val('')
            $('#Description').val('')
        },
        setValue: function (obj) {
            $('#Id').val(obj.Id)
            $('#Name').val(obj.Name)
            $('#Description').val(obj.Description)
        }
    }
    postcategory.load();
    //search
    $('.btn-search').click(function () {

        $('.pagination').removeData("twbs-pagination");
        let search = $('#input-search').val()
        postcategory.load(1, search);

    });
    $(".btn-save").click(function () {
        $('.pagination').removeData("twbs-pagination");

        //formdata = formdata.replace("id=&", '');
        let data = $('#form-validate').serialize();




        //let formdata = $('#form').serialize();
        if ($('#Id').val()) {
            postcategory.edit(data);
        } else {
            let postData = data.replace("Id=&", "");

            postcategory.post(postData);
        }


    });

    $('.btn-add').click(function () {
        $('.modal-entity').modal("show");
    })

    //delete postcategory
    $(document).on('click', '.btn-delete', function () {

        let id = $(this).data("id");
        if (confirm("Do you want to delete this postcategory ?")) {
            postcategory.delete(id);
            $('.pagination').removeData("twbs-pagination");
        }
    });
    //delete postcategory
    $(document).on('click', '.btn-edit', function () {

        let id = $(this).data("id");
        $('.modal-entity').modal("show");
        postcategory.finOne(id);

    });
    $('.modal-entity').on('hidden.bs.modal', function () {
        validator.resetForm();
        $(".form-control").removeClass("error");
        modal.removevalue();
    })
})