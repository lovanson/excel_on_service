﻿$(function () {
    ; (function () {
        flatpickr($('#BirthDay'))

    })()
    jQuery.validator.addMethod("phone", function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.length > 9 &&
            phone_number.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/gm);
    }, "Phone number is not valid");

    var validator = $("#form-validate").validate({
        rules: {
            Name: "required",
            Email: {
                required: true,
                email: true


            },
            Phone: {
                required: true,
                phone: true
            },
            Address: "required",
            BirthDay: "required",

        },
        messages: {
            Name: "The Name field is required.",
            Address: "The Name field is required.",
            BirthDay: "The BirthDay field is required.",

            Email: {
                required: "The Email field is required.",
                email: "Email is not valid "

            },
            Phone: {
                required: "The Phone field is required.",


            }
        }
    });
    var validatorFile = $("#form-import").validate({
        rules: {
            ImportFile: "required"

        }
    });
    var pagination = {

        load: function (totalPage, currentPage, search) {
            $('.pagination').show();
            $('.pagination').twbsPagination({
                totalPages: totalPage,
                visiblePages: 10,
                startPage: currentPage,

                onPageClick: function (event, page) {

                    let search = $('#input-search').val();
                    employee.load(page, search);

                }
            })
        }
    }
    ///
    var department = {
        getAll: function () {
            $.ajax({
                type: "GET",
                url: "/Admin/Department/FindAll",
                success: function (res) {
                    let option = ``;
                    for (let item of res.data) {
                        option += `<option value="${item.Id}">${item.Name}</option>`
                    }

                    $('#DepartmentId').html(option)
                }
            })
        }
    }
    //
    var employee = {

        load: function (page, search) {
            $.ajax({
                type: "GET",
                url: "/Admin/Employee/GetAll",
                data: { page: page, search: search },
                success: function (res) {
                   
                    let rows = ``;
                    let index = 0;
                    for (let item of res.data) {



                        let birthDay = moment(new Date(parseInt(item.BirthDay.split('Date(')[1].split(')/')[0]))).format('yyyy-MM-DD');


                        index++;
                        rows += `<tr>
                                                    <td>${index}</td>
                                                    <td>${item.Name}</td>
                                                    <td>${item.Phone}</td>
                                                    <td>${item.Email}</td>
                                                    <td>${item.Address}</td>
                                                    <td>${birthDay}</td>

                                                    <td>${item.Department.Name}</td>
                                                    <td>${item.Gender ? "Male" : "Female"}</td>
                                                    <td>
                                                        <label class="switch s-icons s-outline  s-outline-success">
                                                            <input type="checkbox" ${item.Status ? `checked` : ``}>
                                                            <span class="slider round change-status" data-id = ${item.Id}></span>
                                                        </label>
                                                    </td>
                                                    <td class="text-center">

                                                        <button   class="btn btn-outline-warning btn-edit "   data-id="${item.Id}" ><i class="far fa-edit"></i></button>
                                                        <button class="btn btn-outline-danger btn-delete" data-id="${item.Id}"  ><i class="far fa-trash-alt"></i></button>
                                                    </td>
                                                </tr>`
                    };


                    $('#input-search').val(res.search);
                    if (res.totalPage > 1) {

                        pagination.load(res.totalPage, res.page, res.search);
                    } else {
                        $('.pagination').hide();
                    }

                    $(".list-employee").html(rows);
                }

            })
        },
        post: function (obj) {
            $.ajax({
                type: "POST",
                url: "/Admin/Employee/Add",
                data: obj,
                success: function (res) {

                    validator.showErrors(res.errors);
                    if (!res.errors) {
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                        $('.modal-entity').modal('hide');
                        employee.load();
                    }

                }
            })
        },
        edit: function (obj) {
            $.ajax({
                type: "PUT",
                url: "/Admin/Employee/Edit",
                data: obj,
                success: function (res) {

                    validator.showErrors(res.errors);
                    if (!res.errors) {

                        $('.modal-entity').modal('hide');
                        employee.load();
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                    }

                }
            })
        },
        delete: function (id) {
            $.ajax({
                type: "DELETE",
                url: "/Admin/Employee/Delete",
                data: { id: id },
                success: function (res) {
                    swal({
                        title: res.message,
                        text: "You clicked the!",
                        type: res.type,
                        padding: '2em'
                    })
                    employee.load();

                }
            })
        },
        finOne: function (id) {
            $.ajax({
                type: "GET",
                url: "/Admin/Employee/GetOne",
                data: { id: id },
                success: function (res) {
                    modal.setValue(res.data)


                }
            })
        },
        updateStatus: function (Id) {
            $.ajax({
                type: "PUT",
                url: "/Admin/Employee/UpdateStatus",
                data: { Id: Id },
                success: function (res) {

                    swal({
                        title: res.message,
                        text: "You clicked the!",
                        type: res.type,
                        padding: '2em'
                    })

                }
            })
        },
        export: function () {
            $.ajax({
                type: "GET",
                url: "/Admin/Employee/ExportExcel",

                success: function (res) {
                    window.location.href = res.path
                }
            })
        }, import: function () {
            let f = new FormData();
            f.append('FileUpload', $('#ImportFile')[0].files[0]);
            $.ajax({
                type: "POST",
                url: "/Admin/Employee/ImportExcel",
                data: f,
                processData: false,
                contentType: false,
                success: function (res) {
                    if (!res.errors) {

                        $('.modal-import').modal('hide');
                        employee.load();
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                        $('#ImportFile').val('')
                    } else {
                        validatorFile.showErrors(res.errors);
                    }
                }
            })
        }
    }
    var modal = {
        removevalue: function () {
            $('#Id').val('')
            $('#Name').val('')
            $('#Phone').val('')
            $('#Email').val('')
            $('#BirthDay').val('')
            $('#Address').val('')

        },
        setValue: function (obj) {
            let birthDay = moment(new Date(parseInt(obj.BirthDay.split('Date(')[1].split(')/')[0]))).format('yyyy-MM-DD');
            $('#Id').val(obj.Id)
            $('#Name').val(obj.Name)
            $('#Phone').val(obj.Phone)
            $('#Email').val(obj.Email)
            $('#BirthDay').val(birthDay)
            $('#Address').val(obj.Address)
            $('#DepartmentId').val(obj.DepartmentId)
            $('input[name="Gender"][value="' + obj.Gender + '"]').attr("checked", "checked")
        }
    }

    employee.load()
    department.getAll()
    //search
    $('.btn-search').click(function () {

        $('.pagination').removeData("twbs-pagination");
        let search = $('#input-search').val()
        employee.load(1, search);

    });
    $(".btn-save").click(function () {
        $('.pagination').removeData("twbs-pagination");

        //formdata = formdata.replace("id=&", '');
        let data = $('#form-validate').serialize();




        //let formdata = $('#form').serialize();
        if ($('#Id').val()) {

            employee.edit(data);
        } else {
            let postData = data.replace("Id=&", "");

            employee.post(postData);
        }


    });

    $('.btn-add').click(function () {
        $('.modal-entity').modal("show");
    })

    //delete employee
    $(document).on('click', '.btn-delete', function () {

        let id = $(this).data("id");
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            padding: '2em'
        }).then(function (result) {
            if (result.value) {
                employee.delete(id);
                $('.pagination').removeData("twbs-pagination");

            }
        })
    });
    //delete employee
    $(document).on('click', '.btn-edit', function () {

        let id = $(this).data("id");
        $('.modal-entity').modal("show");
        employee.finOne(id);

    });
    $('.modal-entity').on('hidden.bs.modal', function () {
        validator.resetForm();
        $(".form-control").removeClass("error");
        modal.removevalue();
    })
    $(document).on('click', '.change-status ', function () {
        let Id = $(this).attr("data-Id");


        if (Id) {
            employee.updateStatus(Id)
        }
    });
    $(document).on('click', '.btn-export', function () {

        employee.export()

    });
    $(document).on('click', '.btn-import', function () {

        $('.modal-import').modal("show");

    });

    $('.btn-save-import').click(function () {
        employee.import();
        $('.pagination').removeData("twbs-pagination");
    })
})