﻿var validator = $("#form-validate").validate({
    rules: {
        Name: "required",
        Description: {
            required: true,

        }
    },
    messages: {
        Name: "The Name field is required.",
        Description: {
            required: "The Description field is required.",

        }
    }
});
var pagination = {

    load: function (totalPage, currentPage, search) {
        $('.pagination').show();
        $('.pagination').twbsPagination({
            totalPages: totalPage,
            visiblePages: 10,
            startPage: currentPage,

            onPageClick: function (event, page) {

                let search = $('#input-search').val();
                Bussiness.load(page, search);

            }
        })
    }
}
var Bussiness = {
    load: function (page, search) {
        $.ajax({
            type: "GET",
            url: "/Admin/Bussiness/GetAll",
            data: { page: page, search: search },
            success: function (res) {
             
                let rows = ``;
                let index = 0;
                for (let item of res.data) {
                   
                    index++;
                    rows += `<tr>
                                                <td>${index}</td>
                                                <td>${item.Name}</td>
                                                <td>${item.Description}</td>
                                              
                                                <td class="text-center">

                                                    <button   class="btn btn-outline-warning btn-edit "   data-id="${item.BusinessId}" ><i class="far fa-edit"></i></button>
                                                    <button class="btn btn-outline-danger btn-delete" data-id="${item.BusinessId}"  ><i class="far fa-trash-alt"></i></button>
                                                </td>
                                            </tr>`
                };


                $('#input-search').val(res.search);
                if (res.totalPage > 1) {

                    pagination.load(res.totalPage, res.page, res.search);
                } else {
                    $('.pagination').hide();
                }

                $(".list-Bussiness").html(rows);
            }

        })
    },
    post: function (obj) {
        $.ajax({
            type: "POST",
            url: "/Admin/Bussiness/Add",

            success: function (res) {
                if (res.type == "success") {
                    Bussiness.load()
                }
                swal({
                    title: res.message,
                    text: "You clicked the!",
                    type: res.type,
                    padding: '2em'
                })
            }
        })
    },
    edit: function (obj) {
        $.ajax({
            type: "PUT",
            url: "/Admin/Bussiness/Edit",
            data: obj,
            success: function (res) {

                validator.showErrors(res.errors);
                if (!res.errors) {

                    $('.modal-entity').modal('hide');
                    Bussiness.load();
                    swal({
                        title: res.message,
                        text: "You clicked the!",
                        type: res.type,
                        padding: '2em'
                    })
                }

            }
        })
    },
    delete: function (id) {

        $.ajax({
            type: "DELETE",
            url: "/Admin/Bussiness/Delete",
            data: { id: id },
            success: function (res) {
                swal({
                    title: res.message,
                    text: "You clicked the!",
                    type: res.type,
                    padding: '2em'
                })
                Bussiness.load();

            }
        })
    },
    finOne: function (id) {
        $.ajax({
            type: "GET",
            url: "/Admin/Bussiness/GetOne",
            data: { id: id },
            success: function (res) {
                modal.setValue(res.data)


            }
        })
    }
}
var modal = {
    removevalue: function () {
        $('#BusinessId').val('')
        $('#Name').val('')
        $('#Description').val('')
    },
    setValue: function (obj) {
        $('#BusinessId').val(obj.BusinessId)
        $('#Name').val(obj.Name)
        $('#Description').val(obj.Description)

    }
}

//search
$('.btn-search').click(function () {

    $('.pagination').removeData("twbs-pagination");
    let search = $('#input-search').val()
    Bussiness.load(1, search);

});
$(".btn-save").click(function () {
    $('.pagination').removeData("twbs-pagination");

    //formdata = formdata.replace("id=&", '');
    let data = $('#form-validate').serialize();


    Bussiness.edit(data);



});

$('.btn-add').click(function () {
    Bussiness.post();
})

//delete  Bussiness
$(document).on('click', '.btn-delete', function () {

    let id = $(this).data("id");
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Delete',
        padding: '2em'
    }).then(function (result) {
        if (result.value) {
            Bussiness.delete(id);
            $('.pagination').removeData("twbs-pagination");

        }
    })
});
//delete  Bussiness
$(document).on('click', '.btn-edit', function () {

    let id = $(this).data("id");
    $('.modal-entity').modal("show");
    Bussiness.finOne(id);

});
$('.modal-entity').on('hidden.bs.modal', function () {
    validator.resetForm();
    $(".form-control").removeClass("error");
    modal.removevalue();
})

Bussiness.load();