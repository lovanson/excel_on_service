﻿$(function () {
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',

        // These options are needed to round to whole numbers if that's what you want.
        //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
        //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
    });

    ///

    //

    var OrderService = {

        load: function () {

            $.ajax({
                type: "GET",
                url: "/Admin/OrderService/GetAll",

                success: function (res) {
                  
                    let rows = ``;
                    let index = 0;
                    for (let item of res.data) {

                        index++;


                        let CreatedDate = moment(new Date(parseInt(item.CreatedDate.split('Date(')[1].split(')/')[0]))).format('yyyy-MM-DD hh:mm:ss');

                        let total = formatter.format(item.TotalPrice)

                        rows += ` <tr>
                                                <td>${index}</td>
                                                <td>${item.User.Name}</td>
                                                <td>${item.TotalQuantity}</td>
                                                
                                                <td>${total}</td>
                                                <td>${CreatedDate}</td>
                                                <td><select class="form-control badge outline-badge-success" id="Status-update" data-Id ="${item.Id}" ${item.Status == 0 ? `` : `disabled `}  >
                                                    <option value="0"  ${item.Status == 0 ? `selected` : ``}>Pending</option>
                                                    <option value="1"${item.Status == 1 ? `selected` : ``}  >Service already staffed</option>
                                                    <option value="2"${item.Status == 2 ? `selected` : ``}>canceled</option>
                                                </select></td>
                                                <td class="text-center">

                                                            <button   class="btn btn-outline-primary btn-view"   data-id="${item.Id}" ><i class="far fa-eye"></i></button>

                                                        </td>
                                            </tr>`




                    };




                    $(".list-order-service").html(rows);

                }

            }).done(function () {
                $('#alter_pagination').DataTable({
                    "pagingType": "full_numbers",
                    "oLanguage": {
                        "oPaginate": {
                            "sFirst": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left"><polyline points="15 18 9 12 15 6"></polyline></svg>',
                            "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                            "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
                            "sLast": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>'
                        },
                        "sInfo": "Showing page _PAGE_ of _PAGES_",
                        "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                        "sSearchPlaceholder": "Search...",
                        "sLengthMenu": "Results :  _MENU_",
                    },
                    "stripeClasses": [],
                    "lengthMenu": [3, 9, 15, 20],
                    "pageLength": 3
                });
            })
        },
        post: function (obj) {
            $.ajax({
                type: "POST",
                url: "/Admin/User/Add",
                data: obj,
                success: function (res) {

                    validator.showErrors(res.errors);
                    if (!res.errors) {
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                        $('.modal-entity').modal('hide');
                        OrderService.load();
                    }

                }
            })
        },
        edit: function (Id, GroupUserId) {
            $.ajax({
                type: "PUT",
                url: "/Admin/User/EditGroupUserId",
                data: { Id: Id, GroupUserId: GroupUserId },
                success: function (res) {


                    if (!res.errors) {

                        $('.modal-entity').modal('hide');
                        User.load();
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                    }

                }
            })
        },
        finOne: function (id) {
            $.ajax({
                type: "GET",
                url: "/Admin/OrderService/GetOne",
                data: { id: id },
                success: function (res) {


                    let total = formatter.format(res.Order.TotalPrice)
                    let CreatedDate = moment(new Date(parseInt(res.Order.CreatedDate.split('Date(')[1].split(')/')[0]))).format('yyyy-MM-DD hh:mm:ss');
                    let ordertail = ``
                    let index = 0;
                    for (let item of res.OrderDetail) {
                        index++;
                        let sub_total = formatter.format(item.NumberDates * item.NumberEmployees * item.Charges)
                        ordertail += `<tr>
                                            <td>${index}</td>
                                            <td>${item.Service.Name}</td>
                                            <td class="text-right">${item.NumberDates}</td>
                                            <td class="text-right">${item.NumberEmployees}</td>
                                            <td class="text-right">${sub_total}</td>
                                        </tr>`
                    }
                    let row = ` <div class="invoice-00001">
                                                <div class="content-section  animated animatedFadeInUp fadeInUp">

                                                    <div class="row inv--head-section">

                                                        <div class="col-sm-6 col-12">
                                                            <h3 class="in-heading">INVOICE</h3>
                                                        </div>


                                                    </div>

                                                    <div class="row inv--detail-section">

                                                        <div class="col-sm-7 align-self-center">
                                                            <p class="inv-detail-title">From : ${res.Order.User.Name}</p>
                                                             <p class="inv-list-number"><span class="inv-title">Phone Number : </span> <span class="inv-number">${res.Order.User.Phone}</span></p>
                                                             <p class="inv-list-number"><span class="inv-title">Email : </span> <span class="inv-number">${res.Order.User.Email}</span></p>
                                                            <p class="inv-created-date"><span class="inv-title">Created Date : </span> <span class="inv-date">${CreatedDate}</span></p>
                                                        </div>

                                                    </div>

                                                    <div class="row inv--product-table-section">
                                                        <div class="col-12">
                                                            <div class="table-responsive">
                                                                <table class="table">
                                                                    <thead class="">
                                                                        <tr>
                                                                            <th scope="col">S.No</th>
                                                                            <th scope="col">Service Name</th>
                                                                            <th class="text-right" scope="col">Date Number</th>
                                                                            <th class="text-right" scope="col">Employee Number</th>
                                                                            <th class="text-right" scope="col">Total</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        ${ordertail}

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row mt-4">
                                                        <div class="col-sm-7 col-12 order-sm-0 order-1">
                                                            <div class="inv--payment-info">
                                                                <div class="row">
                                                                    <div class="col-sm-12 col-12">
                                                                        <h6 class=" inv-title">Payment Info:</h6>
                                                                    </div>
                                                                    <div class="col-sm-7 col-12">
                                                                        <p class=" inv-subtitle">Bank Name: </p>
                                                                    </div>
                                                                    <div class="col-sm-5 col-12">
                                                                        <p class="">${res.Order.Payment.BankName}</p>
                                                                    </div>
                                                                    <div class="col-sm-7 col-12">
                                                                        <p class=" inv-subtitle">Account Number : </p>
                                                                    </div>
                                                                    <div class="col-sm-5 col-12">
                                                                        <p class="">${res.Order.Payment.AccountNumber}</p>
                                                                    </div>
                                                                    <div class="col-sm-7 col-12">
                                                                        <p class=" inv-subtitle">Citizen ID number: </p>
                                                                    </div>
                                                                    <div class="col-sm-5 col-12">
                                                                        <p class="">${res.Order.Payment.Identity}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-5 col-12 order-sm-1 order-0">
                                                            <div class="inv--total-amounts text-sm-right">
                                                                <div class="row">
                                                                    <div class="col-sm-8 col-7">
                                                                        <p class="">Sub Total: </p>
                                                                    </div>
                                                                    <div class="col-sm-4 col-5">
                                                                        <p class="">${total}</p>
                                                                    </div>




                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>`

                    $('.orderDetail').html(row);
                    $('.modal-detail').modal('show')

                }
            })
        },
        updateStatus: function (Id, Status) {
            $.ajax({
                type: "PUT",
                url: "/Admin/OrderService/UpdateStatus",
                data: { Id: Id, Status: Status },
                success: function (res) {

                    swal({
                        title: res.message,
                        text: "You clicked the!",
                        type: res.type,
                        padding: '2em'
                    })

                }
            })
        }
    }
    var Department = {
        getAll: function () {
            $.ajax({
                type: "GET",
                url: "/Admin/Department/FindAll",
                success: function (res) {
                    let option = ``;
                    for (let item of res.data) {
                        option += `<option value="${item.Id}">${item.Name}</option>`
                    }

                    $('#DepartmentId').html(option)

                }
            }).done(function (data) {

                Employee.load($('#DepartmentId').val())

            });
        }
    }
    var Employee = {
        load: function (DepartmentId) {
            $.ajax({
                type: "GET",
                url: "/Admin/Employee/FindAllByDepart",
                data: { DepartmentId: DepartmentId },
                success: function (res) {
                    let option = ``;
                    for (let item of res.data) {
                        option += `<option value="${item.Id}">${item.Name}</option>`
                    }

                    $('#EmployeeIds').html(option)


                }
            }).done(function () {

            })
        }
    }
    OrderService.load();
    Department.getAll();
    //search





    $('.modal-entity').on('hidden.bs.modal', function () {

    })



    $(document).on('change', '#Status-update', function () {
        let text = $('#Status-Service').find('option[value="' + $(this).val() + '"]').text()
        let Id = $(this).attr("data-Id");
        let Status = $(this).val()
        swal({
            title: 'Are you sure?',
            text: "Update status to " + text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Comfirm',
            padding: '2em'
        }).then(function (result) {
            if (result.value) {

                OrderService.updateStatus(Id, Status)

            }
        })
    })
    $(document).on('click', '.btn-view', function () {
        let Id = $(this).attr("data-Id");
        OrderService.finOne(Id)




    });
})