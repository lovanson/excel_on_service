﻿$(function () {
    jQuery.validator.addMethod("phone", function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.length > 9 &&
            phone_number.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/gm);
    }, "Phone number is not valid");

    var validator = $("#form-validate").validate({
        rules: {
            Name: "required",
            Email: {
                required: true,
                email: true


            },
            Phone: {
                required: true,
                phone: true
            },

        },
        messages: {

            Email: {
                required: "The Email field is required.",
                email: "Email is not valid "

            },
            Phone: {
                required: "The Phone field is required.",


            }
        }
    });
    var information = {

        load: function (page, search) {
            $.ajax({
                type: "GET",
                url: "/Admin/Information/GetAll",
                data: { page: page, search: search },
                success: function (res) {
                 
                    let rows = ``;
                    let index = 0;
                    for (let item of res.data) {


                        index++;
                        rows += `<tr>
                                                    <td>${index}</td>
                                                    <td>${item.Phone}</td>
                                                    <td>${item.Email}</td>
                                                    <td>${item.Address}</td>
                                                    <td >
                                                        
                                                            <div class="toggle-switch">
                                                                  <input type="checkbox" id="checkbox-${item.Id}" ${item.Status ? "checked" : ""} data-id="${item.Id}">
                                                                  <label for="checkbox-${item.Id}">
                                                                    <span class="toggle-track"></span>
                                                       
                                                                  </label>
                                                    </div>
   
                                                     </td>
                                                    <td class="text-center">

                                                        <button   class="btn btn-outline-warning btn-edit "   data-id="${item.Id}" ><i class="far fa-edit"></i></button>
                                                        <button class="btn btn-outline-danger btn-delete" data-id="${item.Id}"  ><i class="far fa-trash-alt"></i></button>
                                                    </td>
                                                </tr>`
                    };


                    $('#input-search').val(res.search);
                    if (res.totalPage > 1) {

                        pagination.load(res.totalPage, res.page, res.search);
                    } else {
                        $('.pagination').hide();
                    }

                    $(".list-Information").html(rows);
                }

            })
        },
        post: function (obj) {
            $.ajax({
                type: "POST",
                url: "/Admin/Information/Add",
                data: obj,
                success: function (res) {

                    validator.showErrors(res.errors);
                    if (!res.errors) {

                        $('.modal-entity').modal('hide');
                        information.load();
                    }
                    showToastr(res.type, res.message)
                }
            })
        },
        edit: function (obj) {
            $.ajax({
                type: "PUT",
                url: "/Admin/Information/Edit",
                data: obj,
                success: function (res) {

                    validator.showErrors(res.errors);
                    if (!res.errors) {

                        $('.modal-entity').modal('hide');
                        information.load();
                    }
                    showToastr(res.type, res.message)
                }
            })
        },
        delete: function (id) {
            $.ajax({
                type: "DELETE",
                url: "/Admin/Information/Delete",
                data: { id: id },
                success: function (res) {
                    showToastr(res.type, res.message)
                    information.load();

                }
            })
        },
        finOne: function (id) {
            $.ajax({
                type: "GET",
                url: "/Admin/Information/GetOne",
                data: { id: id },
                success: function (res) {
                    modal.setValue(res.data)

                    console.log(res.data)
                }
            })
        },
        updateStatus: function (id) {
            $.ajax({
                type: "PUT",
                url: "/Admin/Information/UpdateStatus",
                data: { id: id },
                success: function (res) {

                    showToastr(res.type, res.message)

                }
            })
        }
    }
    var modal = {
        removevalue: function () {
            $('#Id').val('')
            $('#Phone').val('')
            $('#Email').val('')
            $('#Address').val('')
            $('#Status').val('true')

        },
        setValue: function (obj) {
            $('#Id').val(obj.Id)
            $('#Phone').val(obj.Phone)
            $('#Email').val(obj.Email)
            $('#Address').val(obj.Address)
            $('#Status').val(obj.Status)
        }
    }
    information.load()
    //search
    $('.btn-search').click(function () {

        $('.pagination').removeData("twbs-pagination");
        let search = $('#input-search').val()
        information.load(1, search);

    });
    $(".btn-save").click(function () {
        $('.pagination').removeData("twbs-pagination");
        let data = $('#form-validate').serialize();

        //let formdata = $('#form').serialize();
        if ($('#Id').val()) {

            information.edit(data);
        } else {
            let postData = data.replace("Id=&", "");

            information.post(postData);
        }


    });

    $('.btn-add').click(function () {
        $('.modal-entity').modal("show");
    })

    //delete employee
    $(document).on('click', '.btn-delete', function () {

        let id = $(this).data("id");
        if (confirm("Do you want to delete this information ?")) {
            information.delete(id);
            $('.pagination').removeData("twbs-pagination");
        }
    });  //delete employee
    $(document).on('click', '.toggle-track ', function () {
        let id = $(this).parents('.toggle-switch').find('input:checkbox').data('id');

        
        if (id) {
            information.updateStatus(id)
        }
    });
    //delete employee
    $(document).on('click', '.btn-edit', function () {

        let id = $(this).data("id");
        $('.modal-entity').modal("show");
        information.finOne(id);

    });
    $('.modal-entity').on('hidden.bs.modal', function () {
        validator.resetForm();
        $(".form-control").removeClass("error");
        modal.removevalue();
    })
})