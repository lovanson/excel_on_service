﻿$(function () {
    var pagination = {

        load: function (totalPage, currentPage, search) {
            $('.pagination').show();
            $('.pagination').twbsPagination({
                totalPages: totalPage,
                visiblePages: 10,
                startPage: currentPage,

                onPageClick: function (event, page) {

                    let search = $('#input-search').val();
                    news.load(page, search);

                }
            })
        }
    }
    var ReviewService = {
        load: function (page, search) {
            $.ajax({
                type: "GET",
                url: "/Admin/ServiceReview/GetAll",
                data: { page: page, search: search },
                success: function (res) {
                   
                    let rows = ``;
                    let index = 0;

                    for (let item of res.data) {
                        index++;
                        rows += `<tr>
                                                                    <td>${index}</td>
                                                                    <td>${item.Service.Name}</td>
                                                                    <td>${item.User.Name}</td>
                                                                    <td>${item.Rate}</td>
                                                                    <td>${item.Comment}</td>
                                                                    <td>
                                                                        <select class="form-control" id="Status-update" data-id ="${item.Id}" >
                                                                        <option value="0"${item.Status == 0 ? `selected` : ``}>Hide reviews</option>
                                                                        <option value="1"${item.Status == 1 ? `selected` : ``}>Display on client home page</option>
                                                                        <option value="2"${item.Status == 2 ? `selected` : ``}>Display on service details page</option>
                                                                    </td>
                                                                </tr>`
                    };


                    $('#input-search').val(res.search);
                    if (res.totalPage > 1) {

                        pagination.load(res.totalPage, res.page, res.search);
                    } else {
                        $('.pagination').hide();
                    }

                    $(".list-new").html(rows);
                }

            })
        },
        delete: function (id) {
            $.ajax({
                type: "DELETE",
                url: "/Admin/New/Delete",
                data: { id: id },
                success: function (res) {
                    showToastr(res.type, res.message)
                    ReviewService.load();

                }
            })
        },
        updateStatus: function (id, status) {
            $.ajax({
                type: "PUT",
                url: "/Admin/ServiceReview/UpdateStatus",
                data: { id: id, status: status },
                success: function (res) {

                    swal({
                        title: res.message,
                        text: "You clicked the!",
                        type: res.type,
                        padding: '2em'
                    })

                }
            })
        }
    }
    ReviewService.load();
    //search
    $('.btn-search').click(function () {

        $('.pagination').removeData("twbs-pagination");
        let search = $('#input-search').val()
        ReviewService.load(1, search);

    });

    $(document).on('change', '#Status-update ', function () {
        let id = $(this).data('id');
        var status = $(this).val();
        ReviewService.updateStatus(id, status)

    });
})