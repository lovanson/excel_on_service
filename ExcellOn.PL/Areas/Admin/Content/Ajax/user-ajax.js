﻿$(function () {
    jQuery.validator.addMethod("phone", function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.length > 9 &&
            phone_number.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/gm);
    }, "Phone number is not valid");

    var validator = $("#form-validate").validate({
        rules: {
            Password: {
                required: true,
                minlength: 8,
            },
            Name: "required",
            Email: {
                required: true,
                email: true
            },

            Phone: {
                required: true,
                phone: true
            },
            ConfirmPassword: {
                required: true,
                minlength: 8,
                equalTo: "#Password"
            }
        }

    });

    var pagination = {

        load: function (totalPage, currentPage, search) {
            $('.pagination').show();
            $('.pagination').twbsPagination({
                totalPages: totalPage,
                visiblePages: 10,
                startPage: currentPage,

                onPageClick: function (event, page) {

                    let search = $('#input-search').val();
                    User.load(page, search);

                }
            })
        }
    }
    ///

    //
    var User = {

        load: function (page, search) {

            $.ajax({
                type: "GET",
                url: "/Admin/User/GetAll",
                data: { groupUserId: $('#groupUserId').val(), page: page, search: search },
                success: function (res) {

                    let rows = ``;
                    let index = 0;
                    for (let item of res.data) {
                        let birthDay = moment(new Date(parseInt(item.BirthDay.split('Date(')[1].split(')/')[0]))).format('yyyy-MM-DD');
                        index++;
                        rows += `<tr>
                                    <td>${index}</td>
                                    <td>${item.Name}</td>
                                    <td>${item.Phone}</td>
                                    <td>${item.Email}</td>
                                    <td>${item.Address}</td>
                                    <td>${birthDay}</td>


                                    <td>
                                        
                                            <label class="switch s-icons s-outline  s-outline-success">
                                                    <input type="checkbox" ${item.Status ? `checked` : ``}>
                                                    <span class="slider round change-status" data-id = ${item.Id}></span>
                                                </label>
                                </td>
                                    <td class="text-center">

                                        <button   class="btn btn-outline-warning btn-edit "   data-id="${item.Id}" ><i class="far fa-edit"></i></button>
                                                                  
                                    </td>
                                </tr>`
                    };


                    $('#input-search').val(res.search);
                    if (res.totalPage > 1) {

                        pagination.load(res.totalPage, res.page, res.search);
                    } else {
                        $('.pagination').hide();
                    }
                    $('#groupUserId').val(res.groupUserId);
                    $(".list-User").html(rows);

                }

            })
        },
        post: function (obj) {
            $.ajax({
                type: "POST",
                url: "/Admin/User/Add",
                data: obj,
                success: function (res) {

                    validator.showErrors(res.errors);
                    if (!res.errors) {
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                        $('.modal-entity').modal('hide');
                        User.load();
                    }

                }
            })
        },
        edit: function (obj) {
            $.ajax({
                type: "PUT",
                url: "/Admin/User/Edit",
                data: obj,
                success: function (res) {


                    if (!res.errors) {

                        $('.modal-entity').modal('hide');
                        User.load();
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                    }

                }
            })
        },
        finOne: function (id) {
            $.ajax({
                type: "GET",
                url: "/Admin/User/GetOne",
                data: { id: id },
                success: function (res) {
                    modal.setValue(res.data)
                }
            })
        },
        updateStatus: function (id) {
            $.ajax({
                type: "PUT",
                url: "/Admin/User/UpdateStatus",
                data: { id: id },
                success: function (res) {

                    swal({
                        title: res.message,
                        text: "You clicked the!",
                        type: res.type,
                        padding: '2em'
                    })

                }
            })
        }
    }
    var groupUser = {
        getAll: function () {
            $.ajax({
                type: "GET",
                url: "/Admin/GroupUser/FindAll",
                success: function (res) {
                    let option = ``;
                    for (let item of res.data) {
                        option += `<option value="${item.Id}">${item.Name}</option>`
                    }

                    $('#GroupUserId').html(option)
                    $('#groupUserId').html(option)
                }
            }).done(function (data) {

                User.load()

            });
        }
    }
    var modal = {

        setValue: function (obj) {
            $('#Id').val(obj.Id)
            $('#Name').val(obj.Name)
            $('#Phone').val(obj.Phone)
            $('#Email').val(obj.Email)
            $('#Password').val(obj.Password)

            $('#GroupUserId').val(obj.GroupUserId)
            $('#Password').css('display', 'none')
            $('#ConfirmPassword').css('display', 'none')
        },
        removeData: function () {
            $('#Id').val('')
            $('#Name').val('')
            $('#Phone').val('')
            $('#Email').val('')
            $('#Password').val('')
            $('#ConfirmPassword').val('')
            $('#Password').css('display', 'block')
            $('#ConfirmPassword').css('display', 'block')

        }
    }
    groupUser.getAll()

    //search
    $('.btn-search').click(function () {

        $('.pagination').removeData("twbs-pagination");
        let search = $('#input-search').val()
        User.load(1, search);

    });
    $(".btn-save").click(function () {
        $('.pagination').removeData("twbs-pagination");
        let Id = $('#Id').val()
        let data = $('#form-validate').serialize();

        //let formdata = $('#form').serialize();
        if (Id) {

            User.edit(data)
        } else {
            let postData = data.replace("Id=&", "");

            User.post(postData);
        }




    });

    $('.btn-add').click(function () {
        $('.modal-entity').modal("show");
    })


    //delete User
    $(document).on('click', '.btn-edit', function () {

        let id = $(this).data("id");
        $('.modal-entity').modal("show");
        User.finOne(id);

    });
    $('.modal-entity').on('hidden.bs.modal', function () {
        validator.resetForm();
        $(".form-control").removeClass("error");
        modal.removeData();
    })

    $('#groupUserId').change(function () {
        User.load()
    })


    $(document).on('click', '.change-status ', function () {
        let id = $(this).attr("data-Id");


        if (id) {
            User.updateStatus(id)
        }
    });
})