﻿$(function () {
    var description = CKEDITOR.replace('Content');
    var validator = $("#form-validate").validate({
        rules: {
            Title: "required",
            ShortDescription: {
                required: true,

            },
            Content: "required",

            PostCategory: "required",


        },
        messages: {
            Title: "The Title field is required.",
            ShortDescription: {
                required: "The ShortDescription field is required.",
            },
            Content: "The Content field is required.",

            PostCategory: "The PostCategory field is required.",

        }
    });
    var pagination = {

        load: function (totalPage, currentPage, search) {
            $('.pagination').show();
            $('.pagination').twbsPagination({
                totalPages: totalPage,
                visiblePages: 10,
                startPage: currentPage,

                onPageClick: function (event, page) {

                    let search = $('#input-search').val();
                    news.load(page, search);

                }
            })
        }
    }

    var postcategory = {
        getAll: function () {
            $.ajax({
                type: "GET",
                url: "/Admin/PostCategori/GetAll",
                success: function (res) {
                    let option = ``;
                    for (let item of res.data) {
                        option += `<option value="${item.Id}">${item.Name}</option>`
                    }

                    $('#PostCategoryId').html(option)
                }
            })
        }
    }
    var news = {
        load: function (page, search) {
            $.ajax({
                type: "GET",
                url: "/Admin/New/GetAll",
                data: { page: page, search: search },
                success: function (res) {
                   
                    let rows = ``;
                    let index = 0;

                    for (let item of res.data) {
                        let createdDate = moment(new Date(parseInt(item.CreatedDate.split('Date(')[1].split(')/')[0]))).format('yyyy-MM-DD hh:mm:ss');
                        let updatedDate = moment(new Date(parseInt(item.UpdatedDate.split('Date(')[1].split(')/')[0]))).format('yyyy-MM-DD hh:mm:ss');
                        index++;
                        rows += `<tr>
                                                                <td>${index}</td>
                                                                <td>${item.Title}</td>
                                                              
                                                               <td><img src="/Areas/Admin/Content/Images/${item.Image}" style="width:100px;height:100px" /></td>
                                                                <td>${item.User.Name}</td>
                                                                <td>${item.PostCategory.Name}</td>
                                                                <td>${createdDate}</td>
                                                                <td>${updatedDate}</td>
                                                                <td>
                                                                    <label class="switch s-icons s-outline  s-outline-success">
                                                                        <input type="checkbox" ${item.Status ? `checked` : ``}>
                                                                        <span class="slider round change-status" data-id = ${item.Id}></span>
                                                                    </label>
                                                                </td>
                                                                <td class="text-center">

                                                                    <button   class="btn btn-outline-warning btn-edit "   data-id="${item.Id}" ><i class="far fa-edit"></i></button>
                                                                    <button class="btn btn-outline-danger btn-delete" data-id="${item.Id}"  ><i class="far fa-trash-alt"></i></button>
                                                                </td>
                                                            </tr>`
                    };


                    $('#input-search').val(res.search);
                    if (res.totalPage > 1) {

                        pagination.load(res.totalPage, res.page, res.search);
                    } else {
                        $('.pagination').hide();
                    }

                    $(".list-new").html(rows);
                }

            })
        },
        post: function (obj) {
            $.ajax({
                type: "POST",
                url: "/Admin/New/Add",
                data: obj,
                contentType: false,
                processData: false,
                success: function (res) {

                    validator.showErrors(res.errors);
                    if (!res.errors) {

                        $('.modal-entity').modal('hide');
                        news.load();
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                    }
                   
                }
            })
        },
        edit: function (obj) {
            $.ajax({
                type: "PUT",
                url: "/Admin/New/Edit",
                data: obj,
                contentType: false,
                processData: false,
                success: function (res) {

                    validator.showErrors(res.errors);
                    if (!res.errors) {

                        $('.modal-entity').modal('hide');
                        news.load();
                        swal({
                            title: res.message,
                            text: "You clicked the!",
                            type: res.type,
                            padding: '2em'
                        })
                    }
                   
                }
            })
        },
        delete: function (id) {
            $.ajax({
                type: "DELETE",
                url: "/Admin/New/Delete",
                data: { id: id },
                success: function (res) {
                    showToastr(res.type, res.message)
                    news.load();

                }
            })
        },
        finOne: function (id) {
            $.ajax({
                type: "GET",
                url: "/Admin/New/GetOne",
                data: { Id: id },
                success: function (res) {
                    console.log(res)
                    modal.setValue(res.data)


                }
            })
        },
        updateStatus: function (id) {
            $.ajax({
                type: "PUT",
                url: "/Admin/New/UpdateStatus",
                data: { id: id },
                success: function (res) {

                    swal({
                        title: res.message,
                        text: "You clicked the!",
                        type: res.type,
                        padding: '2em'
                    })

                }
            })
        }
    }
    var modal = {
        removevalue: function () {
            $('#Id').val('')
            $('#Title').val('')
            $('#ShortDescription').val('')

            $('#PostCategory').val('')
            $('#CreatedDate').val('')
            $('#UpdatedDate').val('')
            $('#PostedFileBase').val(null)
            description.setData('');
        },
        setValue: function (obj) {
            $('#Id').val(obj.Id)
            $('#Title').val(obj.Title)
            $('#ShortDescription').val(obj.ShortDescription)

            $('#PostCategoryId').val(obj.PostCategoryId)
            description.setData(obj.Content);
        }
    }
    postcategory.getAll();
    news.load();
    //search
    $('.btn-search').click(function () {

        $('.pagination').removeData("twbs-pagination");
        let search = $('#input-search').val()
        news.load(1, search);

    });
    $(".btn-save").click(function () {
        $('.pagination').removeData("twbs-pagination");

        //formdata = formdata.replace("id=&", '');
        let formdata = new FormData();
        formdata.append("Id", $('#Id').val())
        formdata.append("Title", $('#Title').val())

        formdata.append("PostCategoryId", $('#PostCategoryId').val())

        formdata.append("Content", description.getData())
        formdata.append("ShortDescription", $('#ShortDescription').val())


        //let formdata = $('#form').serialize();
        if ($('#Id').val()) {
            formdata.append("PostedFileBase", $('#PostedFileBase')[0].files[0] || new Blob())
            news.edit(formdata);
        } else {
            formdata.append("PostedFileBase", $('#PostedFileBase')[0].files[0])
            formdata.delete("Id")
            news.post(formdata);
        }


    });

    $('.btn-add').click(function () {
        $('.modal-entity').modal("show");
    })

    //delete new
    $(document).on('click', '.btn-delete', function () {

        let id = $(this).data("id");
       
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            padding: '2em'
        }).then(function (result) {
            if (result.value) {
                news.delete(id);
                $('.pagination').removeData("twbs-pagination");

            }
        })
    });
    //delete new
    $(document).on('click', '.btn-edit', function () {

        let id = $(this).data("id");
        $('.modal-entity').modal("show");
        news.finOne(id);

    });
    $('.modal-entity').on('hidden.bs.modal', function () {
        validator.resetForm();
        $(".form-control").removeClass("error");
        modal.removevalue();
    })
    $(document).on('click', '.change-status', function () {
        let id = $(this).attr("data-Id");


        if (id) {
            news.updateStatus(id)
        }
    });
})