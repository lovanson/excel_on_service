﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcellOn.DAL
{
   public  class GroupUser
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public bool IsAdmin { get; set; }
        public ICollection<User> Users { get; set; }
        public ICollection<GroupUserPermission> GroupUserPermissions { get; set; }
    }
}
