﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcellOn.DAL
{
    public class GroupUserPermission
    {
        public int Id { get; set; }
        public virtual int GroupUserId { get; set; }
        [ForeignKey("GroupUserId")]
        public virtual GroupUser GroupUser { get; set; }
        public virtual int PermisionId { get; set; }
        [ForeignKey("PermisionId")]
        public virtual Permission Permission { get; set; }
    }
}
