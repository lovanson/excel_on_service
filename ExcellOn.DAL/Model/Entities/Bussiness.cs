﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcellOn.DAL
{
   public class Bussiness
    {
        [Key]
        public string BusinessId { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
     
      
    }
}
