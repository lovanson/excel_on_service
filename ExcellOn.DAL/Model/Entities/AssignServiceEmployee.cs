﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcellOn.DAL
{
   public class AssignServiceEmployee
    {
        public int Id { get; set; }
        public virtual int ClientServiceId { get; set; }
        [ForeignKey("ClientServiceId")]
        public virtual ClientService ClientService { get; set; }
        public virtual int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }
    }
}
