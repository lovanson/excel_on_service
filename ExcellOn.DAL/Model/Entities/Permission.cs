﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcellOn.DAL
{
    public class Permission
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual string BusinessId { get; set; }
        [ForeignKey("BusinessId")]
        public virtual Bussiness Business { get; set; }
        [NotMapped]
        public bool IsGranted { get; set; }
       
    }
}
