﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcellOn.DAL
{
    public class OrderDetail
    {
        public int Id { get; set; }
        public virtual int OrderId { get; set; }
        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }
        public virtual int ServiceId { get; set; }
        [ForeignKey("ServiceId")]
        public virtual Service Service { get; set; }
     
        public double Charges { get; set; }
        public int NumberDates { get; set; }
        public int NumberEmployees { get; set; }
        public byte Status { get; set; }
        public double TotalPrice { get; set; }

    }
}
