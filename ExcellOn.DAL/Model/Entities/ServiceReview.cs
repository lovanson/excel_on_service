﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcellOn.DAL
{
    public class ServiceReview
    {
        public int Id { get; set; }
        public virtual int ServiceId { get; set; }
        [ForeignKey("ServiceId")]
        public virtual Service Service { get; set; }
        public virtual int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public double Rate { get; set; }
        public string Comment { get; set; }
        public byte Status { get; set; }
        public bool isUpdate { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
