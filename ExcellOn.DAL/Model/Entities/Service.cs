﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.DAL
{

    public class Service
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public string Image { get; set; }
        [Required]
        [NotMapped]
        public HttpPostedFileBase PostedFileBase { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public double Change { get; set; }
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        [Column]
        public string Description { get; set; }

        
        public string ShortDescription { get; set; }

        public bool Status { get; set; }
        [NotMapped]
        public string IdEncrypt { get; set; }
        [NotMapped]
        public bool IsFavorite { get; set; }

    }

}
