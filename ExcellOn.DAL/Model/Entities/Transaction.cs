﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcellOn.DAL
{
    public class Transaction
    {
        public int Id { get; set; }
        public string ClientName { get; set; }
        public double Money { get; set; }
        public double Blance { get; set; }
        public bool Status { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
