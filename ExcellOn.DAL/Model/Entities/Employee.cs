﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ExcellOn.DAL
{
    public class Employee
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Phone number is not valid")]
        public string Phone { get; set; }
        [Required]
        public string Address { get; set; }
        public virtual int DepartmentId { get; set; }
        [ForeignKey("DepartmentId")]
        public virtual Department Department  { get; set; }
       
      
        [Required]
       
        public DateTime BirthDay { get; set; }
        public bool Gender { get; set; }
        public bool Status { get; set; }
    }
}
