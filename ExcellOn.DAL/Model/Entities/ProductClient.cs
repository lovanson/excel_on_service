﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.DAL
{
    public class ProductClient
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
     
        public string Image { get; set; }
        [Required]
        [NotMapped]
        public HttpPostedFileBase PostedFileBase { get; set; }
        public double Price { get; set; }
        [Required]
        public string Unit { get; set; }
        [Required]
        public string PromotionInformation { get; set; }
        [Required]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        [Column]
        public string Description { get; set; }

        public virtual int UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public bool Status { get; set; }
        public string IdEncrypt { get; set; }
    }
}
