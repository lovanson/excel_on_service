﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ExcellOn.DAL
{
    public class User
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public string Name { get; set; }
      
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Phone number is not valid")]
        public string Phone { get; set; }

        public string Avatar { get; set; }
        public string Address { get; set; }
     


        [Required(ErrorMessage = "This field is required.")]
        [DataType(DataType.EmailAddress)]
        public String Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MinLength(8,ErrorMessage ="Please enter at least 8 characters.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [NotMapped]
        [Compare("Password",ErrorMessage = "Please enter the same value again.")]
        [MinLength(8, ErrorMessage = "Please enter at least 8 characters !")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        public bool IsClient { get; set; }
        public bool Status { get; set; }
        public bool ComfirmEmail { get; set; }

        public int GroupUserId { get; set; }
        [ForeignKey("GroupUserId")]
        public GroupUser GroupUser { get; set; }
        public bool Gender { get; set; }
        public DateTime BirthDay { get; set; }
    }
}
