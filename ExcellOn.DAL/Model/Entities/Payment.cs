﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcellOn.DAL
{
   public class Payment
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string BankName { get; set; }
        [Required]
        [RegularExpression(@"^[0-9]{10,13}$")]
        public string AccountNumber { get; set; }
        [Required]
        public DateTime ReleaseDate { get; set; }
        [Required]
        public DateTime ExpirationDate { get; set; }
        [Required]
        [RegularExpression(@"^[0-9]{12}$")]
        public string Identity { get; set; }
        [Required]
       
        public double PaymentAmount { get; set; }
       
    }
}
