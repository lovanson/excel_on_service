﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcellOn.DAL
{
    public class ShoppingListDTO
    {
        public Order order { get; set; }
        public List<OrderDetail> orderdetails { get; set; }
    }
}
