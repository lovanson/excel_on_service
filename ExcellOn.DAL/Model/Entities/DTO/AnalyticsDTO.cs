﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcellOn.DAL
{
    public class AnalyticsDTO
    {
        public List<ReceiveEmail> Receives { get; set; }
        public ServiceReview ServiceReview { get; set; }
        public double Balance { get; set; }
        public int ClientNumber { get; set; }
        public int EmployeeNumber { get; set; }
    }
}
