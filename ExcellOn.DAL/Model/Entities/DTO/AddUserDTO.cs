﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcellOn.DAL
{
    public class AddUserDTO
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Phone number is not valid")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [DataType(DataType.EmailAddress)]
        public String Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [MinLength(8, ErrorMessage = "Please enter at least 8 characters.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "This field is required.")]

        [Compare("Password", ErrorMessage = "Please enter the same value again.")]
        [MinLength(8, ErrorMessage = "Please enter at least 8 characters !")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }



        public int GroupUserId { get; set; }
     
      
    }
}
