﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ExcellOn.DAL
{
   public class UserDTO
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Phone number is not valid")]
        public string Phone { get; set; }

        public string Avatar { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public string Address { get; set; }



        public HttpPostedFileBase PostedFileBase { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public DateTime BirthDay { get; set; }
        public bool Gender { get; set; }
    }
}
