﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcellOn.DAL
{
    public class SaleDTO
    {
        public IEnumerable<Transaction> Transactions { get; set; }
        public IEnumerable<Order> Orders { get; set; }
        public IEnumerable<Service> Services { get; set; }
       
        public ExcellBank ExcellBank { get; set; }
        public double Received { get; set; }
        public double Spent { get; set; }
        public int TotalOrder { get; set; }
    }
}
