﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcellOn.DAL
{
    public class UserDTOChangePass
    {
        public int Id { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [MinLength(8, ErrorMessage = "Please enter at least 8 characters.")]
        public string Password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [MinLength(8, ErrorMessage = "Please enter at least 8 characters.")]
        public string NewPassword { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [MinLength(8, ErrorMessage = "Please enter at least 8 characters.")]
        [Compare("NewPassword", ErrorMessage = "Please enter the same value again.")]
        public string ComfirmPassword { get; set; }
    }
}
