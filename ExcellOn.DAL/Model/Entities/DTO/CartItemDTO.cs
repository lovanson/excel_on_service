﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcellOn.DAL
{
   public class CartItemDTO
    {
        public int ServiceId { get; set; }
        [Required]
        [RegularExpression(@"^[0-9]+$")]
       
        public int DateNumber { get; set; }
        [RegularExpression(@"^[0-9]+$")]
        public int EmployeeNumber { get; set; }
    }
}
