﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ExcellOn.DAL
{
    public class New
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string ShortDescription { get; set; }
        [Required]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        [Column]
        public string Content { get; set; }

        public string Image { get; set; }
        [Required]
        [NotMapped]
        public HttpPostedFileBase PostedFileBase { get; set; }

        public virtual int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public virtual int PostCategoryId { get; set; }
        [ForeignKey("PostCategoryId")]
        public virtual PostCategory PostCategory { get; set; }
 
        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }
        public bool Status { get; set; }
        [NotMapped]
        public string IdEncrypt { get; set; }
    }
}
