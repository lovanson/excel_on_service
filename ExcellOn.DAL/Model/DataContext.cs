﻿using ExcellOn.DAL.Migrations;
using ExcellOn.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcellOn.DAL
{
   public class DataContext:DbContext
    {
        public DataContext():base("name=DataContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DataContext, Configuration>("DataContext"));
        }

        public virtual DbSet<Department> Departments { get; set; }


        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Service> Services { get; set; }
    
        public virtual DbSet<ClientService> ClientServices { get; set; }
        public virtual DbSet<AssignServiceEmployee> AssignServiceEmployees { get; set; }
        public virtual DbSet<ReceiveEmail> ReceiveEmails { get; set; }

        public virtual DbSet<New> News { get; set; }
        public virtual DbSet<PostCategory> PostCategories { get; set; }
        public virtual DbSet<Payment> Payments { get; set; }
        public virtual DbSet<Bussiness>  Bussinesses{ get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<GroupUser> GroupUsers { get; set; }
        public virtual DbSet<GroupUserPermission> GroupUserPermissions { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderDetail> OrderDetails { get; set; }
        public virtual DbSet<ProductClient> ProductClients { get; set; }

        public virtual DbSet<FavoriteService> FavoriteServices { get; set; }


        public virtual DbSet<ContactInformation> ContactInformation { get; set; }
        public virtual DbSet<ServiceReview> ServiceReviews { get; set; }
        public virtual DbSet<ExcellBank> ExcellBanks { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        


    }
}
